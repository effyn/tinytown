use {
    std::{fs, net::SocketAddr, path::PathBuf},
    structopt::StructOpt,
    tinytown::{
        backend::Backend,
        game::{Game, GameMode, GameParams},
    },
    tinytown_backend_luminance::LuminanceBackend,
    tinytown_backend_null::NullBackend,
};

const WINDOW_SIZE: (u32, u32) = (960, 960);
const WINDOW_TITLE: &'static str = "Tinytown";

fn available_backends() -> Vec<&'static str> {
    let mut backends = Vec::new();
    #[cfg(feature = "backend-luminance")]
    backends.push("luminance");
    backends.push("null");
    backends
}

#[derive(StructOpt)]
struct Opt {
    /// List backends.
    #[structopt(long = "list-backends")]
    list_backends: bool,

    /// Load a build in .ttb format
    #[structopt(short = "l", long = "load-ttb")]
    load_build: Option<PathBuf>,

    /// Load config.
    #[structopt(short = "c", long = "config", default_value = "game.toml")]
    config_path: PathBuf,

    /// Select backend.
    #[structopt(short = "b", long = "backend")]
    backend: Option<String>,

    /// Choose a name.
    #[structopt(short = "n", long = "name")]
    name: Option<String>,

    /// Client mode.
    #[structopt(long = "connect")]
    connect: Option<SocketAddr>,
}

macro_rules! choose_backend {
    ($name:expr, $cont:expr, $($args:expr),*) => {
        match $name {
            "luminance" => {
                $cont(LuminanceBackend::new(WINDOW_SIZE, WINDOW_TITLE)?, $($args),*)
            },
            "null" => {
                $cont(NullBackend::new(), $($args),*)
            },
            _ => panic!("invalid backend"),
        }
    }
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let backends = available_backends();
    if opt.list_backends {
        println!("Available backends:");
        for backend in &backends {
            println!(" - {}", backend);
        }
        return Ok(());
    }
    let selected_backend = if let Some(ref backend_name) = opt.backend {
        backends
            .iter()
            .find(|&&backend| backend == backend_name)
            .map(|&b| b)
            .expect("invalid backend name")
    } else {
        backends[0]
    };
    choose_backend!(selected_backend, cont, opt)
}

fn cont<B: Backend>(backend: B, opt: Opt) -> anyhow::Result<()> {
    let config = {
        let data = fs::read_to_string(opt.config_path)?;
        toml::from_str(&data)?
    };
    let params = GameParams {
        mode: if opt.connect.is_some() {
            GameMode::Client
        } else {
            GameMode::Single
        },
        connect_addr: opt.connect,
        listen_addr: None,
        my_name: Some(opt.name.unwrap_or_else(|| "anon".to_owned())),
    };
    let mut game = Game::new(backend, config, params);
    game.init()?;
    if let Some(path) = opt.load_build {
        game.load_build(path);
    }
    game.start()?;
    Ok(())
}
