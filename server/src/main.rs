use {
    std::{fs, net::SocketAddr, path::PathBuf},
    structopt::StructOpt,
    tinytown::game::{Game, GameMode, GameParams},
    tinytown_backend_null::NullBackend,
};

#[derive(StructOpt)]
struct Opt {
    /// Load config.
    #[structopt(short = "c", long = "config", default_value = "game.toml")]
    config_path: PathBuf,

    /// Load a build in .ttb format
    #[structopt(short = "l", long = "load-ttb")]
    load_build: Option<PathBuf>,

    /// Address to listen on.
    #[structopt(long = "listen")]
    listen: SocketAddr,
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let config = {
        let data = fs::read_to_string(opt.config_path)?;
        toml::from_str(&data)?
    };
    let params = GameParams {
        mode: GameMode::Server,
        listen_addr: Some(opt.listen),
        connect_addr: None,
        my_name: None,
    };
    let mut game = Game::new(NullBackend::new(), config, params);
    game.init()?;
    if let Some(path) = opt.load_build {
        game.load_build(path);
    }
    game.start()?;
    Ok(())
}
