in vec3 v_normal;
in vec2 v_uv;

out vec4 frag;

uniform sampler2D tex;
uniform vec3 light;

void main() {
  float brightness = 0.2 + ((1. - dot(light, v_normal)) / 2.) * 0.8;
  vec4 sample = texture(tex, mod(v_uv, vec2(1., 1.)));
  frag = vec4(brightness * sample.xyz, sample.w * 0.8);
}
