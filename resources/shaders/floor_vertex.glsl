in vec3 position;
in vec3 normal;
in vec2 tex_uv;

out vec2 v_uv;
out vec3 v_normal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
  v_uv = tex_uv;
  v_normal = normal;
  gl_Position = projection * view * model * vec4(position, 1.);
}
