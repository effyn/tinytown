in vec3 v_tex_dir;

out vec4 color;

uniform samplerCube tex;

void main() {
  color = texture(tex, v_tex_dir);
}
