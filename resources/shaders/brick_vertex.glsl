in vec3 position;
in vec4 color;
in vec3 normal;
in vec2 tex_size;
in vec2 tex_uv;

out vec4 v_color;
out vec3 v_normal;
out vec2 v_size;
out vec2 v_uv;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
  gl_Position = projection * view * model * vec4(position, 1.);
  v_color = color;
  v_normal = normal;
  v_size = tex_size;
  v_uv = tex_uv;
}
