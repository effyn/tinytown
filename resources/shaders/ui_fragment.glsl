uniform sampler2D tex;
uniform vec2 full_tex_size;

in vec2 v_rect_pos;
in vec2 v_rect_size;
in vec2 v_tex_start;
in vec2 v_tex_size;
in vec2 v_tex_border_tl;
in vec2 v_tex_border_br;

out vec4 color;

// border (0 to tex_border_tl.x)
// |---|
//     inner (tex_border_tl.x to tex_size.x - tex_border_br.x)
//     |---|
//          border (tex_size.x - tex_border_br.x to tex_size.x)
//         |---|
//
// +-----------+ ---
// |           |  | border (0 to tex_border_tl.y)
// |   |   |   |  |
// |           |  |
// | - +---+ - | ---
// |   |   |   |  | inner (tex_border_tl.y to tex_size.y - tex_border.br.y)
// |   |   |   |  |
// | - +---+ - | ---
// |           |  | border (tex_size.y - tex_border_br.y to tex_size.y)
// |   |   |   |  |
// |           |  |
// +-----------+ ---
//

float segment_axis(float pos, float size, float tex_start, float tex_size, float border_start, float border_end) {
  if(pos < border_start) {
    return tex_start + pos;
  }
  else if(pos > size - border_end) {
    float overflow = pos - (size - border_end);
    return tex_start + tex_size - border_end + overflow;
  }
  else {
    float tex_inner_size = tex_size - border_start - border_end;
    float adjusted_pos = pos - border_start;
    float fl = floor(adjusted_pos / tex_inner_size);
    return tex_start + border_start + adjusted_pos - fl * tex_inner_size;
  }
}

vec2 segment2d(vec2 pos, vec2 size, vec2 tex_start, vec2 tex_size, vec2 border_start, vec2 border_end) {
  return vec2(
    segment_axis(pos.x, size.x, tex_start.x, tex_size.x, border_start.x, border_end.x),
    segment_axis(pos.y, size.y, tex_start.y, tex_size.y, border_start.y, border_end.y)
  );
}

void main() {
  vec2 tex_pos = segment2d(v_rect_pos, v_rect_size, v_tex_start, v_tex_size, v_tex_border_tl, v_tex_border_br);
  vec2 sampler_coord = tex_pos / full_tex_size;
  color = texture(tex, sampler_coord);
}
