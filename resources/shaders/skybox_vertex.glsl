uniform mat4 view;
uniform mat4 projection;

in vec3 position;

in vec3 tex_dir;

out vec3 v_tex_dir;

void main() {
  v_tex_dir = tex_dir;
  gl_Position = projection * view * vec4(position, 1.);
}
