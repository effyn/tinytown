uniform vec2 window_size;

in vec2 screen_position;
in vec2 rect_pos;
in vec2 rect_size;
in vec2 tex_start;
in vec2 tex_size;
in vec2 tex_border_tl;
in vec2 tex_border_br;

out vec2 v_rect_pos;
out vec2 v_rect_size;
out vec2 v_tex_start;
out vec2 v_tex_size;
out vec2 v_tex_border_tl;
out vec2 v_tex_border_br;

void main() {
  vec2 position = (screen_position / window_size * 2.) - vec2(1., 1.);
  position.y = -position.y;
  gl_Position = vec4(position, 0., 1.);
  v_rect_pos = rect_pos;
  v_rect_size = rect_size;
  v_tex_start = tex_start;
  v_tex_size = tex_size;
  v_tex_border_tl = tex_border_tl;
  v_tex_border_br = tex_border_br;
}
