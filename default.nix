{ pkgs ? import <nixpkgs> {}
, stdenv ? pkgs.stdenv
, ...
}:

stdenv.mkDerivation rec {
  pname = "tinytown";
  version = "0.0.1";

  buildInputs = with pkgs; [
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    xorg.libXinerama
    xorg.libXext
    xorg.libXxf86vm
    wayland
    wayland-protocols
    libGL
    glfw
    cmake
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${stdenv.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
  '';
}
