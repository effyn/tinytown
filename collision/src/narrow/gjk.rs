use {
    approx::ulps_eq,
    cgmath::{prelude::*, Point3, Vector3},
    std::fmt,
};

use crate::{
    consts::{EPA_MAX_ITERATIONS, GJK_MAX_ITERATIONS, TOLERANCE},
    Contact, Midpoint, MinkowskiDifference, Support,
};

#[derive(Clone)]
pub struct Simplex {
    vertices: [Point3<f32>; 4],
    dim: usize,
}

impl fmt::Debug for Simplex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.dim {
            0 => write!(f, "Simplex::point({:?})", &self.vertices[0]),
            1 => write!(f, "Simplex::line({:?})", &self.vertices[..2]),
            2 => write!(f, "Simplex::triangle({:?})", &self.vertices[..3]),
            3 => write!(f, "Simplex::tetrahedron({:?})", &self.vertices[..4]),
            _ => unreachable!(),
        }
    }
}

impl Simplex {
    pub fn point(a: Point3<f32>) -> Simplex {
        Simplex {
            vertices: [a; 4],
            dim: 0,
        }
    }

    pub fn push(&mut self, point: Point3<f32>) {
        if self.dim < 3 {
            self.dim += 1;
            self.vertices[self.dim] = point;
        } else {
            unimplemented!()
        }
    }

    pub fn get_new_dir_and_cull(&mut self) -> Option<Vector3<f32>> {
        match self.dim {
            1 => Some(cross_aba(
                self.vertices[0] - self.vertices[1],
                Point3::origin() - self.vertices[1],
            )),
            2 => Some(self.triangle_case()),
            3 => self.tetrahedral_case(),
            _ => unreachable!(),
        }
    }

    fn tetrahedral_case(&mut self) -> Option<Vector3<f32>> {
        let (a, b, c, d) = (
            self.vertices[3],
            self.vertices[2],
            self.vertices[1],
            self.vertices[0],
        );
        let ab = b - a;
        let ac = c - a;
        let ao = Point3::origin() - a;
        let abc = ab.cross(ac);
        debug_assert!(abc.dot(d - a) <= 0.);
        if abc.dot(ao) > TOLERANCE {
            self.vertices[2] = a;
            self.vertices[1] = b;
            self.vertices[0] = c;
            self.dim = 2;
            Some(self.triangle_case())
        } else {
            let ad = d - a;
            let acd = ac.cross(ad);
            debug_assert!(acd.dot(b - a) <= 0.);
            if acd.dot(ao) > TOLERANCE {
                self.vertices[2] = a;
                self.vertices[1] = c;
                self.vertices[0] = d;
                self.dim = 2;
                Some(self.triangle_case())
            } else {
                let adb = ad.cross(ab);
                debug_assert!(adb.dot(c - a) <= 0.);
                if adb.dot(ao) > TOLERANCE {
                    self.vertices[2] = a;
                    self.vertices[1] = d;
                    self.vertices[0] = b;
                    self.dim = 2;
                    Some(self.triangle_case())
                } else {
                    None
                }
            }
        }
    }

    fn triangle_case(&mut self) -> Vector3<f32> {
        let (a, b, c) = (self.vertices[2], self.vertices[1], self.vertices[0]);
        let ab = b - a;
        let ac = c - a;
        let ao = Point3::origin() - a;
        let abc = ab.cross(ac);
        if abc.cross(ac).dot(ao) > 0. {
            if ac.dot(ao) > 0. {
                self.vertices[0] = c;
                self.vertices[1] = a;
                self.dim = 1;
                cross_aba(ac, ao)
            } else if ab.dot(ao) > 0. {
                self.vertices[0] = b;
                self.vertices[1] = a;
                self.dim = 1;
                cross_aba(ab, ao)
            } else {
                self.vertices[0] = a;
                self.dim = 0;
                ao
            }
        } else if ab.cross(abc).dot(ao) > 0. {
            if ab.dot(ao) > 0. {
                self.vertices[0] = b;
                self.vertices[1] = a;
                self.dim = 2;
                cross_aba(ab, ao)
            } else {
                self.vertices[0] = a;
                self.dim = 0;
                ao
            }
        } else if abc.dot(ao) > 0. {
            abc
        } else {
            self.vertices.swap(0, 1);
            -abc
        }
    }

    #[cfg(test)]
    pub fn as_openscad_string(&self) -> String {
        use std::fmt::Write;
        let mut s = String::new();
        writeln!(s, "hull() {{").unwrap();
        for i in 0..self.dim + 1 {
            let vtx = self.vertices[i];
            writeln!(
                s,
                "    translate([{}, {}, {}]) sphere(0.1);",
                vtx.x, vtx.y, vtx.z
            )
            .unwrap();
        }
        writeln!(s, "}}").unwrap();
        s
    }
}

fn cross_aba(a: Vector3<f32>, b: Vector3<f32>) -> Vector3<f32> {
    let ret = a.cross(b).cross(a);
    if ulps_eq!(ret, Vector3::zero()) {
        a.cross(b + Vector3::new(1., 0., 0.)).cross(a)
    } else {
        ret
    }
}

/// Run GJK on both shapes and return the resulting simplex.
pub fn gjk<A: Support + Midpoint, B: Support + Midpoint>(
    shape_a: &A,
    shape_b: &B,
) -> Option<Simplex> {
    let shape = MinkowskiDifference::new(shape_a, shape_b);
    let mut midpoint = shape.midpoint();
    if Point3::origin().distance(midpoint) < TOLERANCE {
        midpoint.x += 0.00001;
        midpoint.y += 0.00001;
        midpoint.z += 0.00001;
    }
    let mut dir = midpoint - Point3::origin();
    let initial_point = shape.support_point(dir);
    let mut simplex = Simplex::point(initial_point);
    dir = -dir;
    for _i in 0..GJK_MAX_ITERATIONS {
        let support_point = shape.support_point(dir);
        if support_point.dot(dir) <= 0. {
            return None;
        }
        simplex.push(support_point);
        #[cfg(test)]
        eprintln!("if (frame == {}) {}", _i, simplex.as_openscad_string());
        if let Some(new_dir) = simplex.get_new_dir_and_cull() {
            dir = if ulps_eq!(new_dir, Vector3::zero()) {
                Vector3::new(0.1, 0.2, 0.3)
            } else {
                new_dir
            };
        } else {
            return Some(simplex);
        }
    }
    log::error!("GJK max iterations exhausted, giving up");
    #[cfg(test)]
    panic!("gjk iteration count exceeded in tests");
    #[cfg(not(test))]
    None
}

pub fn gjk_collide<A: Support + Midpoint, B: Support + Midpoint>(
    shape_a: &A,
    shape_b: &B,
) -> Option<Contact> {
    gjk(shape_a, shape_b).and_then(|simplex| {
        let minkowski_diff = MinkowskiDifference::new(shape_a, shape_b);
        let face = epa(&minkowski_diff, simplex);
        if ulps_eq!(face.normal * face.distance, Vector3::zero()) {
            None
        } else {
            Some(Contact {
                normal: face.normal,
                depth: face.distance,
            })
        }
    })
}

// TODO: implement CCD
//pub fn gjk_collision_time<A: Support + Midpoint, B: Support + Midpoint>(shape_a: &A, speed_a: Vector3<f32>, shape_b: &B, speed_b: Vector3<f32>) -> Option<(f32, CollisionInfo)> {
//    unimplemented!()
//}

#[derive(Clone, Debug)]
struct Polytope {
    faces: Vec<Face>,
}

impl Polytope {
    pub fn from_simplex(simplex: Simplex) -> Polytope {
        debug_assert_eq!(simplex.dim, 3);
        let (a, b, c, d) = (
            simplex.vertices[3],
            simplex.vertices[2],
            simplex.vertices[1],
            simplex.vertices[0],
        );
        let faces = vec![
            Face::new([a, b, c]),
            Face::new([a, c, d]),
            Face::new([a, d, b]),
            Face::new([c, b, d]),
        ];
        let poly = Polytope { faces };
        #[cfg(test)]
        poly.check_faces_point_outwards();
        poly
    }

    #[cfg(test)]
    pub fn check_faces_point_outwards(&self) {
        let mut inwards = 0;
        for face in &self.faces {
            if face.normal.dot(Point3::origin() - face.vertices[0]) > TOLERANCE {
                inwards += 1;
            }
        }
        if inwards > 0 {
            panic!("{} polytope faces are pointing inwards!", inwards);
        }
    }

    pub fn closest_face(&self) -> &Face {
        let mut best_face = &self.faces[0];
        for face in self.faces[1..].iter() {
            if face.distance < best_face.distance {
                best_face = face;
            }
        }
        best_face
    }

    pub fn add_point(&mut self, point: Point3<f32>) {
        let mut edges: Vec<(Point3<f32>, Point3<f32>)> = Vec::new();
        fn add_edge(
            edges: &mut Vec<(Point3<f32>, Point3<f32>)>,
            start: Point3<f32>,
            end: Point3<f32>,
        ) {
            let len = edges.len();
            edges.retain(|&(a, b)| (a, b) != (end, start));
            if len == edges.len() {
                edges.push((start, end));
            }
        }
        self.faces.retain(|face| {
            let dot = face.normal.dot(point - face.vertices[0]);
            let cull_face = dot > 0.;
            if cull_face {
                let (a, b, c) = (face.vertices[0], face.vertices[1], face.vertices[2]);
                add_edge(&mut edges, a, b);
                add_edge(&mut edges, b, c);
                add_edge(&mut edges, c, a);
            }
            !cull_face
        });
        let new_faces = edges.into_iter().map(|e| Face::new([e.0, e.1, point]));
        self.faces.extend(new_faces);
        #[cfg(test)]
        self.check_faces_point_outwards();
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Face {
    pub vertices: [Point3<f32>; 3],
    pub normal: Vector3<f32>,
    pub distance: f32,
}

impl Face {
    pub fn new(vertices: [Point3<f32>; 3]) -> Face {
        let (a, b, c) = (vertices[0], vertices[1], vertices[2]);
        #[cfg(test)]
        assert!(
            a != b && b != c,
            "There are overlapping face vertices! {:?}",
            vertices
        );
        let normal = (b - a).cross(c - a).normalize();
        let distance = a.dot(normal);
        #[cfg(test)]
        assert!(
            distance + TOLERANCE >= 0.,
            "face.distance is smaller than zero! ( {} )",
            distance
        );
        Face {
            vertices,
            normal,
            distance,
        }
    }
}

pub fn epa<A: Support>(shape: &A, simplex: Simplex) -> Face {
    let mut poly = Polytope::from_simplex(simplex);
    let mut iterations = 0;
    loop {
        let support_point = {
            let face = poly.closest_face();
            let dir = face.normal;
            let support_point = shape.support_point(dir);
            let dot = support_point.dot(face.normal);
            if dot <= face.distance + TOLERANCE || iterations > EPA_MAX_ITERATIONS {
                return *face;
            }
            support_point
        };
        poly.add_point(support_point);
        iterations += 1;
    }
}
