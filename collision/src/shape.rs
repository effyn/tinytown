#![deny(missing_docs)]

use cgmath::{prelude::*, Point3, Quaternion, Vector3};

use super::{Aabb, Midpoint, Support};

/// An enum collecting various shapes together.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Shape {
    /// A cuboid with a specified radius
    Cuboid(Vector3<f32>),
    /// A sphere with a specified radius
    Sphere(f32),
}

impl Shape {
    /// Apply a transformation to the shape.
    pub fn with_transform(self, pos: Point3<f32>, rot: Quaternion<f32>) -> TransformedShape {
        TransformedShape {
            pos,
            rot,
            shape: self,
        }
    }

    /// Get the smallest AABB which contains this shape.
    pub fn aabb(self) -> Aabb {
        match self {
            Shape::Cuboid(radius) => {
                Aabb::new(Point3::origin() - radius, Point3::origin() + radius)
            }
            Shape::Sphere(radius) => {
                let radius = Vector3::new(radius, radius, radius);
                Aabb::new(Point3::origin() - radius, Point3::origin() + radius)
            }
        }
    }
}

impl Midpoint for Shape {
    #[inline(always)]
    fn midpoint(&self) -> Point3<f32> {
        Point3::origin()
    }
}

impl Support for Shape {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        fn nonzero_signum(x: f32) -> f32 {
            let s = x.signum();
            if s != 0. {
                s
            } else {
                1.
            }
        }
        match *self {
            Shape::Cuboid(radius) => Point3::new(
                radius.x * nonzero_signum(dir.x),
                radius.y * nonzero_signum(dir.y),
                radius.z * nonzero_signum(dir.z),
            ),
            Shape::Sphere(radius) => Point3::origin() + dir.normalize() * radius,
        }
    }
}

/// A shape with a transformation applied to it.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct TransformedShape {
    /// The position of the center point of the shape
    pub pos: Point3<f32>,
    /// The rotation of the shape
    pub rot: Quaternion<f32>,
    /// The un-transformed shape
    pub shape: Shape,
}

impl TransformedShape {
    /// Get the smallest AABB which contains this shape.
    pub fn aabb(self) -> Aabb {
        let mut aabb = self.shape.aabb();
        aabb += self.pos.to_vec();
        // TODO: deal with rotations
        aabb
    }
}

impl Support for TransformedShape {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        self.pos + self.rot * self.shape.support_point(self.rot.invert() * dir).to_vec()
    }
}

impl Midpoint for TransformedShape {
    fn midpoint(&self) -> Point3<f32> {
        self.pos + self.shape.midpoint().to_vec()
    }
}
