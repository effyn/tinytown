use cgmath::{Vector2, Vector3};

pub trait EuclidElementWise {
    fn div_euclid_element_wise(self, rhs: Self) -> Self;
    fn rem_euclid_element_wise(self, rhs: Self) -> Self;
}

impl EuclidElementWise for Vector3<f32> {
    fn div_euclid_element_wise(self, rhs: Vector3<f32>) -> Vector3<f32> {
        Vector3::new(
            self.x.div_euclid(rhs.x),
            self.y.div_euclid(rhs.y),
            self.z.div_euclid(rhs.z),
        )
    }

    fn rem_euclid_element_wise(self, rhs: Vector3<f32>) -> Vector3<f32> {
        Vector3::new(
            self.x.rem_euclid(rhs.x),
            self.y.rem_euclid(rhs.y),
            self.z.rem_euclid(rhs.z),
        )
    }
}

impl EuclidElementWise for Vector2<f32> {
    fn div_euclid_element_wise(self, rhs: Vector2<f32>) -> Vector2<f32> {
        Vector2::new(self.x.div_euclid(rhs.x), self.y.div_euclid(rhs.y))
    }

    fn rem_euclid_element_wise(self, rhs: Vector2<f32>) -> Vector2<f32> {
        Vector2::new(self.x.rem_euclid(rhs.x), self.y.rem_euclid(rhs.y))
    }
}

impl EuclidElementWise for Vector3<i32> {
    fn div_euclid_element_wise(self, rhs: Vector3<i32>) -> Vector3<i32> {
        Vector3::new(
            self.x.div_euclid(rhs.x),
            self.y.div_euclid(rhs.y),
            self.z.div_euclid(rhs.z),
        )
    }

    fn rem_euclid_element_wise(self, rhs: Vector3<i32>) -> Vector3<i32> {
        Vector3::new(
            self.x.rem_euclid(rhs.x),
            self.y.rem_euclid(rhs.y),
            self.z.rem_euclid(rhs.z),
        )
    }
}
