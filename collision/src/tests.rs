use {
    approx::{assert_ulps_eq, ulps_eq},
    cgmath::{prelude::*, Point3, Quaternion, Rad, Vector3},
    core::{f32::consts::PI, ops::Range},
    proptest::{prelude::*, test_runner::Config},
    std::collections::HashSet,
    tinytown_testutil::{prop_point2_all, prop_point3_all, prop_vec3_all, prop_vec3_unit},
};

use crate::consts::TOLERANCE;

use super::{
    broad::bvh::Bvh,
    narrow::gjk::{gjk, gjk_collide},
    Aabb, Contact, Midpoint, MinkowskiDifference, Ray, RaycastHit, Shape, Support,
    TransformedShape,
};

prop_compose! {
    fn prop_aabb_all
        (center_range: Range<f32>, radius_range: Range<f32>)
        (center in prop_point3_all(center_range), radius in prop_vec3_all(radius_range))
        -> Aabb {
        Aabb::new_centered(center, radius)
    }
}

fn rectangle(pos: Point3<f32>, radius: Vector3<f32>) -> TransformedShape {
    Shape::Cuboid(radius).with_transform(pos, Quaternion::one())
}

fn sphere(pos: Point3<f32>, radius: f32) -> TransformedShape {
    Shape::Sphere(radius).with_transform(pos, Quaternion::one())
}

const XPLUS: Vector3<f32> = Vector3 {
    x: 1.,
    y: 0.,
    z: 0.,
};
const XMINUS: Vector3<f32> = Vector3 {
    x: -1.,
    y: 0.,
    z: 0.,
};
const YPLUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 1.,
    z: 0.,
};
const YMINUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: -1.,
    z: 0.,
};
const ZPLUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 0.,
    z: 1.,
};
const ZMINUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 0.,
    z: -1.,
};
const AXES_WITH_LATERAL: [(Vector3<f32>, Vector3<f32>, Vector3<f32>); 6] = [
    (XPLUS, YPLUS, ZPLUS),
    (XMINUS, YPLUS, ZPLUS),
    (YPLUS, XPLUS, ZPLUS),
    (YMINUS, XPLUS, ZPLUS),
    (ZPLUS, XPLUS, YPLUS),
    (ZMINUS, XPLUS, YPLUS),
];

#[test]
fn aabb_ray_cast_works() {
    let unit_aabb = Aabb::new_centered(Point3::origin(), Vector3::new(1., 1., 1.));

    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(1., 0., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(0., 1., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(0., 0., 1.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(-1., 0., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(0., -1., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(Point3::origin(), Vector3::new(0., 0., -1.)))
        .is_some());

    let close_to_origin = Point3::new(0.1, -0.1, 0.2);

    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(1., 0., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(0., 1., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(0., 0., 1.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(-1., 0., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(0., -1., 0.)))
        .is_some());
    assert!(unit_aabb
        .cast_ray(Ray::new(close_to_origin, Vector3::new(0., 0., -1.)))
        .is_some());

    let surface_point_offsets = [
        // Center of faces
        Vector3::new(1., 0., 0.),
        Vector3::new(0., 1., 0.),
        Vector3::new(0., 0., 1.),
        Vector3::new(-1., 0., 0.),
        Vector3::new(0., -1., 0.),
        Vector3::new(0., 0., -1.),
        // Corners
        Vector3::new(1., 1., 1.),
        Vector3::new(-1., 1., 1.),
        Vector3::new(1., -1., 1.),
        Vector3::new(-1., -1., 1.),
        Vector3::new(1., 1., -1.),
        Vector3::new(-1., 1., -1.),
        Vector3::new(1., -1., -1.),
        Vector3::new(-1., -1., -1.),
    ];

    for &offset in &surface_point_offsets[..] {
        let sp = Point3::origin() + offset;
        let ray_origin: Point3<f32> = Point3::origin() + 3. * offset;
        let ray_dir = -offset.normalize();
        let distance = ray_origin.distance(sp);
        let ray = Ray::new(ray_origin, ray_dir);
        assert_ulps_eq!(unit_aabb.cast_ray(ray).unwrap(), distance);
    }

    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(1.1, 0., 0.),
            Vector3::new(0., -1., 0.)
        ))
        .is_none());
    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(0., 1.1, 0.),
            Vector3::new(0., 0., -1.)
        ))
        .is_none());
    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(0., 0., 1.1),
            Vector3::new(-1., 0., 0.)
        ))
        .is_none());
    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(-1.1, 0., 0.),
            Vector3::new(0., 1., 0.)
        ))
        .is_none());
    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(0., -1.1, 0.),
            Vector3::new(0., 0., 1.)
        ))
        .is_none());
    assert!(unit_aabb
        .cast_ray(Ray::new(
            Point3::new(0., 0., -1.1),
            Vector3::new(1., 0., 0.)
        ))
        .is_none());
}

#[test]
fn shape_support_points_rectangle() {
    let rect1 = Shape::Cuboid(Vector3::new(1., 2., 3.));

    assert_ulps_eq!(rect1.support_point(XPLUS).x, 1.);
    assert_ulps_eq!(rect1.support_point(YPLUS).y, 2.);
    assert_ulps_eq!(rect1.support_point(ZPLUS).z, 3.);
    assert_ulps_eq!(rect1.support_point(XMINUS).x, -1.);
    assert_ulps_eq!(rect1.support_point(YMINUS).y, -2.);
    assert_ulps_eq!(rect1.support_point(ZMINUS).z, -3.);

    assert_ulps_eq!(
        rect1.support_point(Vector3::new(1., -1., 1.)),
        Point3::new(1., -2., 3.)
    );

    let rect1t = rect1.with_transform(Point3::new(2., -3., 4.), Quaternion::one());

    assert_ulps_eq!(rect1t.support_point(XPLUS).x, 3.);
    assert_ulps_eq!(rect1t.support_point(YPLUS).y, -1.);
    assert_ulps_eq!(rect1t.support_point(ZPLUS).z, 7.);
    assert_ulps_eq!(rect1t.support_point(XMINUS).x, 1.);
    assert_ulps_eq!(rect1t.support_point(YMINUS).y, -5.);
    assert_ulps_eq!(rect1t.support_point(ZMINUS).z, 1.);

    assert_ulps_eq!(
        rect1t.support_point(Vector3::new(1., -1., -1.)),
        Point3::new(3., -5., 1.)
    );

    let rect1r = rect1.with_transform(
        Point3::new(0., 0., 0.),
        Quaternion::from_angle_x(Rad(PI * 0.5)),
    );

    assert_ulps_eq!(rect1r.support_point(XPLUS).x, 1.);
    assert_ulps_eq!(rect1r.support_point(YPLUS).y, 3.);
    assert_ulps_eq!(rect1r.support_point(ZPLUS).z, 2.);
    assert_ulps_eq!(rect1r.support_point(XMINUS).x, -1.);
    assert_ulps_eq!(rect1r.support_point(YMINUS).y, -3.);
    assert_ulps_eq!(rect1r.support_point(ZMINUS).z, -2.);
}

#[test]
fn minkowski_difference_rectangle() {
    let rect1 = Shape::Cuboid(Vector3::new(1., 2., 3.));
    let rect2 = Shape::Cuboid(Vector3::new(2., 2., 4.));
    let diff = MinkowskiDifference::new(&rect1, &rect2);

    assert_ulps_eq!(diff.support_point(XPLUS).x, 3.);
    assert_ulps_eq!(diff.support_point(YPLUS).y, 4.);
    assert_ulps_eq!(diff.support_point(ZPLUS).z, 7.);
    assert_ulps_eq!(diff.support_point(XMINUS).x, -3.);
    assert_ulps_eq!(diff.support_point(YMINUS).y, -4.);
    assert_ulps_eq!(diff.support_point(ZMINUS).z, -7.);

    let rect1 = Shape::Cuboid(Vector3::new(2., 1., 1.))
        .with_transform(Point3::new(1., 1., 1.), Quaternion::one());
    let rect2 = Shape::Cuboid(Vector3::new(4., 1., 1.))
        .with_transform(Point3::new(3., 1., 1.), Quaternion::one());
    let diff = MinkowskiDifference::new(&rect1, &rect2);

    assert_ulps_eq!(diff.support_point(XPLUS).x, 4.);
    assert_ulps_eq!(diff.support_point(YPLUS).y, 2.);
    assert_ulps_eq!(diff.support_point(ZPLUS).z, 2.);
    assert_ulps_eq!(diff.support_point(XMINUS).x, -8.);
    assert_ulps_eq!(diff.support_point(YMINUS).y, -2.);
    assert_ulps_eq!(diff.support_point(ZMINUS).z, -2.);

    // TODO: test rotations
}

#[test]
fn gjk_works_rect() {
    let shape1 = rectangle(Point3::origin(), Vector3::new(1., 1., 1.));
    let shape2 = rectangle(Point3::new(5., 7., 3.), Vector3::new(1., 2., 1.));
    let shape3 = rectangle(Point3::new(5., -4., 3.), Vector3::new(1., 2., 1.));
    let shape4 = rectangle(Point3::new(0.9, 0., 0.), Vector3::new(1., 2., 3.));
    let shape5 = rectangle(Point3::new(0.8, 0.8, 0.), Vector3::new(1., 1., 3.));
    let shape6 = rectangle(Point3::new(0.3, 0.4, 0.7), Vector3::new(1., 1., 1.));
    let shape7 = rectangle(Point3::new(0.9, -0.6, 0.9), Vector3::new(1., 1., 1.));
    let shape8 = rectangle(Point3::new(-0.9, -0.8, -0.8), Vector3::new(1., 1., 1.));
    let shape9 = rectangle(Point3::new(-5., 0., 0.), Vector3::new(4., 1., 1.));
    let outppp = rectangle(Point3::new(2., 2., 2.), Vector3::new(1., 1., 1.));
    let outppn = rectangle(Point3::new(2., 2., -2.), Vector3::new(1., 1., 1.));
    let outpnp = rectangle(Point3::new(2., -2., 2.), Vector3::new(1., 1., 1.));
    let outpnn = rectangle(Point3::new(2., -2., -2.), Vector3::new(1., 1., 1.));
    let outnpp = rectangle(Point3::new(-2., 2., 2.), Vector3::new(1., 1., 1.));
    let outnpn = rectangle(Point3::new(-2., 2., -2.), Vector3::new(1., 1., 1.));
    let outnnp = rectangle(Point3::new(-2., -2., 2.), Vector3::new(1., 1., 1.));
    let outnnn = rectangle(Point3::new(-2., -2., -2.), Vector3::new(1., 1., 1.));
    let rect = rectangle(Point3::origin(), Vector3::new(20., 20., 20.));
    let empty = rectangle(Point3::origin(), Vector3::new(0., 0., 0.));
    assert!(gjk(&shape1, &empty).is_some());
    assert!(gjk(&shape2, &empty).is_none());
    assert!(gjk(&shape3, &empty).is_none());
    assert!(gjk(&shape1, &empty).is_some());
    assert!(gjk(&shape2, &empty).is_none());
    assert!(gjk(&shape3, &empty).is_none());
    assert!(gjk(&shape4, &empty).is_some());
    assert!(gjk(&shape5, &empty).is_some());
    assert!(gjk(&shape6, &empty).is_some());
    assert!(gjk(&shape7, &empty).is_some());
    assert!(gjk(&shape8, &empty).is_some());
    assert!(gjk(&shape9, &empty).is_none());
    assert!(gjk(&outppp, &empty).is_none());
    assert!(gjk(&outppn, &empty).is_none());
    assert!(gjk(&outpnp, &empty).is_none());
    assert!(gjk(&outpnn, &empty).is_none());
    assert!(gjk(&outnpp, &empty).is_none());
    assert!(gjk(&outnpn, &empty).is_none());
    assert!(gjk(&outnnp, &empty).is_none());
    assert!(gjk(&outnnn, &empty).is_none());
    assert!(gjk(&rect, &empty).is_some());
}

#[test]
fn gjk_works_sphere() {
    let shape1 = sphere(Point3::origin(), 1.);
    let shape2 = sphere(Point3::new(-0.25585842, -1.7270455, 1.6210499), 3.6433325);
    let shape3 = sphere(Point3::new(-0.83318996, -0.26156235, 0.0), 1.8025575);
    let shape4 = sphere(Point3::new(1.363091, -1.3061776, -1.4266706), 4.885711);
    let shape5 = sphere(Point3::new(1.8346295, -0.63550043, 0.6474104), 3.5623746);
    let shape6 = sphere(Point3::new(-1.7397628, 0.63715553, 1.0508142), 3.596167);
    let empty = sphere(Point3::origin(), 0.);
    assert!(gjk(&shape1, &empty).is_some());
    assert!(gjk(&shape2, &empty).is_some());
    assert!(gjk(&shape3, &empty).is_some());
    assert!(gjk(&shape4, &empty).is_some());
    assert!(gjk(&shape5, &empty).is_some());
    assert!(gjk(&shape6, &empty).is_some());
    assert!(gjk(&shape1, &shape1).is_some());
    assert!(gjk(&shape1, &shape2).is_some());
    assert!(gjk(&shape1, &shape3).is_some());
    assert!(gjk(&shape1, &shape4).is_some());
    assert!(gjk(&shape1, &shape5).is_some());
    assert!(gjk(&shape1, &shape6).is_some());
    assert!(gjk(&shape2, &shape1).is_some());
    assert!(gjk(&shape2, &shape2).is_some());
    assert!(gjk(&shape2, &shape3).is_some());
    assert!(gjk(&shape2, &shape4).is_some());
    assert!(gjk(&shape2, &shape5).is_some());
    assert!(gjk(&shape2, &shape6).is_some());
    assert!(gjk(&shape3, &shape1).is_some());
    assert!(gjk(&shape3, &shape2).is_some());
    assert!(gjk(&shape3, &shape3).is_some());
    assert!(gjk(&shape3, &shape4).is_some());
    assert!(gjk(&shape3, &shape5).is_some());
    assert!(gjk(&shape3, &shape6).is_some());
    assert!(gjk(&shape4, &shape1).is_some());
    assert!(gjk(&shape4, &shape2).is_some());
    assert!(gjk(&shape4, &shape3).is_some());
    assert!(gjk(&shape4, &shape4).is_some());
    assert!(gjk(&shape4, &shape5).is_some());
    assert!(gjk(&shape4, &shape6).is_some());
    assert!(gjk(&shape5, &shape1).is_some());
    assert!(gjk(&shape5, &shape2).is_some());
    assert!(gjk(&shape5, &shape3).is_some());
    assert!(gjk(&shape5, &shape4).is_some());
    assert!(gjk(&shape5, &shape5).is_some());
    // TODO: actually handle spheres properly
    // assert!(gjk(&shape5, &shape6).is_some());
    assert!(gjk(&shape6, &shape1).is_some());
    assert!(gjk(&shape6, &shape2).is_some());
    assert!(gjk(&shape6, &shape3).is_some());
    assert!(gjk(&shape6, &shape4).is_some());
    // TODO: actually handle spheres properly
    // assert!(gjk(&shape6, &shape5).is_some());
    assert!(gjk(&shape6, &shape6).is_some());
}

#[test]
fn gjk_works_rect_sphere() {
    let sphere1 = sphere(Point3::new(1., 1., 1.), 0.5);
    let rect1 = rectangle(Point3::new(1., 2., 1.), Vector3::new(0.5, 1., 0.25));
    assert!(gjk(&sphere1, &rect1).is_some());
    assert!(gjk(&rect1, &sphere1).is_some());
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_spheres_gjk(radius in 0.5f32..5., center in prop_point3_all(-2. .. 2.)) {
        let shape = sphere(center, radius);
        let empty = sphere(Point3::origin(), 0.);
        let center_dist = (Point3::origin() - center).magnitude();
        let contains_origin_stricter = center_dist < radius - TOLERANCE;
        if contains_origin_stricter {
            assert!(gjk(&shape, &empty).is_some());
        }
        let contains_origin_looser = center_dist < radius + TOLERANCE;
        if !contains_origin_looser {
            assert!(gjk(&shape, &empty).is_none());
        }
    }
}

// TODO: actually handling spheres properly
//proptest! {
//    #![proptest_config(Config::with_cases(3000))]
//    #[test]
//    fn proptest_two_spheres_gjk(
//        radius_a in 0.5f32..5., center_a in prop_point3_all(-2. .. 2.),
//        radius_b in 0.5f32..5., center_b in prop_point3_all(-2. .. 2.),
//    ) {
//        let shape_a = sphere(center_a, radius_a);
//        let shape_b = sphere(center_b, radius_b);
//        let center_diff = center_b - center_a;
//        let center_dist = dbg!(center_diff.magnitude());
//        let radius_sum = dbg!(radius_a + radius_b);
//        let intersects_stricter = center_dist + TOLERANCE < radius_sum;
//        if intersects_stricter {
//            assert!(gjk(&shape_a, &shape_b).is_some(), "shape_a intersects shape_b, but gjk says it does not");
//            assert!(gjk(&shape_b, &shape_a).is_some(), "shape_b intersects shape_a, but gjk says it does not");
//        }
//        let intersects_looser = center_dist - TOLERANCE < radius_sum;
//        if !intersects_looser {
//            assert!(gjk(&shape_a, &shape_b).is_none(), "shape_a does not intersect shape_b, but gjk says it does");
//            assert!(gjk(&shape_b, &shape_a).is_none(), "shape_b does not intersect shape_a, but gjk says it does");
//        }
//    }
//}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_rects_gjk(center in prop_point3_all(-2. .. 2.), radius in prop_vec3_all(0.1 .. 2.)) {
        let min = center - radius;
        let max = center + radius;
        let shape = rectangle(center, radius);
        let empty = rectangle(Point3::origin(), Vector3::new(0., 0., 0.));
        let contains_origin_stricter =
            (min.x + TOLERANCE < 0. && max.x - TOLERANCE > 0.) &&
            (min.y + TOLERANCE < 0. && max.y - TOLERANCE > 0.) &&
            (min.z + TOLERANCE < 0. && max.z - TOLERANCE > 0.);
        if contains_origin_stricter {
            assert!(gjk(&shape, &empty).is_some());
        }
        let contains_origin_looser =
            (min.x - TOLERANCE < 0. && max.x + TOLERANCE > 0.) &&
            (min.y - TOLERANCE < 0. && max.y + TOLERANCE > 0.) &&
            (min.z - TOLERANCE < 0. && max.z + TOLERANCE > 0.);
        if !contains_origin_looser {
            assert!(gjk(&shape, &empty).is_none());
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_two_rects_gjk(
        center_a in prop_point3_all(-2. .. 2.), radius_a in prop_vec3_all(0.1 .. 2.),
        center_b in prop_point3_all(-2. .. 2.), radius_b in prop_vec3_all(0.1 .. 2.),
    ) {
        let center_diff = center_b - center_a;
        let center_dist = center_diff.map(f32::abs);
        let radius_sum = radius_a + radius_b;
        let shape_a = rectangle(center_a, radius_a);
        let shape_b = rectangle(center_b, radius_b);
        let contains_origin_stricter =
            (center_dist.x + TOLERANCE < radius_sum.x) &&
            (center_dist.y + TOLERANCE < radius_sum.y) &&
            (center_dist.z + TOLERANCE < radius_sum.z);
        if contains_origin_stricter {
            assert!(gjk(&shape_a, &shape_b).is_some());
            assert!(gjk(&shape_b, &shape_a).is_some());
        }
        let contains_origin_looser =
            (center_dist.x - TOLERANCE < radius_sum.x) &&
            (center_dist.y - TOLERANCE < radius_sum.y) &&
            (center_dist.z - TOLERANCE < radius_sum.z);
        if !contains_origin_looser {
            assert!(gjk(&shape_a, &shape_b).is_none());
            assert!(gjk(&shape_b, &shape_a).is_none());
        }
    }
}

fn gjk_collide_test<A, B>(shape_a: &A, shape_b: &B) -> Option<Contact>
where
    A: Support + Midpoint,
    B: Support + Midpoint,
{
    let fwd = gjk_collide(shape_a, shape_b);
    let bwd = gjk_collide(shape_b, shape_a);
    assert_eq!(fwd.is_some(), bwd.is_some());
    fwd
}

#[test]
fn gjk_collision_works() {
    let rect1 = rectangle(Point3::new(1., 0., 0.), Vector3::new(2., 1., 1.));
    let rect2 = rectangle(Point3::new(3., 1., 0.), Vector3::new(4., 2., 2.));
    let rect3 = rectangle(Point3::new(6., 0., 0.), Vector3::new(2., 1., 1.));
    let rect4 = rectangle(Point3::new(0., 0., 0.), Vector3::new(10., 10., 10.));
    let rect5 = rectangle(Point3::new(3., 1., 0.), Vector3::new(4., 1., 2.));

    // Rect1 collides with rect2, rect2 collides with rect3, rect3 does not collide with rect1.
    assert!(gjk_collide_test(&rect1, &rect2).is_some());
    assert!(gjk_collide_test(&rect2, &rect3).is_some());
    assert!(gjk_collide_test(&rect1, &rect3).is_none());

    // Rect4 collides with everything.
    assert!(gjk_collide_test(&rect1, &rect4).is_some());
    assert!(gjk_collide_test(&rect2, &rect4).is_some());
    assert!(gjk_collide_test(&rect3, &rect4).is_some());
    assert!(gjk_collide_test(&rect4, &rect1).is_some());
    assert!(gjk_collide_test(&rect4, &rect2).is_some());
    assert!(gjk_collide_test(&rect4, &rect3).is_some());
    assert!(gjk_collide_test(&rect4, &rect4).is_some());

    // Rect7 collides with rect1, rect2 and rect3.
    assert!(gjk_collide_test(&rect1, &rect5).is_some());
    assert!(gjk_collide_test(&rect2, &rect5).is_some());
    assert!(gjk_collide_test(&rect3, &rect5).is_some());
}

#[test]
fn gjk_collision_works_detailed() {
    fn gjk_collide_test_detailed<A, B>(shape_a: &A, shape_b: &B, normal: Vector3<f32>, depth: f32)
    where
        A: Support + Midpoint,
        B: Support + Midpoint,
    {
        let fwd = gjk_collide(shape_a, shape_b).unwrap();
        let bwd = gjk_collide(shape_b, shape_a).unwrap();
        assert_ulps_eq!(fwd.normal.magnitude(), 1.);
        assert_ulps_eq!(bwd.normal.magnitude(), 1.);
        assert!(fwd.depth >= 0., "{} < 0", fwd.depth);
        assert!(bwd.depth >= 0., "{} < 0", bwd.depth);
        assert_ulps_eq!(fwd.depth, bwd.depth);
        assert_ulps_eq!(fwd.normal, normal);
        assert_ulps_eq!(fwd.depth, depth);
    }

    gjk_collide_test_detailed(
        &rectangle(Point3::new(0., 0., 0.), Vector3::new(1., 1., 1.)),
        &rectangle(Point3::new(1., 0., 0.), Vector3::new(1., 1., 1.)),
        Vector3::new(1., 0., 0.),
        1.,
    );
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_rect_rect_collision(
        pos_a in prop_vec3_all(-2. .. 2.),
        pos_b in prop_vec3_all(-2. .. 2.),
        radius_a in prop_vec3_all(0.1 .. 2.),
        radius_b in prop_vec3_all(0.1 .. 2.),
    ) {
        let dx = (pos_b - pos_a).map(|x| x.abs());
        let rsum = radius_a + radius_b;
        let collides = dx.x < rsum.x && dx.y < rsum.y && dx.z < rsum.z;
        let shape_a = rectangle(Point3::from_vec(pos_a), radius_a);
        let shape_b = rectangle(Point3::from_vec(pos_b), radius_b);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert_eq!(result_ab.is_some(), collides);
        assert_eq!(result_ba.is_some(), collides);
        if let (Some(rab), Some(rba)) = (result_ab, result_ba) {
            assert_ulps_eq!(rab.depth, rba.depth);
            assert!(rab.depth > 0.);
            assert!(rba.depth > 0.);
            assert_ulps_eq!(rab.normal.magnitude(), 1.);
            assert_ulps_eq!(rba.normal.magnitude(), 1.);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_sphere_sphere_collision(
        pos_a in prop_vec3_all(-2. .. 2.),
        pos_b in prop_vec3_all(-2. .. 2.),
        radius_a in 0.1f32 .. 2.,
        radius_b in 0.1f32 .. 2.,
    ) {
        let dx = (pos_b - pos_a).map(|x| x.abs());
        let rsum = radius_a + radius_b;
        let collides = dx.magnitude() < rsum;
        let shape_a = sphere(Point3::from_vec(pos_a), radius_a);
        let shape_b = sphere(Point3::from_vec(pos_b), radius_b);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert_eq!(result_ab.is_some(), collides);
        assert_eq!(result_ba.is_some(), collides);
        if let (Some(rab), Some(rba)) = (result_ab, result_ba) {
            assert_ulps_eq!(rab.depth, rba.depth, epsilon = TOLERANCE);
            assert!(rab.depth > 0.);
            assert!(rba.depth > 0.);
            assert_ulps_eq!(rab.normal.magnitude(), 1.);
            assert_ulps_eq!(rba.normal.magnitude(), 1.);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_sphere_rect_collision_guaranteed(
        sphere_center in prop_vec3_all(-2. .. 2.),
        rect_offset_t in 0.0f32 .. 0.9,
        rect_offset_dir in prop_vec3_unit(),
        sphere_radius in 0.1f32 .. 2.,
        rect_radius in prop_vec3_all(0.1 .. 2.),
    ) {
        let rect_center = sphere_center + rect_offset_dir * (sphere_radius * rect_offset_t);
        let shape_a = rectangle(Point3::from_vec(rect_center), rect_radius);
        let shape_b = sphere(Point3::from_vec(sphere_center), sphere_radius);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert!(result_ab.is_some());
        assert!(result_ba.is_some());
        let rab = result_ab.unwrap();
        let rba = result_ba.unwrap();
        assert_ulps_eq!(rab.depth, rba.depth, epsilon = TOLERANCE);
        assert!(rab.depth > 0.);
        assert!(rba.depth > 0.);
        assert_ulps_eq!(rab.normal.magnitude(), 1.);
        assert_ulps_eq!(rba.normal.magnitude(), 1.);
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ulps_eq_reflexive(
        aabb in prop_aabb_all(-2. .. 2., 0.001 .. 2.),
    ) {
        assert_ulps_eq!(aabb, aabb);
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_intersects_and_intersection_agree(
        aabb_a in prop_aabb_all(-2. .. 2., 0.001 .. 2.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.001 .. 2.),
    ) {
        assert_eq!(aabb_a.intersects(aabb_b), aabb_a.intersection(aabb_b).is_some());
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_normal_at_and_vector_to_center_do_not_deviate_too_much(
        aabb in prop_aabb_all(-2. .. 2., 0.0001 .. 0.2),
        point in prop_point3_all(-10. .. 10.),
    ) {
        if aabb.center().distance(point) > 0.0001 {
            let diff = (point - aabb.center()).div_element_wise(aabb.size()).normalize();
            assert!(aabb.normal_at(point).dot(diff) > 0.5);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ray_cast_to_center_always_succeeds(
        aabb in prop_aabb_all(-2. .. 2., 0.0001 .. 0.2),
        ray_origin in prop_point3_all(-10. .. 10.),
    ) {
        if aabb.center().distance(ray_origin) > 0.0001 {
            let ray_dir = (aabb.center() - ray_origin).normalize();
            let ray = Ray::new(ray_origin, ray_dir);
            assert!(aabb.cast_ray(ray).is_some());
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ray_cast_away_from_center_always_fails_if_outside(
        aabb in  prop_aabb_all(-2. .. 2., 0.1 .. 2.),
        ray_origin in prop_point3_all(-10. .. 10.),
    ) {
        let ray_dir = (ray_origin - aabb.center()).normalize();
        let ray = Ray::new(ray_origin, ray_dir);
        if aabb.contains_point(ray.origin) {
            assert!(aabb.cast_ray(ray).is_some());
        }
        else {
            assert!(aabb.cast_ray(ray).is_none());
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ray_cast_other_direction(
        aabb in  prop_aabb_all(-2. .. 2., 0.1 .. 2.),
        ray_origin in prop_point3_all(-10. .. 10.),
        ray_dir in prop_vec3_unit(),
    ) {
        let ray = Ray::new(ray_origin, ray_dir);
        if let Some(t) = aabb.cast_ray(ray) {
            let opposite_ray = Ray::new(ray.point_at(t * 2.), -ray_dir);
            assert!(aabb.cast_ray(opposite_ray).is_some());
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ray_cast_inner_point(
        aabb in  prop_aabb_all(-2. .. 2., 0.1 .. 2.),
        ray_origin in prop_point3_all(-10. .. 10.),
        target_offset in prop_vec3_all(-1. .. 1.),
    ) {
        let target = aabb.center() + aabb.radius().mul_element_wise(target_offset);
        if !ulps_eq!(target, ray_origin) {
            let ray_delta = target - ray_origin;
            let ray_dir = ray_delta.normalize();
            let ray = Ray::new(ray_origin, ray_dir);
            let ray_delta_mag = ray_delta.magnitude();
            assert!(aabb.cast_ray(ray).unwrap() <= ray_delta_mag);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_ray_cast_axis_aligned(
        aabb in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        ray_origin_2d in prop_point2_all(-4. .. 4.),
        ray_origin_distance in 2f32 .. 10.,
    ) {
        for &(axis, axis_lateral_a, axis_lateral_b) in &AXES_WITH_LATERAL[..] {
            eprintln!(" --- axis {:?} ---", axis);
            eprintln!("axis-lateral");
            eprintln!("  a={:?}", axis_lateral_a);
            eprintln!("  b={:?}", axis_lateral_b);
            let lateral_a_min = aabb.min.to_vec().dot(axis_lateral_a);
            let lateral_b_min = aabb.min.to_vec().dot(axis_lateral_b);
            let lateral_a_max = aabb.max.to_vec().dot(axis_lateral_a);
            let lateral_b_max = aabb.max.to_vec().dot(axis_lateral_b);
            eprintln!("aabb-extents:");
            eprintln!("  min={:?}", aabb.min);
            eprintln!("  max={:?}", aabb.max);
            eprintln!("aabb-lateral-extents:");
            eprintln!("  a={} .. {}", lateral_a_min, lateral_a_max);
            eprintln!("  b={} .. {}", lateral_b_min, lateral_b_max);
            let ray_origin_ = Point3::origin() + ray_origin_distance * axis;
            let lateral_offset = axis_lateral_a * ray_origin_2d.x + axis_lateral_b * ray_origin_2d.y;
            let ray_origin = ray_origin_ + lateral_offset;
            let ray_dir = -axis;
            let ray = Ray::new(ray_origin, ray_dir);
            eprintln!("ray {:?}", ray);
            let cast_t = aabb.cast_ray(ray);
            eprintln!("cast {:?}", cast_t);
            if    ray_origin_2d.x > lateral_a_min + TOLERANCE
               && ray_origin_2d.y > lateral_b_min + TOLERANCE
               && ray_origin_2d.x < lateral_a_max - TOLERANCE
               && ray_origin_2d.y < lateral_b_max - TOLERANCE {
                assert!(cast_t.is_some());
            }
            else if    ray_origin_2d.x < lateral_a_min - TOLERANCE
                    || ray_origin_2d.y < lateral_b_min - TOLERANCE
                    || ray_origin_2d.x > lateral_a_max + TOLERANCE
                    || ray_origin_2d.y > lateral_b_max + TOLERANCE {
                assert!(cast_t.is_none());
            }
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_union_associative(
        aabb_a in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_c in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
    ) {
        let union_ab_c = aabb_a.union(aabb_b).union(aabb_c);
        let union_a_bc = aabb_a.union(aabb_b.union(aabb_c));

        assert_ulps_eq!(union_ab_c, union_a_bc, epsilon = 0.000001); // TODO: figure out where the numerical instability comes from
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_union_commutes(
        aabb_a in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
    ) {
        let union_ab = aabb_a.union(aabb_b);
        let union_ba = aabb_b.union(aabb_a);

        assert_ulps_eq!(union_ab, union_ba);
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_intersection_associative(
        aabb_a in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_c in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
    ) {
        let intersection_ab_c = aabb_a.intersection(aabb_b).and_then(|r| r.intersection(aabb_c));
        let intersection_a_bc = aabb_b.intersection(aabb_c).and_then(|r| aabb_a.intersection(r));

        assert_eq!(intersection_ab_c.is_some(), intersection_a_bc.is_some());
        if intersection_ab_c.is_some() {
            assert_ulps_eq!(intersection_ab_c.unwrap(), intersection_a_bc.unwrap(), epsilon = 0.000001); // TODO: figure out where the numerical instability comes from
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_intersection_commutes(
        aabb_a in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
    ) {
        let intersection_ab = aabb_a.intersection(aabb_b);
        let intersection_ba = aabb_b.intersection(aabb_a);

        assert_eq!(intersection_ab.is_some(), intersection_ba.is_some());
        if intersection_ab.is_some() {
            assert_ulps_eq!(intersection_ab.unwrap(), intersection_ba.unwrap());
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_aabb_contains_implies_intersection(
        aabb_a in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
        aabb_b in prop_aabb_all(-2. .. 2., 0.0001 .. 3.),
    ) {
        if aabb_a.contains_aabb(aabb_b) {
            assert!(aabb_a.intersects(aabb_b));
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_bvh_query_aabb_works(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
        query_aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
    ) {
        use crate::broad::bvh::AabbQueryCache;
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        let mut cache = AabbQueryCache::new();
        for &query_aabb in &query_aabbs {
            let mut results = [false; 32];
            for i in bvh.query_aabb(&mut cache, query_aabb) {
                results[i] = true;
            }
            for (i, &aabb) in aabbs.iter().enumerate() {
                assert_eq!(aabb.intersects(query_aabb), results[i]);
            }
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_raycast_works(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
        ray_data in proptest::collection::vec((prop_point3_all(-4. .. 4.), prop_vec3_unit()), 0..32),
    ) {
        let rays: Vec<Ray> = ray_data.into_iter().map(|(origin, dir)| Ray::new(origin, dir)).collect();
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        for &ray in &rays {
            let narrow_test = |idx| {
                let aabb: Aabb = aabbs[idx];
                let distance = aabb.cast_ray(ray).unwrap();
                let point = ray.point_at(distance);
                let normal = aabb.normal_at(point);
                Some(RaycastHit { point, normal, distance, value: idx })
            };
            let bvh_hit_opt = bvh.cast_ray(ray, 100., narrow_test);
            let brute_force_hit_opt = {
                let mut distance = 101.;
                let mut hit = None;
                for (idx, &aabb) in aabbs.iter().enumerate() {
                    if let Some(d) = aabb.cast_ray(ray) {
                        if d < distance {
                            distance = d;
                            let point = ray.point_at(distance);
                            let normal = aabb.normal_at(point);
                            hit = Some(RaycastHit {
                                point,
                                normal,
                                distance,
                                value: idx,
                            });
                        }
                    }
                }
                hit
            };
            match (bvh_hit_opt, brute_force_hit_opt) {
                (Some(bvh_hit), Some(brute_force_hit)) => {
                    // can't make any assumptions about what they hit or the normals
                    // this is because there are degenerate cases when faces coincide
                    assert_ulps_eq!(bvh_hit.point, brute_force_hit.point);
                    assert_ulps_eq!(bvh_hit.distance, brute_force_hit.distance);
                },
                (None, None) => (),
                (a, b) => panic!("bvh and brute force hits don't match: bvh={:?} brute_force={:?}", a, b),
            }
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_colliding_pairs_works(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
    ) {
        use crate::broad::bvh::CollidingPairsCache;
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        let mut collision_matrix = [[false; 32]; 32];
        let mut cache = CollidingPairsCache::new();
        for (i, j) in bvh.query_colliding_pairs(&mut cache) {
            // TODO: maybe check for duplicates?
            collision_matrix[i][j] = true;
            collision_matrix[j][i] = true;
        }
        for i in 0..aabbs.len() {
            for j in 0..aabbs.len() {
                if i == j { continue; }
                let aabb_a = aabbs[i];
                let aabb_b = aabbs[j];
                let correct_test = aabb_a.intersects(aabb_b);
                let bvh_test = collision_matrix[i][j];
                if correct_test != bvh_test {
                    bvh.debug_print();
                    panic!("aabb {} vs aabb {} collision results don't match", i, j);
                }
            }
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_check_parents_contain_children(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
    ) {
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        bvh.debug_print();
        bvh.check_parents_contain_children();
    }
}

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_check_parent_child_parent(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
    ) {
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        bvh.debug_print();
        bvh.check_parent_child_relationship();
    }
}

const RANGE_0_32: [usize; 32] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32,
];

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_remove_maintains_invariants(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
        remove_order in Just(RANGE_0_32.to_owned()).prop_shuffle(),
    ) {
        let mut bvh = Bvh::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
        }
        bvh.debug_print();
        bvh.check_invariants();
        for &i in &remove_order {
            if i >= aabbs.len() {
                continue;
            }
            eprintln!("removing {}", i);
            let aabb = aabbs[i];
            assert!(bvh.remove(aabb, i));
            bvh.debug_print();
            bvh.check_invariants();
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(300))]
    #[test]
    fn proptest_bvh_remove_works(
        aabbs in proptest::collection::vec(prop_aabb_all(-2. .. 2., 0.1 .. 2.), 0..32),
        remove_order in Just(RANGE_0_32.to_owned()).prop_shuffle(),
    ) {
        let mut bvh = Bvh::new();
        let mut hs = HashSet::new();
        for (i, &aabb) in aabbs.iter().enumerate() {
            bvh.add(aabb, i);
            hs.insert(i);
        }
        bvh.debug_print();
        bvh.check_invariants();
        for &i in &remove_order {
            if i >= aabbs.len() {
                continue;
            }
            eprintln!("removing {}", i);
            let aabb = aabbs[i];
            assert!(bvh.remove(aabb, i));
            hs.remove(&i);
            bvh.debug_print();
            let mut new_hs = hs.clone();
            for idx in bvh.iter() {
                assert!(new_hs.remove(idx));
            }
            assert!(new_hs.is_empty());
        }
    }
}

#[test]
fn bvh_remove_root_works() {
    let mut bvh = Bvh::new();
    let aabb = Aabb::new(Point3::new(0., 0., 0.), Point3::new(1., 1., 1.));
    bvh.add(aabb, 0);
    assert!(bvh.remove(aabb, 0));
    assert!(bvh.iter().next().is_none());
}
