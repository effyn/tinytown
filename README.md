# Tinytown

## What's this?

A game engine for a brick building game.

## Non-proprietary communications pledge

This project pledges to never require proprietary software to access the official communication channels.

It will also prefer decentralized over centralized communication software.

## Contact

Currently, there doesn't need to be a lot of communication, so you can just contact me personally.

In the future, there will be public MUC on XMPP.

## How do I run it?

These instructions are for Linux, but should also work for Windows and OSX, look below for notes.

For running the client or running in single-player mode:

```
cargo run --release --bin tinytown-client
```

For running the server:

```
cargo run --release --bin tinytown-server
```

Running the server is kind of useless, currently, because multiplayer doesn't work at all.

### Windows Info

Download and install CMake: https://cmake.org/download/

Make sure you have Git installed: https://gitforwindows.org/

Open `Git Bash` or any other terminal (Command Prompt, PowerShell) and enter the following commands
```
$ git clone https://gitlab.com/tinytown/tinytown.git
$ cd tinytown
$ cargo run --release --bin tinytown-client
```

### OSX Info (Thanks to MacMittens!)

This assumes you have homebrew installed and the bare minimum otherwise

Build instructions for MacOS 10.15:

#### Install cmake with brew

```
$ brew install cmake
```

#### Acquire the Crab. Copy and paste the command below and follow the instructions.

```
$ curl --proto ‘=https’ --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

#### Use git to download the repository locally

```
git clone https://gitlab.com/tinytown/tinytown.git
```

#### cd into the new `tinytown` directory

```
$ cd tinytown
```

#### Compile and run tinytown by running this command:

```
$ cargo run --release --bin tinytown-client
```

Once it compiles it should start right up. Exit using the esc key.

## What are the default controls?

Check `game.toml`. If you use a non-qwerty keyboard layout, this can be quite inaccurate, though.

I do need to fix that.

## What's planned for the future?

No idea, maybe I will actually make something cool, but most likely I'll get disinterested and
abandon this.

If I do continue:

  - More face culling, since there are a lot of optimizations possible there
  - Multiplayer
  - Loading .obj models
  - Special bricks
  - Better collision detection:
    - Collision between two non-brick entities
    - Time-of-impact, so you can't run through walls when you're fast enough
  - UI
  - XMPP integration?

## What license is this under?

GNU AGPLv3 or later
