# Seeds for failure cases proptest has generated in the past. It is
# automatically read and these particular cases re-run before any
# novel cases are generated.
#
# It is recommended to check this file in to source control so that
# everyone who runs the test benefits from these saved cases.
cc 937e0378ef52ecfdb72452bfb42bfe23d282b2bcbe0cfd6baf6e8aff97fb55b4 # shrinks to vec_a = Vector3 [0.9994402, 0.03345521, 0.0]
cc a9e374845f2247582e3b8221cab31a11fb944ac1ea66b1a5ece00491b410b825 # shrinks to vec_a = Vector3 [-0.05892454, 0.99826247, -0.0]
cc 0684ab92a499cd74d95faa9d33d2cb446b146d2e4bd6e7107b2562b67d0707fe # shrinks to vec_a = Vector3 [0.01735107, 0.99984944, 0.0]
cc 4109c9f22812650909e06535e55e1e9b1816aa6eb1cbbe62b1fca6adc6779a60 # shrinks to vec_a = Vector3 [-0.012465198, 0.99992234, -0.0]
cc 998d8c6c41083dd77d88a6e4bb57c22e2555a812190bc11cee1fb450478b2990 # shrinks to vec_a = Vector3 [0.7736851, 0.048617177, -0.63170236]
cc 3b2910bb44ddaeddb9d906524b71139317e7a2c17fad6c0cb7325d6cdcd9e66e # shrinks to vec_a = Vector3 [-0.37240857, 0.21870187, -0.901932]
cc 1715788c1f022f237909ccf37c8e5a5a1929c61d7333e134a6c341491fc1593f # shrinks to vec_a = Vector3 [-0.023047037, -0.9997004, 0.0082406765]
