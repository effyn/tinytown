use pigeon::{Dump, FrameReader, FrameTarget, FrameWriter, Grab, ShortStr};

use crate::PROTOCOL_VERSION;

pub type ClientCookie = [u8; 8];
pub type ServerChallenge = [u8; 8];

#[derive(Debug, Clone)]
pub enum FrameHeader<'a> {
    JoinRequest {
        client_cookie: ClientCookie,
    },
    JoinChallenge {
        challenge: ServerChallenge,
    },
    JoinResponse {
        client_cookie: ClientCookie,
        challenge: ServerChallenge,
        name: &'a str,
    },
    JoinAccept {
        server_tick: u32,
    },
    JoinReject {
        reason: JoinRejectReason,
    },
    Push {
        push_id: u32,
        last_push_id: u32,
        push_window: u32,
        payload_count: u8,
    },
}

impl<'a> Dump for FrameHeader<'a> {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(b't');
        writer.write(b't');
        writer.write(PROTOCOL_VERSION);
        match self {
            FrameHeader::JoinRequest { client_cookie } => {
                writer.write(1u8);
                writer.write_bytes(client_cookie);
            }
            FrameHeader::JoinChallenge { challenge } => {
                writer.write(2u8);
                writer.write_bytes(challenge);
            }
            FrameHeader::JoinResponse {
                client_cookie,
                challenge,
                name,
            } => {
                writer.write(3u8);
                writer.write_bytes(client_cookie);
                writer.write_bytes(challenge);
                writer.write(ShortStr(name));
            }
            FrameHeader::JoinAccept { server_tick } => {
                writer.write(4u8);
                writer.write(server_tick);
            }
            FrameHeader::JoinReject { reason } => {
                writer.write(5u8);
                writer.write(reason);
            }
            FrameHeader::Push {
                push_id,
                last_push_id,
                push_window,
                payload_count,
            } => {
                writer.write(6u8);
                writer.write(push_id);
                writer.write(last_push_id);
                writer.write(push_window);
                writer.write(payload_count);
            }
        }
    }
}

impl<'a> Grab<'a> for FrameHeader<'a> {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<FrameHeader<'a>> {
        reader.expect(b't')?;
        reader.expect(b't')?;
        reader.expect(PROTOCOL_VERSION)?;
        let frame_type_id: u8 = reader.read()?;
        match frame_type_id {
            1 => {
                let mut client_cookie = [0; 8];
                reader.read_bytes_to(&mut client_cookie[..])?;
                Some(FrameHeader::JoinRequest { client_cookie })
            }
            2 => {
                let mut challenge = [0; 8];
                reader.read_bytes_to(&mut challenge[..])?;
                Some(FrameHeader::JoinChallenge { challenge })
            }
            3 => {
                let mut client_cookie = [0; 8];
                let mut challenge = [0; 8];
                reader.read_bytes_to(&mut client_cookie[..])?;
                reader.read_bytes_to(&mut challenge[..])?;
                let ShortStr(name) = reader.read()?;
                Some(FrameHeader::JoinResponse {
                    client_cookie,
                    challenge,
                    name,
                })
            }
            4 => {
                let server_tick: u32 = reader.read()?;
                Some(FrameHeader::JoinAccept { server_tick })
            }
            5 => {
                let reason = reader.read()?;
                Some(FrameHeader::JoinReject { reason })
            }
            6 => {
                let push_id: u32 = reader.read()?;
                let last_push_id: u32 = reader.read()?;
                let push_window: u32 = reader.read()?;
                let payload_count: u8 = reader.read()?;
                Some(FrameHeader::Push {
                    push_id,
                    last_push_id,
                    push_window,
                    payload_count,
                })
            }
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum JoinRejectReason {
    Full = 1,
}

impl Dump for JoinRejectReason {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(*self as u8);
    }
}

impl<'a> Grab<'a> for JoinRejectReason {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<JoinRejectReason> {
        match reader.read::<u8>()? {
            1 => Some(JoinRejectReason::Full),
            _ => None,
        }
    }
}

impl JoinRejectReason {
    pub fn from_u8(byte: u8) -> Option<JoinRejectReason> {
        match byte {
            1 => Some(JoinRejectReason::Full),
            _ => None,
        }
    }
}
