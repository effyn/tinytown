use {
    cgmath::Vector3,
    pigeon::{Dump, FrameReader, FrameTarget, FrameWriter, Grab},
    std::f32::consts::PI,
};

pub struct LossyUnitVector(pub Vector3<f32>);

impl Dump for LossyUnitVector {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        let (x, y, z) = (self.0.x, self.0.y, self.0.z);
        let theta = z.atan2(x);
        let phi = y.atan2((x * x + z * z).sqrt());
        let theta_i8 = (theta * 128. / PI).trunc() as i8;
        let phi_i8 = (phi * 128. / PI).trunc() as i8;
        writer.write(theta_i8);
        writer.write(phi_i8);
    }
}

impl<'a> Grab<'a> for LossyUnitVector {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<LossyUnitVector> {
        let theta_i8: i8 = reader.read()?;
        let phi_i8: i8 = reader.read()?;
        let theta = theta_i8 as f32 * PI / 128.;
        let phi = phi_i8 as f32 * PI / 128.;
        let x = phi.cos() * theta.cos();
        let z = phi.cos() * theta.sin();
        let y = phi.sin();
        let vec = Vector3::new(x, y, z);
        Some(LossyUnitVector(vec))
    }
}

pub struct UnitVector(pub Vector3<f32>);

impl Dump for UnitVector {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(self.0.x);
        writer.write(self.0.y);
        writer.write(self.0.z);
    }
}

impl<'a> Grab<'a> for UnitVector {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<UnitVector> {
        let x = reader.read()?;
        let y = reader.read()?;
        let z = reader.read()?;
        Some(UnitVector(Vector3::new(x, y, z)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use {
        approx::assert_ulps_eq,
        cgmath::prelude::*,
        proptest::{proptest, test_runner::Config},
        std::f32::consts::PI,
        tinytown_testutil::prop_vec3_unit,
    };

    #[test]
    fn test_lossy_unit_vector_axes_exact() {
        const VECTORS: [Vector3<f32>; 6] = [
            Vector3::new(1., 0., 0.),
            Vector3::new(0., 1., 0.),
            Vector3::new(0., 0., 1.),
            Vector3::new(-1., 0., 0.),
            Vector3::new(0., -1., 0.),
            Vector3::new(0., 0., -1.),
        ];
        let mut buf = [0; 2];
        for &v in &VECTORS {
            FrameWriter::with(&mut buf[..], |writer| {
                writer.write(LossyUnitVector(v));
            });
            let mut reader = FrameReader::new(&buf[..]);
            let LossyUnitVector(rv) = reader.read().unwrap();
            assert_ulps_eq!(rv, rv.normalize());
            assert_ulps_eq!(rv, v);
        }
    }

    proptest! {
        #![proptest_config(Config::with_cases(3000))]
        #[test]
        fn proptest_lossy_unit_vector_correct(vec_a in prop_vec3_unit()) {
            let mut buf = [0; 2];
            FrameWriter::with(&mut buf[..], |writer| {
                writer.write(LossyUnitVector(vec_a));
            });
            let mut reader = FrameReader::new(&buf[..]);
            let LossyUnitVector(vec_b) = reader.read().unwrap();
            assert_ulps_eq!(vec_b, vec_b.normalize());
            let angle_between = vec_a.dot(vec_b).min(1.).max(-1.).acos();
            assert_ulps_eq!(angle_between, 0., epsilon = PI / 64.);
        }
    }
}
