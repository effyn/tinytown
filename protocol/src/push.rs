use pigeon::{Dump, FrameReader, FrameTarget, FrameWriter, Grab};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum PushKind {
    Event = 0,
    ClientTick = 1,
    Ghost = 2,
    ServerTick = 3,
}

impl Dump for PushKind {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(*self as u8);
    }
}

impl<'a> Grab<'a> for PushKind {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<PushKind> {
        let kind_u8 = reader.read::<u8>()?;
        match kind_u8 {
            0 => Some(PushKind::Event),
            1 => Some(PushKind::ClientTick),
            2 => Some(PushKind::Ghost),
            3 => Some(PushKind::ServerTick),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum PushHeader {
    Event { event_id: u32 },
    ClientTick { tick: u32, controls_count: u8 },
    Ghost { net_id: u32, components: u8 },
    ServerTick { tick: u32 },
}

impl Dump for PushHeader {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(self.kind());
        match self {
            PushHeader::Event { event_id } => {
                writer.write(event_id);
            }
            PushHeader::ClientTick {
                tick,
                controls_count,
            } => {
                writer.write(tick);
                writer.write(controls_count);
            }
            PushHeader::Ghost { net_id, components } => {
                writer.write(net_id);
                writer.write(components);
            }
            PushHeader::ServerTick { tick } => {
                writer.write(tick);
            }
        }
    }
}

impl<'a> Grab<'a> for PushHeader {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<PushHeader> {
        let kind = reader.read()?;
        match kind {
            PushKind::Event => {
                let event_id = reader.read()?;
                Some(PushHeader::Event { event_id })
            }
            PushKind::ClientTick => {
                let tick = reader.read()?;
                let controls_count = reader.read()?;
                Some(PushHeader::ClientTick {
                    tick,
                    controls_count,
                })
            }
            PushKind::Ghost => {
                let net_id = reader.read()?;
                let components = reader.read()?;
                Some(PushHeader::Ghost { net_id, components })
            }
            PushKind::ServerTick => {
                let tick = reader.read()?;
                Some(PushHeader::ServerTick { tick })
            }
        }
    }
}

impl PushHeader {
    pub fn kind(&self) -> PushKind {
        match self {
            PushHeader::Event { .. } => PushKind::Event,
            PushHeader::ClientTick { .. } => PushKind::ClientTick,
            PushHeader::Ghost { .. } => PushKind::Ghost,
            PushHeader::ServerTick { .. } => PushKind::ServerTick,
        }
    }
}
