pub use {
    luminance_glfw::{Action as GlfwAction, Key as GlfwKey, MouseButton as GlfwMouseButton},
    tinytown::controls::{Action, Key, MouseButton},
};

pub fn action_convert(action: GlfwAction) -> Option<Action> {
    match action {
        GlfwAction::Press => Some(Action::Press),
        GlfwAction::Release => Some(Action::Release),
        _ => None,
    }
}

pub fn mouse_button_convert(button: GlfwMouseButton) -> Option<MouseButton> {
    match button {
        GlfwMouseButton::Button1 => Some(MouseButton::Left),
        GlfwMouseButton::Button2 => Some(MouseButton::Right),
        GlfwMouseButton::Button3 => Some(MouseButton::Middle),
        _ => None,
    }
}

pub fn key_convert(key: GlfwKey) -> Option<Key> {
    match key {
        GlfwKey::A => Some(Key::A),
        GlfwKey::B => Some(Key::B),
        GlfwKey::C => Some(Key::C),
        GlfwKey::D => Some(Key::D),
        GlfwKey::E => Some(Key::E),
        GlfwKey::F => Some(Key::F),
        GlfwKey::G => Some(Key::G),
        GlfwKey::H => Some(Key::H),
        GlfwKey::I => Some(Key::I),
        GlfwKey::J => Some(Key::J),
        GlfwKey::K => Some(Key::K),
        GlfwKey::L => Some(Key::L),
        GlfwKey::M => Some(Key::M),
        GlfwKey::N => Some(Key::N),
        GlfwKey::O => Some(Key::O),
        GlfwKey::P => Some(Key::P),
        GlfwKey::Q => Some(Key::Q),
        GlfwKey::R => Some(Key::R),
        GlfwKey::S => Some(Key::S),
        GlfwKey::T => Some(Key::T),
        GlfwKey::U => Some(Key::U),
        GlfwKey::V => Some(Key::V),
        GlfwKey::W => Some(Key::W),
        GlfwKey::X => Some(Key::X),
        GlfwKey::Y => Some(Key::Y),
        GlfwKey::Z => Some(Key::Z),
        GlfwKey::Num0 => Some(Key::Num0),
        GlfwKey::Num1 => Some(Key::Num1),
        GlfwKey::Num2 => Some(Key::Num2),
        GlfwKey::Num3 => Some(Key::Num3),
        GlfwKey::Num4 => Some(Key::Num4),
        GlfwKey::Num5 => Some(Key::Num5),
        GlfwKey::Num6 => Some(Key::Num6),
        GlfwKey::Num7 => Some(Key::Num7),
        GlfwKey::Num8 => Some(Key::Num8),
        GlfwKey::Num9 => Some(Key::Num9),
        GlfwKey::Space => Some(Key::Space),
        GlfwKey::LeftBracket => Some(Key::LeftBracket),
        GlfwKey::RightBracket => Some(Key::RightBracket),
        GlfwKey::Semicolon => Some(Key::Semicolon),
        GlfwKey::LeftShift => Some(Key::LeftShift),
        GlfwKey::RightShift => Some(Key::RightShift),
        GlfwKey::Escape => Some(Key::Escape),
        _ => None,
    }
}
