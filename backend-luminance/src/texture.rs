use {
    image::{self, GenericImageView},
    luminance::{
        context::GraphicsContext,
        pixel::RGBA32F,
        texture::{CubeFace, Cubemap, Dim2, GenMipmaps, Sampler, Texture},
    },
    std::path::Path,
    tinytown::texture_manager::TextureLoader,
};

pub struct TexLoader<'a, C: GraphicsContext + 'a> {
    ctx: &'a mut C,
}

impl<'a, C: GraphicsContext + 'a> TexLoader<'a, C> {
    pub fn new(ctx: &'a mut C) -> TexLoader<'a, C> {
        TexLoader { ctx }
    }
}

fn map_pixel(rgba: image::Rgba<u8>) -> (f32, f32, f32, f32) {
    (
        rgba[0] as f32 / 255.,
        rgba[1] as f32 / 255.,
        rgba[2] as f32 / 255.,
        rgba[3] as f32 / 255.,
    )
}

fn to_texels(view: &impl GenericImageView<Pixel = image::Rgba<u8>>) -> Vec<(f32, f32, f32, f32)> {
    view.pixels().map(|(_, _, p)| map_pixel(p)).collect()
}

impl<'a, C: GraphicsContext + 'a> TextureLoader for TexLoader<'a, C> {
    type Texture = Texture<Dim2, RGBA32F>;

    fn load_texture(&mut self, path: impl AsRef<Path>) -> anyhow::Result<Self::Texture> {
        let path = path.as_ref();
        log::info!("loading texture {:?}", path);
        let img = image::open(path)?;
        let rgb_img = img.to_rgba();
        let (w, h) = rgb_img.dimensions();
        let texels = to_texels(&rgb_img);
        let tex = Texture::new(self.ctx, [w, h], 0, Sampler::default())
            .expect("failed to create texture"); // TODO: throw
        tex.upload(GenMipmaps::Yes, &texels)
            .expect("failed to upload texture data"); // TODO: throw
        Ok(tex)
    }
}

pub fn load_cubemap(
    ctx: &mut impl GraphicsContext,
    path: impl AsRef<Path>,
) -> anyhow::Result<Texture<Cubemap, RGBA32F>> {
    let path = path.as_ref();
    log::info!("loading texture {:?}", path);
    let img = image::open(path)?;
    let rgb_img = img.to_rgba();
    let (w, h) = rgb_img.dimensions();
    assert_eq!(w / 4, h / 3);
    let size = w / 4;
    let xplus_texels: Vec<_> = to_texels(&rgb_img.view(size, size, size, size));
    let xminus_texels: Vec<_> = to_texels(&rgb_img.view(3 * size, size, size, size));
    let yplus_texels: Vec<_> = to_texels(&rgb_img.view(size, 0, size, size));
    let yminus_texels: Vec<_> = to_texels(&rgb_img.view(size, 2 * size, size, size));
    let zplus_texels: Vec<_> = to_texels(&rgb_img.view(0, size, size, size));
    let zminus_texels: Vec<_> = to_texels(&rgb_img.view(2 * size, size, size, size));
    let tex =
        Texture::new(ctx, size, 0, Sampler::default()).expect("failed to create cubemap texture"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::PositiveX),
        size,
        &xplus_texels,
    )
    .expect("failed to upload x+ face texture data"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::NegativeX),
        size,
        &xminus_texels,
    )
    .expect("failed to upload x- face texture data"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::PositiveY),
        size,
        &yplus_texels,
    )
    .expect("failed to upload y+ face texture data"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::NegativeY),
        size,
        &yminus_texels,
    )
    .expect("failed to upload y- face texture data"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::PositiveZ),
        size,
        &zplus_texels,
    )
    .expect("failed to upload z+ face texture data"); // TODO: throw
    tex.upload_part(
        GenMipmaps::Yes,
        ([0, 0], CubeFace::NegativeZ),
        size,
        &zminus_texels,
    )
    .expect("failed to upload z- face texture data"); // TODO: throw
    Ok(tex)
}
