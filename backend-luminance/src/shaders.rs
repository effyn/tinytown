use {
    cgmath::{Point2, Point3, Vector2, Vector3},
    luminance::{
        pipeline::BoundTexture,
        pixel::Floating,
        shader::program::{Program, Uniform},
        texture::{Cubemap, Dim2},
    },
    luminance_derive::{Semantics, UniformInterface, Vertex},
    regenboog::RgbaU8,
    std::fs,
};

#[derive(UniformInterface)]
pub struct BrickShaderInterface {
    pub view: Uniform<[[f32; 4]; 4]>,
    pub projection: Uniform<[[f32; 4]; 4]>,
    pub model: Uniform<[[f32; 4]; 4]>,
    pub light: Uniform<[f32; 3]>,
    pub default_color: Uniform<[f32; 4]>,
    #[uniform(unbound)]
    pub tex_top: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
    #[uniform(unbound)]
    pub tex_side: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
    #[uniform(unbound)]
    pub tex_bottom: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
}

#[derive(UniformInterface)]
pub struct FloorShaderInterface {
    pub view: Uniform<[[f32; 4]; 4]>,
    pub projection: Uniform<[[f32; 4]; 4]>,
    pub model: Uniform<[[f32; 4]; 4]>,
    pub light: Uniform<[f32; 3]>,
    #[uniform(unbound)]
    pub tex: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
}

#[derive(UniformInterface)]
pub struct SkyboxShaderInterface {
    pub view: Uniform<[[f32; 4]; 4]>,
    pub projection: Uniform<[[f32; 4]; 4]>,
    #[uniform(unbound)]
    pub tex: Uniform<&'static BoundTexture<'static, Cubemap, Floating>>,
}

#[derive(Debug, Clone, Copy, Semantics)]
pub enum VertexSemantics {
    #[sem(name = "position", repr = "[f32; 3]", wrapper = "VertexPosition")]
    Position,
    #[sem(name = "color", repr = "[f32; 4]", wrapper = "VertexColor")]
    RgbaU8,
    #[sem(name = "normal", repr = "[f32; 3]", wrapper = "VertexNormal")]
    Normal,
    #[sem(name = "tex_size", repr = "[f32; 2]", wrapper = "VertexTexSize")]
    TexSize,
    #[sem(name = "tex_uv", repr = "[f32; 2]", wrapper = "VertexTexUv")]
    TexUv,
    #[sem(name = "tex_dir", repr = "[f32; 3]", wrapper = "VertexTexDir")]
    TexDir,
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct BrickVertex {
    pub position: VertexPosition,
    pub color: VertexColor,
    pub normal: VertexNormal,
    pub tex_size: VertexTexSize,
    pub tex_uv: VertexTexUv,
}

impl BrickVertex {
    pub fn create(
        position: Point3<f32>,
        color: RgbaU8,
        normal: Vector3<f32>,
        tex_size: Vector2<f32>,
        tex_uv: Point2<f32>,
    ) -> BrickVertex {
        BrickVertex {
            position: VertexPosition::new(position.into()),
            color: VertexColor::new(color.into()),
            normal: VertexNormal::new(normal.into()),
            tex_size: VertexTexSize::new(tex_size.into()),
            tex_uv: VertexTexUv::new(tex_uv.into()),
        }
    }
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct FloorVertex {
    pub position: VertexPosition,
    pub normal: VertexNormal,
    pub tex_uv: VertexTexUv,
}

impl FloorVertex {
    pub fn create(position: Point3<f32>, normal: Vector3<f32>, tex_uv: Point2<f32>) -> FloorVertex {
        FloorVertex {
            position: VertexPosition::new(position.into()),
            normal: VertexNormal::new(normal.into()),
            tex_uv: VertexTexUv::new(tex_uv.into()),
        }
    }
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct SkyboxVertex {
    pub position: VertexPosition,
    pub tex_dir: VertexTexDir,
}

impl SkyboxVertex {
    pub fn create(position: Point3<f32>, tex_dir: Vector3<f32>) -> SkyboxVertex {
        SkyboxVertex {
            position: VertexPosition::new(position.into()),
            tex_dir: VertexTexDir::new(tex_dir.into()),
        }
    }
}

#[derive(UniformInterface)]
pub struct UiShaderInterface {
    pub window_size: Uniform<[f32; 2]>,
    pub full_tex_size: Uniform<[f32; 2]>,
    #[uniform(unbound)]
    pub tex: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
}

#[derive(Debug, Clone, Copy, Semantics)]
pub enum UiVertexSemantics {
    #[sem(
        name = "screen_position",
        repr = "[f32; 2]",
        wrapper = "UiVertexPosition"
    )]
    Position,
    #[sem(name = "rect_pos", repr = "[f32; 2]", wrapper = "UiVertexRectPos")]
    RectPos,
    #[sem(name = "rect_size", repr = "[f32; 2]", wrapper = "UiVertexRectSize")]
    RectSize,
    #[sem(name = "tex_start", repr = "[f32; 2]", wrapper = "UiVertexTexStart")]
    TexStart,
    #[sem(name = "tex_size", repr = "[f32; 2]", wrapper = "UiVertexTexSize")]
    TexSize,
    #[sem(
        name = "tex_border_tl",
        repr = "[f32; 2]",
        wrapper = "UiVertexTexBorderTl"
    )]
    TexBorderTl,
    #[sem(
        name = "tex_border_br",
        repr = "[f32; 2]",
        wrapper = "UiVertexTexBorderBr"
    )]
    TexBorderBr,
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "UiVertexSemantics")]
pub struct UiVertex {
    pub screen_position: UiVertexPosition,
    pub rect_pos: UiVertexRectPos,
    pub rect_size: UiVertexRectSize,
    pub tex_start: UiVertexTexStart,
    pub tex_size: UiVertexTexSize,
    pub tex_border_tl: UiVertexTexBorderTl,
    pub tex_border_br: UiVertexTexBorderBr,
}

impl UiVertex {
    pub fn create(
        screen_position: Point2<f32>,
        rect_pos: Point2<f32>,
        rect_size: Vector2<f32>,
        tex_start: Point2<f32>,
        tex_size: Vector2<f32>,
        tex_border_tl: Vector2<f32>,
        tex_border_br: Vector2<f32>,
    ) -> UiVertex {
        UiVertex {
            screen_position: UiVertexPosition::new(screen_position.into()),
            rect_pos: UiVertexRectPos::new(rect_pos.into()),
            rect_size: UiVertexRectSize::new(rect_size.into()),
            tex_start: UiVertexTexStart::new(tex_start.into()),
            tex_size: UiVertexTexSize::new(tex_size.into()),
            tex_border_tl: UiVertexTexBorderTl::new(tex_border_tl.into()),
            tex_border_br: UiVertexTexBorderBr::new(tex_border_br.into()),
        }
    }
}

pub struct Shaders {
    pub bricks: Program<VertexSemantics, (), BrickShaderInterface>,
    pub floor: Program<VertexSemantics, (), FloorShaderInterface>,
    pub skybox: Program<VertexSemantics, (), SkyboxShaderInterface>,
    pub ui: Program<UiVertexSemantics, (), UiShaderInterface>,
}

impl Shaders {
    pub fn new() -> anyhow::Result<Shaders> {
        let brick_vertex_shader = fs::read_to_string("resources/shaders/brick_vertex.glsl")?;
        let brick_fragment_shader = fs::read_to_string("resources/shaders/brick_fragment.glsl")?;
        let floor_vertex_shader = fs::read_to_string("resources/shaders/floor_vertex.glsl")?;
        let floor_fragment_shader = fs::read_to_string("resources/shaders/floor_fragment.glsl")?;
        let skybox_vertex_shader = fs::read_to_string("resources/shaders/skybox_vertex.glsl")?;
        let skybox_fragment_shader = fs::read_to_string("resources/shaders/skybox_fragment.glsl")?;
        let ui_vertex_shader = fs::read_to_string("resources/shaders/ui_vertex.glsl")?;
        let ui_fragment_shader = fs::read_to_string("resources/shaders/ui_fragment.glsl")?;
        let bricks =
            Program::from_strings(None, &brick_vertex_shader, None, &brick_fragment_shader)
                .expect("failed to load brick shader")
                .program; // TODO: throw
        let floor = Program::from_strings(None, &floor_vertex_shader, None, &floor_fragment_shader)
            .expect("failed to load floor shader")
            .program; // TODO: throw
        let skybox =
            Program::from_strings(None, &skybox_vertex_shader, None, &skybox_fragment_shader)
                .expect("failed to load floor shader")
                .program; // TODO: throw
        let ui = Program::from_strings(None, &ui_vertex_shader, None, &ui_fragment_shader)
            .expect("failed to load ui shader")
            .program; // TODO: throw
        Ok(Shaders {
            bricks,
            floor,
            skybox,
            ui,
        })
    }
}
