Contributing info
=================

Can I contribute?
-----------------

At this point, the project is very much in flux, so it's pretty hard to accept contributions.

If you wish to report a bug, feel free to submit it to the issue tracker.

I hope to get this project into a state where I can accept contributions soon.

The project will likely get an XMPP MUC once it gets to that point.
