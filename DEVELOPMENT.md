Development notes
=================

Coordinate system
-----------------

The X axis goes from west to east, the Y axis goes from down to up, the Z axis goes from south to north.

Resources
---------

Collision detection & response:

 - [Allen Chou's series on game physics](http://allenchou.net/game-physics-series)
 - Erin Catto's talks at GDC:
   - [Physics for Game Programmers: Understanding Constraints](https://www.youtube.com/watch?v=SHinxAhv1ZE)
   - [Physics for Game Programmers: Continuous Collision](https://www.youtube.com/watch?v=7_nKOET6zwI)
   - [Computing Distance](https://box2d.org/files/ErinCatto_GJK_GDC2010.pdf)
 - [Implementing GJK - 2006](https://www.youtube.com/watch?v=Qupqu1xe7Io)

Netcode:

 - [I Shot You First: Networking of the Gameplay of Halo: Reach](https://www.youtube.com/watch?v=h47zZrqjgLc)
 - [Overwatch Gameplay Architecture and Netcode](https://www.youtube.com/watch?v=W3aieHjyNvw)
 - [The TRIBES Engine Networking Model](https://www.gamedevs.org/uploads/tribes-networking-model.pdf)
 - The articles relating to netcode on [Gaffer On Games](https://www.gafferongames.com)
