use tinytown::{
    backend::{Backend, BackendName},
    texture_manager::{TextureManager, TextureManagerProxy},
};

pub struct NullBackend {
    texture_manager: TextureManager<()>,
}

impl NullBackend {
    pub fn new() -> NullBackend {
        NullBackend {
            texture_manager: TextureManager::new(),
        }
    }
}

impl BackendName for NullBackend {
    fn name() -> &'static str {
        "null"
    }
}

impl Backend for NullBackend {
    fn handle_events(&mut self, _world: &specs::World) -> anyhow::Result<()> {
        Ok(())
    }

    fn render(&mut self, _world: &specs::World) -> anyhow::Result<()> {
        Ok(())
    }

    fn aspect_ratio(&self) -> f32 {
        1.
    }

    fn size(&self) -> (u32, u32) {
        (1, 1)
    }

    fn texture_manager_proxy(&self) -> TextureManagerProxy {
        self.texture_manager.proxy()
    }
}
