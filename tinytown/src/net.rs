use {
    pigeon::{FrameReader, FrameWriter},
    std::collections::VecDeque,
    tinytown_protocol::{ClientCookie, FrameHeader, PushHeader, PushKind, ServerChallenge},
};

pub mod client;
pub mod controls_queue;
pub mod server;

pub use controls_queue::{ControlsQueue, ControlsQueueEntry};

pub const MAX_PACKET_SIZE: usize = 1024;

#[derive(Debug, Clone)]
pub enum Controller {
    None,
    Local,
    Remote(SlotId),
}

impl Controller {
    pub fn is_local(&self) -> bool {
        if let Controller::Local = self {
            true
        } else {
            false
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SlotId(pub usize);

pub use self::{client::ClientConnection, server::ConnectionManager};

#[derive(Debug, Clone)]
pub enum ConnectionEvent {
    Lost(PushInfo),
    Delivered(PushInfo),
    Recv(PushInfo),
}

#[derive(Debug, Clone)]
pub enum PushEvent {
    Lost(PushInfo),
    Delivered(PushInfo),
}

#[derive(Debug, Clone)]
pub struct PushInTransit {
    pub id: u32,
    pub info: PushInfo,
}

#[derive(Debug, Clone)]
pub enum PushInfo {
    Ghost { net_id: u32, components: u8 },
    Event { event_id: u32 },
    ClientTick { tick: u32 },
    ServerTick { tick: u32 },
}

impl PushInfo {
    pub fn to_kind(&self) -> PushKind {
        match self {
            PushInfo::Ghost { .. } => PushKind::Ghost,
            PushInfo::Event { .. } => PushKind::Event,
            PushInfo::ClientTick { .. } => PushKind::ClientTick,
            PushInfo::ServerTick { .. } => PushKind::ServerTick,
        }
    }

    pub fn from_header(push_header: &PushHeader) -> PushInfo {
        match *push_header {
            PushHeader::Event { event_id } => PushInfo::Event { event_id },
            PushHeader::Ghost { net_id, components } => PushInfo::Ghost { net_id, components },
            PushHeader::ClientTick { tick, .. } => PushInfo::ClientTick { tick },
            PushHeader::ServerTick { tick } => PushInfo::ServerTick { tick },
        }
    }
}

#[derive(Debug)]
pub struct PushWindow {
    last_sent_id: u32,
    last_recv_id: u32,
    last_seen_id: u32,
    in_transit: VecDeque<PushInTransit>,
    recv_window: u32,
    payload_count: u8,
}

impl PushWindow {
    pub fn new() -> PushWindow {
        PushWindow {
            last_sent_id: 0,
            last_recv_id: 0,
            last_seen_id: 0,
            in_transit: VecDeque::with_capacity(256),
            recv_window: 0,
            payload_count: 0,
        }
    }

    pub fn recv_header(
        &mut self,
        push_id: u32,
        last_push_id: u32,
        push_window: u32,
        mut cb: impl FnMut(PushEvent),
    ) {
        if self.last_seen_id < last_push_id {
            while let Some(item) = self.in_transit.front() {
                if item.id > last_push_id {
                    break;
                }
                let pkt_received = if last_push_id != item.id {
                    let offset = last_push_id - item.id - 1;
                    (push_window << offset) & 1 != 0
                } else {
                    true
                };
                let item = self.in_transit.pop_front().unwrap(); // can't panic; checked earlier
                if pkt_received {
                    cb(PushEvent::Delivered(item.info));
                } else {
                    cb(PushEvent::Lost(item.info));
                }
            }
            self.last_seen_id = last_push_id;
        }
        if self.last_recv_id < push_id {
            let diff = push_id - self.last_recv_id;
            self.recv_window = (self.recv_window >> diff) | 1;
            self.last_recv_id = push_id;
        }
    }

    pub fn add_push(&mut self, info: PushInfo) {
        let record = PushInTransit {
            id: self.last_sent_id + 1,
            info,
        };
        self.in_transit.push_back(record);
        self.payload_count += 1;
    }

    pub fn next_header(&mut self) -> FrameHeader {
        self.last_sent_id += 1;
        let payload_count = self.payload_count;
        self.payload_count = 0;
        FrameHeader::Push {
            push_id: self.last_sent_id,
            last_push_id: self.last_recv_id,
            push_window: self.recv_window << 1,
            payload_count,
        }
    }
}

#[derive(Debug, Clone)]
pub struct PendingPush(pub usize);

pub struct Connection {
    push_window: PushWindow,
    initiating: bool,
    pending: Vec<PendingPush>,
    pending_buf: [u8; 2048],
    pending_ptr: usize,
}

impl Connection {
    pub fn initiating() -> Connection {
        Connection {
            push_window: PushWindow::new(),
            initiating: true,
            pending: Vec::new(),
            pending_buf: [0; 2048],
            pending_ptr: 0,
        }
    }

    pub fn accepting() -> Connection {
        Connection {
            push_window: PushWindow::new(),
            initiating: false,
            pending: Vec::new(),
            pending_buf: [0; 2048],
            pending_ptr: 0,
        }
    }

    pub fn on_recv<'a>(
        &mut self,
        reader: &'a mut FrameReader<'a>,
        mut push_status_cb: impl FnMut(PushEvent),
        mut push_recv_cb: impl FnMut(&mut FrameReader<'a>, PushHeader) -> Option<()>,
    ) {
        if let Some(header) = reader.read::<FrameHeader>() {
            match header {
                FrameHeader::Push {
                    push_id,
                    last_push_id,
                    push_window,
                    payload_count,
                } => {
                    self.push_window.recv_header(
                        push_id,
                        last_push_id,
                        push_window,
                        |evt| match evt {
                            PushEvent::Lost(info) => push_status_cb(PushEvent::Lost(info)),
                            PushEvent::Delivered(info) => {
                                push_status_cb(PushEvent::Delivered(info))
                            }
                        },
                    );
                    for _ in 0..payload_count {
                        if let Some(ph) = reader.read::<PushHeader>() {
                            if push_recv_cb(reader, ph).is_none() {
                                log::error!("could not decode push payload");
                            }
                        } else {
                            log::error!("invalid push header received");
                        }
                    }
                }
                _ => log::error!("received an invalid frame for this state, or a malformed frame"),
            }
        }
    }

    pub fn add_push<'a>(
        &'a mut self,
        header: &PushHeader,
        cb: impl FnOnce(&mut FrameWriter<&'a mut [u8]>),
    ) {
        // TODO: also add bounds checks here
        let mut writer = FrameWriter::new(&mut self.pending_buf[self.pending_ptr..]);
        writer.write(header);
        cb(&mut writer);
        let info = PushInfo::from_header(header);
        self.push_window.add_push(info);
        self.pending_ptr += writer.position();
        self.pending.push(PendingPush(writer.position()));
    }

    pub fn write_frame<'a>(&mut self, writer: &mut FrameWriter<&'a mut [u8]>) {
        let header = self.push_window.next_header();
        writer.write(header);
        let mut ptr = 0;
        for PendingPush(len) in self.pending.drain(..) {
            let slice = &self.pending_buf[ptr..ptr + len];
            if writer.fits_bytes(slice) {
                writer.write_bytes(slice);
                ptr += len;
            } else {
                log::warn!("dropping a push because the packet is full");
            }
        }
        self.pending_ptr = 0;
    }
}
