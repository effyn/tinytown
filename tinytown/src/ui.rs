use {
    cgmath::{prelude::*, Point2, Vector2},
    slotmap::DenseSlotMap,
    std::collections::VecDeque,
};

use crate::texture_manager::{TextureId, TextureManagerProxy};

slotmap::new_key_type! {
    pub struct WidgetId;
}

pub struct GameUi {
    pub tool_indicator_root: WidgetId,
    pub tool_indicator_button: WidgetId,
    pub tool_indicator_view: WidgetId,
    pub crosshair_root: WidgetId,
    pub crosshair: WidgetId,
}

impl GameUi {
    pub fn new(ui: &mut Ui) -> GameUi {
        let tool_indicator_root = ui.create_root(
            Rect::with_size(Point2::new(10., 10.), Vector2::new(38., 38.)),
            Gravity::top_left(),
        );
        let tool_indicator_button = ui.create_widget(tool_indicator_root, Widget::button());
        let tool_indicator_view =
            ui.create_widget(tool_indicator_button, Widget::tool_view(Tool::Brick));
        let crosshair_root = ui.create_root(
            Rect::with_center(ui.screen_rect().center(), Vector2::new(32., 32.)),
            Gravity::center(),
        );
        let crosshair = ui.create_widget(crosshair_root, Widget::crosshair());
        GameUi {
            tool_indicator_root,
            tool_indicator_button,
            tool_indicator_view,
            crosshair_root,
            crosshair,
        }
    }

    pub fn set_current_tool(&mut self, ui: &mut Ui, tool: Tool) {
        let tool_indicator_view = ui.get_widget(self.tool_indicator_view);
        tool_indicator_view.inner = WidgetInner::ToolView(tool);
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Rect {
    pub min: Point2<f32>,
    pub max: Point2<f32>,
}

impl Rect {
    pub fn with_size(min: Point2<f32>, size: Vector2<f32>) -> Rect {
        Rect {
            min,
            max: min + size,
        }
    }

    pub fn with_center(center: Point2<f32>, size: Vector2<f32>) -> Rect {
        let half_size = size * 0.5;
        Rect {
            min: center - half_size,
            max: center + half_size,
        }
    }

    pub fn size(self) -> Vector2<f32> {
        self.max - self.min
    }

    pub fn inset(self, amount: f32) -> Rect {
        let off = Vector2::new(amount, amount);
        let min = self.min + off;
        let max = self.max - off;
        if max.x < min.x || max.y < min.y {
            let middle = min.midpoint(max);
            Rect {
                min: middle,
                max: middle,
            }
        } else {
            Rect { min, max }
        }
    }

    pub fn center(self) -> Point2<f32> {
        self.min.midpoint(self.max)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum GravityAxis {
    Min,
    Center,
    Max,
}

impl GravityAxis {
    pub fn point(self, min: f32, max: f32) -> f32 {
        match self {
            GravityAxis::Min => min,
            GravityAxis::Center => min * 0.5 + max * 0.5,
            GravityAxis::Max => max,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Gravity {
    pub horizontal: GravityAxis,
    pub vertical: GravityAxis,
}

impl Gravity {
    pub fn top_left() -> Self {
        Self {
            horizontal: GravityAxis::Min,
            vertical: GravityAxis::Min,
        }
    }

    pub fn center() -> Self {
        Self {
            horizontal: GravityAxis::Center,
            vertical: GravityAxis::Center,
        }
    }

    pub fn point(self, rect: Rect) -> Point2<f32> {
        Point2::new(
            self.horizontal.point(rect.min.x, rect.max.x),
            self.vertical.point(rect.min.y, rect.max.y),
        )
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Tool {
    None,
    Brick,
    Hammer,
    PaintCan,
}

impl Tool {
    pub fn prev(self) -> Tool {
        match self {
            Tool::None => Tool::PaintCan,
            Tool::Brick => Tool::None,
            Tool::Hammer => Tool::Brick,
            Tool::PaintCan => Tool::Hammer,
        }
    }

    pub fn next(self) -> Tool {
        match self {
            Tool::None => Tool::Brick,
            Tool::Brick => Tool::Hammer,
            Tool::Hammer => Tool::PaintCan,
            Tool::PaintCan => Tool::None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum WidgetInner {
    Root,
    Column,
    Row,
    Button,
    ToolView(Tool),
    Crosshair,
    Label { label: String },
}

#[derive(Debug, Clone)]
struct WidgetVisual {
    rect: Rect,
    segtex_id: Option<usize>,
}

#[derive(Debug, Clone)]
pub struct Widget {
    pub parent_id: Option<WidgetId>,
    pub children: Vec<(Point2<f32>, WidgetId)>,
    inner: WidgetInner,
    visual: Option<WidgetVisual>,
}

impl Widget {
    fn root() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Root,
            visual: None,
        }
    }

    pub fn button() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Button,
            visual: None,
        }
    }

    pub fn tool_view(tool: Tool) -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::ToolView(tool),
            visual: None,
        }
    }

    pub fn crosshair() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Crosshair,
            visual: None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct SegmentTexture {
    pub src: Rect,
    pub border: Border,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Border {
    pub left: f32,
    pub top: f32,
    pub right: f32,
    pub bottom: f32,
}

impl Border {
    pub fn all(amount: f32) -> Border {
        Border {
            left: amount,
            top: amount,
            right: amount,
            bottom: amount,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Ui {
    screen_rect: Rect,
    roots: Vec<(Rect, Gravity, WidgetId)>,
    widgets: DenseSlotMap<WidgetId, Widget>,
    atlas: TextureId,
    segtex: Vec<SegmentTexture>,
}

impl Ui {
    pub fn new(proxy: &TextureManagerProxy, initial_window_size: Vector2<u32>) -> Ui {
        let widgets = DenseSlotMap::with_key();
        let atlas = proxy.load("resources/textures/ui_atlas.png");
        let mut segtex = Vec::new();
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 0.), Vector2::new(12., 12.)),
            border: Border::all(3.),
        });
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 32.), Vector2::new(32., 32.)),
            border: Border::all(0.),
        });
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 64.), Vector2::new(32., 32.)),
            border: Border::all(0.),
        });
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 96.), Vector2::new(32., 32.)),
            border: Border::all(0.),
        });
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(32., 0.), Vector2::new(32., 32.)),
            border: Border::all(0.),
        });
        let roots = Vec::new();
        Ui {
            screen_rect: Rect::with_size(
                Point2::origin(),
                initial_window_size.cast::<f32>().unwrap(),
            ),
            roots,
            widgets,
            atlas,
            segtex,
        }
    }

    pub fn screen_rect(&self) -> Rect {
        self.screen_rect
    }

    pub fn create_root(&mut self, rect: Rect, gravity: Gravity) -> WidgetId {
        let widget = Widget::root();
        let widget_id = self.widgets.insert(widget);
        self.roots.push((rect, gravity, widget_id));
        widget_id
    }

    pub fn create_widget(&mut self, parent_id: WidgetId, mut widget: Widget) -> WidgetId {
        widget.parent_id = Some(parent_id);
        let widget_id = self.widgets.insert(widget);
        let parent = self
            .widgets
            .get_mut(parent_id)
            .expect("creating widget with an invalid parent?");
        parent.children.push((Point2::origin(), widget_id));
        widget_id
    }

    pub fn get_widget(&mut self, widget_id: WidgetId) -> &mut Widget {
        &mut self.widgets[widget_id]
    }

    pub fn atlas(&self) -> TextureId {
        self.atlas
    }

    pub fn layout(&mut self, window_size: Vector2<u32>) {
        let old_screen_rect = self.screen_rect;
        self.screen_rect = Rect::with_size(Point2::origin(), window_size.cast::<f32>().unwrap());
        if self.screen_rect != old_screen_rect {
            for &mut (ref mut rect, gravity, root_id) in &mut self.roots {
                let gravity_point_old = gravity.point(old_screen_rect);
                let gravity_point_new = gravity.point(self.screen_rect);
                let diff = gravity_point_new - gravity_point_old;
                rect.min += diff * 0.5;
                rect.max += diff * 0.5;
            }
        }
        let mut stack = Vec::new();
        let window_size = window_size.cast::<f32>().unwrap();
        for &(rect, _, root_id) in &self.roots {
            let root = &mut self.widgets[root_id];
            root.visual = Some(WidgetVisual {
                rect,
                segtex_id: None,
            });
            for &(_, child_id) in &root.children {
                // TODO: maybe they shouldn't all be stacked
                // TODO: maybe enforce only one child in a root?
                stack.push((child_id, rect.size()));
            }
        }
        while let Some((widget_id, bounds)) = stack.pop() {
            let widget = &mut self.widgets[widget_id];
            let rect = Rect::with_size(Point2::origin(), bounds);
            match widget.inner {
                WidgetInner::Button => {
                    widget.visual = Some(WidgetVisual {
                        rect,
                        segtex_id: Some(0),
                    });
                    let child_rect = rect.inset(3.);
                    for &mut (ref mut child_pos, child_id) in &mut widget.children {
                        *child_pos = child_rect.min;
                        stack.push((child_id, child_rect.size()));
                    }
                }
                WidgetInner::ToolView(tool) => {
                    let segtex_id = match tool {
                        Tool::None => None,
                        Tool::Brick => Some(1),
                        Tool::Hammer => Some(2),
                        Tool::PaintCan => Some(3),
                    };
                    widget.visual = Some(WidgetVisual { rect, segtex_id });
                }
                WidgetInner::Crosshair => {
                    widget.visual = Some(WidgetVisual {
                        rect,
                        segtex_id: Some(4),
                    });
                }
                _ => {
                    // Uninteresting default
                    widget.visual = Some(WidgetVisual {
                        rect,
                        segtex_id: None,
                    });
                    for &(_, child_id) in &widget.children {
                        stack.push((child_id, rect.size()));
                    }
                }
            }
        }
    }

    pub fn render(&self, mut inner: impl FnMut(&SegmentTexture, Rect)) {
        let mut queue = VecDeque::new();
        for &(_, _, root_id) in &self.roots {
            queue.push_front((Point2::origin(), root_id));
        }
        while let Some((offset, widget_id)) = queue.pop_back() {
            let widget = &self.widgets[widget_id];
            if let Some(vis) = &widget.visual {
                let abs_rect = Rect {
                    min: vis.rect.min + offset.to_vec(),
                    max: vis.rect.max + offset.to_vec(),
                };
                if let Some(segtex_id) = vis.segtex_id {
                    inner(&self.segtex[segtex_id], abs_rect);
                }
                for &(child_pos, child_id) in &widget.children {
                    queue.push_front((abs_rect.min + child_pos.to_vec(), child_id));
                }
            }
        }
    }
}
