use std::{
    ops::Index,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TextureId(usize);

#[derive(Clone, Debug)]
pub struct TextureManager<T> {
    textures: Vec<T>,
    texture_paths: Arc<Mutex<Vec<PathBuf>>>,
}

impl<T> TextureManager<T> {
    pub fn new() -> TextureManager<T> {
        TextureManager {
            textures: Vec::new(),
            texture_paths: Arc::new(Mutex::new(Vec::new())),
        }
    }

    pub fn proxy(&self) -> TextureManagerProxy {
        TextureManagerProxy {
            texture_paths: self.texture_paths.clone(),
        }
    }

    pub fn load<P: AsRef<Path>>(&self, path: P) -> TextureId {
        load_texture(&self.texture_paths, path)
    }

    pub fn sync<L: TextureLoader<Texture = T>>(&mut self, loader: &mut L) -> anyhow::Result<()> {
        let texture_paths = self.texture_paths.lock().unwrap();
        for i in self.textures.len()..texture_paths.len() {
            let path = &texture_paths[i];
            let tex = loader.load_texture(path)?;
            self.textures.push(tex);
        }
        Ok(())
    }
}

impl<T> Index<TextureId> for TextureManager<T> {
    type Output = T;

    fn index(&self, TextureId(idx): TextureId) -> &T {
        &self.textures[idx]
    }
}

#[derive(Clone)]
pub struct TextureManagerProxy {
    texture_paths: Arc<Mutex<Vec<PathBuf>>>,
}

impl TextureManagerProxy {
    pub fn load<P: AsRef<Path>>(&self, path: P) -> TextureId {
        load_texture(&self.texture_paths, path)
    }
}

fn load_texture<P: AsRef<Path>>(texture_paths: &Mutex<Vec<PathBuf>>, path: P) -> TextureId {
    let path = path.as_ref();
    let mut texture_paths = texture_paths.lock().unwrap();
    for (i, tex_path) in texture_paths.iter().enumerate() {
        if path == tex_path {
            return TextureId(i);
        }
    }
    let len = texture_paths.len();
    texture_paths.push(path.to_owned());
    TextureId(len)
}

pub trait TextureLoader {
    type Texture;

    fn load_texture(&mut self, path: impl AsRef<Path>) -> anyhow::Result<Self::Texture>;
}
