use {
    cgmath::Vector3,
    shrev::EventChannel,
    specs::{prelude::*, world::Index},
    std::collections::HashMap,
    tinytown_collision::{
        broad::bvh::{AabbQueryCache, Bvh, CollidingPairsCache},
        Aabb, CollisionMask, ContactPair, Ray, RaycastHit, TransformedShape,
    },
};

use crate::brick::{self, VoxAabb};

#[derive(Debug, Clone, Copy, PartialEq)]
struct Entry {
    mask: CollisionMask,
    collides_with: CollisionMask,
    shape: TransformedShape,
    entity: Index,
}

pub struct CollisionManager {
    bvh: Bvh<Entry>,
    entries: HashMap<Index, Entry>,
    colliding_pairs_cache: CollidingPairsCache,
    bricks: HashMap<Index, VoxAabb>,
    pub brick_grid: brick::grid::BrickGrid<Index>,
    pub events: EventChannel<ContactPair<Entity>>,
}

impl Default for CollisionManager {
    fn default() -> CollisionManager {
        CollisionManager {
            bvh: Bvh::new(),
            entries: HashMap::new(),
            colliding_pairs_cache: CollidingPairsCache::new(),
            bricks: HashMap::new(),
            brick_grid: brick::grid::BrickGrid::new(),
            events: EventChannel::new(),
        }
    }
}

impl CollisionManager {
    pub fn clear(&mut self) {
        log::trace!("clearing the collision manager");
        self.bvh.clear();
    }

    pub fn add(
        &mut self,
        entity: Index,
        mask: CollisionMask,
        collides_with: CollisionMask,
        shape: TransformedShape,
    ) {
        let aabb = shape.aabb();
        let entry = Entry {
            entity,
            mask,
            collides_with,
            shape,
        };
        log::trace!(
            "adding {:?} to the collision manager, aabb {:?}",
            entry,
            aabb
        );
        self.bvh.add(aabb, entry);
        self.entries.insert(entity, entry);
    }

    pub fn update(
        &mut self,
        entity: Index,
        mask: CollisionMask,
        collides_with: CollisionMask,
        shape: TransformedShape,
    ) {
        if let Some(&old_entry) = self.entries.get(&entity) {
            let new_entry = Entry {
                entity,
                mask,
                collides_with,
                shape,
            };
            let new_aabb = shape.aabb();
            let old_aabb = old_entry.shape.aabb();
            self.entries.insert(entity, new_entry);
            assert!(self.bvh.update(old_aabb, new_aabb, old_entry, new_entry));
        }
    }

    pub fn remove(&mut self, entity: Index) {
        if let Some(entry) = self.entries.get(&entity) {
            let aabb = entry.shape.aabb();
            assert!(self.bvh.remove(aabb, *entry));
            self.entries.remove(&entity);
        }
    }

    pub fn add_brick(&mut self, entity: Index, vox_aabb: VoxAabb) {
        self.bricks.insert(entity, vox_aabb);
        self.brick_grid.add(vox_aabb, entity);
    }

    pub fn remove_brick(&mut self, entity: Index) {
        if let Some(&vox_aabb) = self.bricks.get(&entity) {
            self.brick_grid.remove(vox_aabb, entity);
        }
    }

    pub fn cast_ray(
        &self,
        query_mask: CollisionMask,
        ray: Ray,
        limit: f32,
    ) -> Option<RaycastHit<Option<Index>>> {
        log::debug!(
            "casting ray from {:?} in direction {:?}. mask: {:?}, limit: {}",
            ray.origin,
            ray.dir,
            query_mask,
            limit
        );
        let mut closest_dist = limit;
        let mut closest_hit = None;
        let bvh_narrow = |entry: Entry| {
            // TODO: actually use a narrow phase
            if query_mask.contains(entry.mask) {
                let aabb = entry.shape.aabb();
                let distance = aabb.cast_ray(ray).unwrap();
                let point = ray.point_at(distance);
                let normal = aabb.normal_at(point);
                Some(RaycastHit {
                    point,
                    normal,
                    distance,
                    value: Some(entry.entity),
                })
            } else {
                None
            }
        };
        if let Some(distance) = ray.intersects_floor() {
            let point = ray.point_at(distance);
            let normal = Vector3::new(0., 1., 0.);
            closest_dist = distance;
            closest_hit = Some(RaycastHit {
                point,
                normal,
                distance,
                value: None,
            });
            log::debug!("floor hit at {:?}, distance: {}", point, distance);
        }
        if query_mask.contains(CollisionMask::BRICK) {
            log::debug!("mask contains CollisionMask::BRICK, checking for bricks");
            if let Some(hit) = self.brick_grid.cast_ray(ray, closest_dist) {
                log::debug!(
                    "brick hit at {:?}, distance: {}, normal: {:?}, entity: {:?}",
                    hit.point,
                    hit.distance,
                    hit.normal,
                    hit.value
                );
                closest_dist = hit.distance;
                closest_hit = Some(RaycastHit {
                    point: hit.point,
                    normal: hit.normal,
                    distance: hit.distance,
                    value: Some(hit.value),
                });
            }
        }
        let hit_bvh_opt = self.bvh.cast_ray(ray, closest_dist, bvh_narrow);
        if let Some(hit) = hit_bvh_opt {
            log::debug!(
                "bvh hit at {:?}, distance: {}, normal: {:?}, entity: {:?}",
                hit.point,
                hit.distance,
                hit.normal,
                hit.value
            );
            closest_dist = hit.distance;
            closest_hit = Some(hit);
        }
        closest_hit
    }

    pub fn query_aabb<'a, 'b: 'a>(
        &'a self,
        aabb_query_cache: &'b mut AabbQueryCache,
        query_mask: CollisionMask,
        query_aabb: Aabb,
    ) -> impl Iterator<Item = Index> + 'a {
        // TODO: query for bricks if `CollisionMask::BRICK` is in the mask
        self.bvh
            .query_aabb(aabb_query_cache, query_aabb)
            .filter(move |entry| query_mask.contains(entry.mask))
            .map(|entry| entry.entity)
    }

    pub fn colliding_pairs<'a>(&'a mut self) -> impl Iterator<Item = (Index, Index)> + 'a {
        self.bvh
            .query_colliding_pairs(&mut self.colliding_pairs_cache)
            .filter(|(entry_a, entry_b)| {
                entry_a.collides_with.contains(entry_b.mask)
                    || entry_b.collides_with.contains(entry_a.mask)
            })
            .map(|(entry_a, entry_b)| (entry_a.entity, entry_b.entity))
    }

    pub fn colliding_with_bricks<'a>(&'a mut self, mut cb: impl FnMut(Index, Index)) {
        for entry in self.bvh.iter() {
            // there's probably a much better way of doing this
            let aabb = entry.shape.aabb();
            for brick_entity in self.brick_grid.query_world_aabb(aabb) {
                cb(entry.entity, brick_entity);
            }
        }
    }
}
