#![deny(missing_docs)]

//! Camera handling

use cgmath::{prelude::*, Matrix4, PerspectiveFov, Point3, Vector3};

/// A camera at location `eye` which points in direction `dir` with up vector `up`.
#[derive(Clone, Debug)]
pub struct Camera {
    /// The eye position of the camera
    pub eye: Point3<f32>,
    /// The direction the camera is facing
    pub dir: Vector3<f32>,
    /// The up vector of the camera
    pub up: Vector3<f32>,
    /// The field of view information about the camera
    pub perspective_fov: PerspectiveFov<f32>,
}

impl Camera {
    /// Create a new camera from its components.
    pub fn new(
        eye: Point3<f32>,
        dir: Vector3<f32>,
        up: Vector3<f32>,
        perspective_fov: PerspectiveFov<f32>,
    ) -> Camera {
        Camera {
            eye,
            dir: dir.normalize(),
            up,
            perspective_fov,
        }
    }

    /// Get the projection matrix of the camera.
    pub fn projection(&self) -> Matrix4<f32> {
        let perspective = self.perspective_fov.to_perspective();

        Matrix4::from(perspective)
    }

    /// Get the view matrix of the camera.
    pub fn view(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(self.eye, self.dir, self.up)
    }

    /// Get the rotation component of the view matrix of the camera.
    pub fn view_rot(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(Point3::origin(), self.dir, self.up)
    }

    /// Make the camera look at a location.
    pub fn look_at(&mut self, target: Point3<f32>) {
        self.dir = (target - self.eye).normalize();
    }
}
