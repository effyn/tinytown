use {
    chashmap::CHashMap,
    specs::prelude::*,
    std::sync::atomic::{AtomicU32, Ordering},
};

use crate::components::NetId;

/// An allocator of network ids
#[derive(Debug)]
pub struct NetIdAllocator {
    last_id: AtomicU32,
    mapping: CHashMap<NetId, Entity>, // TODO: may not be the best datastructure for this
}

impl NetIdAllocator {
    /// Create a new network id allocator.
    pub fn new() -> NetIdAllocator {
        NetIdAllocator {
            last_id: AtomicU32::new(0),
            mapping: CHashMap::new(),
        }
    }

    /// Get the next id from the allocator.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::{
    /// #         resources::NetIdAllocator,
    /// #         components::NetId,
    /// #     },
    /// # };
    /// let mut nia = NetIdAllocator::new();
    ///
    /// let net_id_a = nia.next_id();
    /// let net_id_b = nia.next_id();
    ///
    /// assert_ne!(net_id_a, net_id_b);
    /// ```
    pub fn next_id(&self) -> NetId {
        NetId(self.last_id.fetch_add(1, Ordering::SeqCst))
    }

    /// Register an entity with the allocator.
    pub fn register_id(&self, net_id: NetId, entity: Entity) {
        self.mapping.insert(net_id, entity);
    }

    /// Unregister an entity from the allocator.
    pub fn unregister_id(&self, net_id: NetId) {
        self.mapping.remove(&net_id);
    }

    /// Get the entity associated with a network id.
    pub fn get(&self, net_id: NetId) -> Option<Entity> {
        self.mapping.get(&net_id).map(|e| *e)
    }
}
