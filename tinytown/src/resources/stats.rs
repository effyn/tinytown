use std::fmt;

pub const N_SAMPLES: usize = 60;

#[derive(Clone, Debug)]
pub struct RingBuffer<T> {
    inner: Vec<T>,
    current: usize,
}

impl<T: Default + Clone> RingBuffer<T> {
    pub fn new(size: usize) -> RingBuffer<T> {
        RingBuffer {
            inner: vec![T::default(); size],
            current: 0,
        }
    }
}

impl<T> RingBuffer<T> {
    pub fn push(&mut self, elem: T) {
        self.inner[self.current] = elem;
        self.current += 1;
        if self.current >= self.inner.len() {
            self.current -= self.inner.len();
        }
    }
}

impl fmt::Display for RingBuffer<u32> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut min = None;
        let mut max = None;
        let mut avg = 0;
        for &elem in &self.inner {
            if min.is_none() || elem < min.unwrap() {
                min = Some(elem);
            }
            if max.is_none() || elem > max.unwrap() {
                max = Some(elem);
            }
            avg += elem;
        }
        avg /= N_SAMPLES as u32;
        write!(f, "min={} max={} avg={}", min.unwrap(), max.unwrap(), avg)
    }
}

#[derive(Clone, Debug)]
pub struct Stats {
    pub broad_phase_collision_time: RingBuffer<u32>,
    pub narrow_phase_collision_time: RingBuffer<u32>,
    pub render_time: RingBuffer<u32>,
    pub narrow_phase_collisions: RingBuffer<u32>,
}

impl Stats {
    pub fn new() -> Stats {
        Stats {
            broad_phase_collision_time: RingBuffer::new(N_SAMPLES),
            narrow_phase_collision_time: RingBuffer::new(N_SAMPLES),
            narrow_phase_collisions: RingBuffer::new(N_SAMPLES),
            render_time: RingBuffer::new(N_SAMPLES),
        }
    }

    pub fn print_stats(&self) {
        log::debug!(" == STATS == ");
        log::debug!(
            "Broad phase collision time (ms): {}",
            self.broad_phase_collision_time
        );
        log::debug!(
            "Narrow phase collision time (ms): {}",
            self.narrow_phase_collision_time
        );
        log::debug!("Render time (ms): {}", self.render_time);
    }
}
