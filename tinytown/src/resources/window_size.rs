use cgmath::Vector2;

/// Window size information
#[derive(Clone, Debug)]
pub struct WindowSize {
    /// The width of the window, in pixels
    pub width: u32,
    /// The height of the window, in pixels
    pub height: u32,
}

impl WindowSize {
    /// Get the size of the window as a `Vector2<u32>`.
    pub fn size(&self) -> Vector2<u32> {
        Vector2::new(self.width, self.height)
    }
}
