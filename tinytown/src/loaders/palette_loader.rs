use {
    core::{num::ParseIntError, str::FromStr},
    regenboog::RgbaU8,
    std::{
        fs::File,
        io::{self, BufRead, BufReader},
        path::Path,
    },
    thiserror::Error,
};

#[derive(Debug, Error)]
pub enum PaletteLoadError {
    #[error("malformed line in palette")]
    MalformedLine,
    #[error("io error while loading palette: {0:}")]
    IoError(#[from] io::Error),
    #[error("couldn't parse an integer while loading palette: {0:}")]
    ParseIntError(#[from] ParseIntError),
}

pub fn load(path: impl AsRef<Path>) -> Result<Vec<RgbaU8>, PaletteLoadError> {
    let path = path.as_ref();
    log::debug!("loading color palette from {}", path.display());
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let mut palette = Vec::new();
    for line in reader.lines() {
        let line = line?;
        let mut nums = line.split(' ').take(4).map(u8::from_str);
        let r = nums
            .next()
            .ok_or_else(|| PaletteLoadError::MalformedLine)??;
        let g = nums
            .next()
            .ok_or_else(|| PaletteLoadError::MalformedLine)??;
        let b = nums
            .next()
            .ok_or_else(|| PaletteLoadError::MalformedLine)??;
        let a = nums.next().unwrap_or(Ok(255))?;
        if nums.next().is_some() {
            return Err(PaletteLoadError::MalformedLine);
        }
        let color = [r, g, b, a].into();
        palette.push(color);
    }
    Ok(palette)
}
