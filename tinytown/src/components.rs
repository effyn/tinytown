#![deny(missing_docs)]

//! Components used in tinytown

mod brick;
mod netid;
mod physical;
mod player;

pub use self::{
    brick::{Brick, GhostBrick},
    netid::NetId,
    physical::Physical,
    player::Player,
};
