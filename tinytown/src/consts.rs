pub const NEAR: f32 = 0.1;
pub const FAR: f32 = 1000.;
pub const FOV_DEG: f32 = 45.;
pub const TICKS_PER_SECOND: u64 = 60;
pub const TICK_NANOS: u64 = 1_000_000_000 / TICKS_PER_SECOND;
pub const CONTROLS_QUEUE_SIZE: usize = 4;
pub const SERVER_CONTROLS_QUEUE_SIZE: usize = 4 * CONTROLS_QUEUE_SIZE;
