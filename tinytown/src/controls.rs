use {
    cgmath::Vector2,
    pigeon::{Dump, FrameReader, FrameTarget, FrameWriter, Grab},
    serde::{Deserialize, Serialize},
    std::collections::{HashMap, HashSet},
};

/// Whether a key or mouse button has been pressed or released.
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Action {
    /// The control has been pressed.
    Press,
    /// The control has been released.
    Release,
}

/// A mouse button
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum MouseButton {
    /// The left mouse button
    Left,
    /// The middle mouse button
    Middle,
    /// The right mouse button
    Right,
}

/// A key on the keyboard
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum Key {
    Escape,
    Space,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    #[serde(rename = "0")]
    Num0,
    #[serde(rename = "1")]
    Num1,
    #[serde(rename = "2")]
    Num2,
    #[serde(rename = "3")]
    Num3,
    #[serde(rename = "4")]
    Num4,
    #[serde(rename = "5")]
    Num5,
    #[serde(rename = "6")]
    Num6,
    #[serde(rename = "7")]
    Num7,
    #[serde(rename = "8")]
    Num8,
    #[serde(rename = "9")]
    Num9,
    LeftShift,
    RightShift,
    Semicolon,
    LeftBracket,
    RightBracket,
}

/// All controls
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[repr(u8)]
#[serde(rename_all = "kebab-case")]
pub enum Control {
    /// Exit the game
    Exit = 1,
    /// Move forward
    MoveForward = 2,
    /// Move backward
    MoveBackward = 3,
    /// Move left
    MoveLeft = 4,
    /// Move right
    MoveRight = 5,
    /// Move the ghost brick forward
    MoveGhostForward = 6,
    /// Move the ghost brick backward
    MoveGhostBackward = 7,
    /// Move the ghost brick left
    MoveGhostLeft = 8,
    /// Move the ghost brick right
    MoveGhostRight = 9,
    /// Move the ghost brick up
    MoveGhostUp = 10,
    /// Move the ghost brick down
    MoveGhostDown = 11,
    /// Enable/disable proportional movement on the ghost brick
    MoveGhostProportional = 12,
    /// Rotate the ghost brick clockwise
    RotateGhostCw = 13,
    /// Rotate the ghost brick counter-clockwise
    RotateGhostCcw = 14,
    /// Cycle the ghost brick to the previous brick type
    CycleGhostPrev = 15,
    /// Cycle the ghost brick to the next brick type
    CycleGhostNext = 16,
    /// Cycle the ghost brick to the previous color in the palette
    PalettePrev = 17,
    /// Cycle the ghost brick to the next color in the palette
    PaletteNext = 18,
    /// Remove the ghost brick
    DispelGhost = 19,
    /// Jump
    Jump = 20,
    /// Enable/disable crawling
    Crawl = 21,
    /// Build the ghost brick
    Build = 22,
    /// Interact with whatever is in front of the player
    Interact = 23,
    /// Use the jetpack
    Jetpack = 24,
    /// Cycle to the previous tool
    PrevTool = 25,
    /// Cycle to the next tool
    NextTool = 26,
}

impl Control {
    /// Turn a `u8` into a `Control`.
    pub fn from_u8(control: u8) -> Option<Control> {
        match control {
            1 => Some(Control::Exit),
            2 => Some(Control::MoveForward),
            3 => Some(Control::MoveBackward),
            4 => Some(Control::MoveLeft),
            5 => Some(Control::MoveRight),
            6 => Some(Control::MoveGhostForward),
            7 => Some(Control::MoveGhostBackward),
            8 => Some(Control::MoveGhostLeft),
            9 => Some(Control::MoveGhostRight),
            10 => Some(Control::MoveGhostUp),
            11 => Some(Control::MoveGhostDown),
            12 => Some(Control::MoveGhostProportional),
            13 => Some(Control::RotateGhostCw),
            14 => Some(Control::RotateGhostCcw),
            15 => Some(Control::CycleGhostPrev),
            16 => Some(Control::CycleGhostNext),
            17 => Some(Control::PalettePrev),
            18 => Some(Control::PaletteNext),
            19 => Some(Control::DispelGhost),
            20 => Some(Control::Jump),
            21 => Some(Control::Crawl),
            22 => Some(Control::Build),
            23 => Some(Control::Interact),
            24 => Some(Control::Jetpack),
            25 => Some(Control::PrevTool),
            26 => Some(Control::NextTool),
            _ => None,
        }
    }
}

/// Mapping from keyboard keys and mouse buttons to controls.
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct ControlConfig {
    /// Mapping from keyboard keys to their controls
    pub keyboard: HashMap<Key, Control>,
    /// Mapping from mouse buttons to their controls
    pub mouse: HashMap<MouseButton, Control>,
}

/// Keeps track of what controls are activated and when controls are activated or deactivated.
#[derive(Debug, Clone, Default)]
pub struct ControlState {
    pub pressed: u32,
    pub events: Vec<(Action, Control)>,
}

impl Dump for ControlState {
    fn dump_to<T: FrameTarget>(&self, writer: &mut FrameWriter<T>) {
        writer.write(self.pressed);
        writer.write(self.events.len() as u8);
        for (action, control) in &self.events {
            let action_bit = match action {
                Action::Press => 0x80,
                Action::Release => 0x00,
            };
            writer.write(*control as u8 | action_bit);
        }
    }
}

impl<'a> Grab<'a> for ControlState {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<ControlState> {
        let pressed = reader.read()?;
        let len: u8 = reader.read()?;
        let mut events = Vec::with_capacity(len as usize);
        for _ in 0..len {
            let evt: u8 = reader.read()?;
            let action = if evt & 0x80 != 0 {
                Action::Press
            } else {
                Action::Release
            };
            let control = Control::from_u8(evt & 0x7F)?;
            events.push((action, control));
        }
        Some(ControlState { pressed, events })
    }
}

impl ControlState {
    /// Handle a control event.
    pub fn handle(&mut self, action: Action, control: Control) {
        self.events.push((action, control));
        let bit_offset = control as u8 - 1;
        match action {
            Action::Press => {
                self.pressed = self.pressed | (1 << bit_offset);
            }
            Action::Release => {
                self.pressed = self.pressed & !(1 << bit_offset);
            }
        }
    }

    /// Check whether a control is currently activated.
    pub fn activated(&self, control: Control) -> bool {
        let bit_offset = control as u8 - 1;
        self.pressed & (1 << bit_offset) != 0
    }
}

/// Manages the controls
#[derive(Debug)]
pub struct ControlsManager {
    mouse_pressed: HashSet<MouseButton>,
    cursor_entered: bool,
    cursor_pos: Option<Vector2<f32>>,
    last_cursor_pos: Option<Vector2<f32>>,
    exit_requested: bool,
    /// The current control state
    pub state: ControlState,
    /// The current control configuration
    pub config: ControlConfig,
}

impl ControlsManager {
    /// Create a new controls manager.
    pub fn new() -> ControlsManager {
        ControlsManager {
            mouse_pressed: HashSet::new(),
            cursor_entered: false,
            cursor_pos: None,
            last_cursor_pos: None,
            exit_requested: false,
            state: ControlState::default(),
            config: ControlConfig::default(),
        }
    }

    /// Handle a key event.
    pub fn handle_key_event(&mut self, action: Action, key: Key) {
        if let Some(control) = self.config.keyboard.get(&key) {
            self.state.handle(action, *control);
            match action {
                Action::Press => {
                    log::info!("control {:?} activated", control);
                }
                Action::Release => {
                    log::info!("control {:?} deactivated", control);
                }
            }
        }
    }

    /// Handle a mouse event.
    pub fn handle_mouse_event(&mut self, action: Action, button: MouseButton) {
        match action {
            Action::Press => {
                self.mouse_pressed.insert(button);
            }
            Action::Release => {
                self.mouse_pressed.remove(&button);
            }
        }
        if let Some(control) = self.config.mouse.get(&button) {
            self.state.handle(action, *control);
            match action {
                Action::Press => {
                    log::info!("control {:?} activated", control);
                }
                Action::Release => {
                    log::info!("control {:?} deactivated", control);
                }
            }
        }
    }

    /// Reset the control state for a new frame.
    ///
    /// This will reset the event queue and the mouse cursor position.
    pub fn reset(&mut self) {
        self.state.events.clear();
        self.last_cursor_pos = self.cursor_pos;
    }

    /// Handle a cursor position change.
    pub fn handle_cursor_pos(&mut self, pos: Vector2<f32>) {
        self.cursor_pos = Some(pos);
    }

    /// Handle the cursor entering and leaving the window.
    pub fn handle_cursor_enter(&mut self, entered: bool) {
        self.cursor_entered = entered;
        if !entered {
            self.cursor_pos = None;
        }
    }

    /// Get the position of the cursor.
    pub fn cursor_pos(&self) -> Option<Vector2<f32>> {
        self.cursor_pos
    }

    /// Get the previous cursor position.
    pub fn last_cursor_pos(&self) -> Option<Vector2<f32>> {
        self.last_cursor_pos
    }

    /// Request the game to exit.
    pub fn request_exit(&mut self) {
        self.exit_requested = true;
    }

    /// Has an exit been requested?
    pub fn exit_requested(&self) -> bool {
        self.exit_requested
    }
}
