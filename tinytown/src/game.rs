use {
    cgmath::{prelude::*, PerspectiveFov, Point2, Point3, Quaternion, Rad, Vector2, Vector3},
    serde::{Deserialize, Serialize},
    specs::prelude::*,
    std::{
        collections::VecDeque,
        fs, io,
        net::SocketAddr,
        path::{Path, PathBuf},
        thread,
        time::{Duration, Instant},
    },
    tinytown_collision::Shape,
};

use crate::{
    backend::Backend,
    brick::{BrickDataStore, BrickEventQueue, BrickOrientation, BrickPalette, Vox},
    camera::Camera,
    collision_manager::CollisionManager,
    components::{Brick, GhostBrick, NetId, Physical, Player},
    consts::{FAR, FOV_DEG, NEAR, TICK_NANOS},
    controls::{Control, ControlConfig, ControlsManager},
    net::{ClientConnection, ConnectionManager, Controller},
    resources::{NetIdAllocator, Sim, Stats, WindowSize},
    systems::{
        BrickCollisionResolver, BrickSpawnerSystem, BuildingSystem, CameraSystem,
        ClientSyncEndSystem, ClientSyncStartSystem, CollisionSystem, ControlsSystem,
        MovementSystem, NetIdMaintainerSystem, PhysicsSystem, ServerSyncEndSystem,
        ServerSyncStartSystem, UiSystem,
    },
    tick::Tick,
    ui::{self, GameUi, Ui},
};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Config {
    #[serde(flatten)]
    pub control_config: ControlConfig,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum GameMode {
    Client,
    Server,
    Single,
}

impl GameMode {
    pub fn is_client(self) -> bool {
        if let GameMode::Client = self {
            true
        } else {
            false
        }
    }

    pub fn is_server(self) -> bool {
        if let GameMode::Server = self {
            true
        } else {
            false
        }
    }

    pub fn is_single(self) -> bool {
        if let GameMode::Single = self {
            true
        } else {
            false
        }
    }
}

#[derive(Debug, Clone)]
enum Command {
    LoadBuild(PathBuf),
}

#[derive(Debug, Clone)]
pub struct GameParams {
    pub mode: GameMode,
    pub listen_addr: Option<SocketAddr>,
    pub connect_addr: Option<SocketAddr>,
    pub my_name: Option<String>,
}

pub struct Game<B: Backend> {
    config: Config,
    params: GameParams,
    is_server: bool,
    is_client: bool,
    last_tick: Instant,
    command_queue: VecDeque<Command>,
    world: World,
    backend: B,
}

impl<B: Backend> Game<B> {
    pub fn new(backend: B, config: Config, params: GameParams) -> Game<B> {
        let is_server = params.listen_addr.is_some();
        let is_client = params.connect_addr.is_some();
        Game {
            config,
            params,
            is_server,
            is_client,
            last_tick: Instant::now(),
            command_queue: VecDeque::new(),
            world: World::new(),
            backend,
        }
    }

    pub fn init_camera(&mut self) {
        let perspective_fov = PerspectiveFov {
            near: NEAR,
            far: FAR,
            fovy: Rad(FOV_DEG * ::std::f32::consts::PI / 180.),
            aspect: self.backend.aspect_ratio(),
        };
        let mut camera = Camera::new(
            Point3::new(0., 10., 0.),
            Vector3::new(0., 0., 1.),
            Vector3::new(0., 1., 0.),
            perspective_fov,
        );
        camera.look_at(Point3::new(10., 0.3, 10.));
        self.world.insert(camera);
    }

    pub fn init_components(&mut self) {
        self.world.register::<Brick>();
        self.world.register::<GhostBrick>();
        self.world.register::<Player>();
        self.world.register::<Physical>();
        self.world.register::<NetId>(); // TODO: might want to make it so this isn't needed when netcode isn't in use
    }

    pub fn init_resources(&mut self) {
        self.world.insert(Tick(0));
        self.world.insert(BrickDataStore::new());
        self.world.insert(CollisionManager::default());
        self.world.insert(BrickPalette::new());
        self.world.insert(BrickEventQueue::new());
        self.world.insert(Stats::new());
        self.world.insert(NetIdAllocator::new());
        self.world.insert(self.params.mode);
        self.world.insert(Sim {
            paused: self.is_client,
        });
        if self.is_server {
            let addr = self.params.listen_addr.unwrap();
            self.world
                .insert(ConnectionManager::bind(addr, 16).unwrap()); // TODO: unwraps
        }
        if !self.is_server {
            self.world.insert(ControlsManager::new());
            let (width, height) = self.backend.size();
            self.world.insert(WindowSize { width, height });
            self.init_camera();
        }
        if self.is_client {
            let addr = self.params.connect_addr.unwrap();
            let client_connection =
                ClientConnection::connect(addr, self.params.my_name.as_ref().unwrap()).unwrap(); // TODO: unwraps
            self.world.insert(client_connection);
        }
        if !self.is_server {
            let window_size = self.world.fetch::<WindowSize>();
            let proxy = self.backend.texture_manager_proxy();
            let mut ui = Ui::new(&proxy, window_size.size());
            let game_ui = GameUi::new(&mut ui);
            drop(window_size);
            self.world.insert(ui);
            self.world.insert(game_ui);
        }
    }

    pub fn load_bricks(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        let path = path.as_ref();
        log::info!("loading bricks from directory {}", path.display());
        let mut bds = self.world.fetch_mut::<BrickDataStore>();
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let brick_path = entry.path();
            if brick_path.is_file() {
                log::info!("loading brick from {}", brick_path.display());
                bds.load_brick(brick_path)?;
            }
        }
        Ok(())
    }

    pub fn load_palette(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        let path = path.as_ref();
        log::info!("loading brick color palette from file {}", path.display());
        let mut bp = self.world.fetch_mut::<BrickPalette>();
        bp.load_palette(path)?;
        Ok(())
    }

    pub fn load_build(&mut self, path: impl AsRef<Path>) {
        self.command_queue
            .push_back(Command::LoadBuild(path.as_ref().to_owned()));
    }

    fn load_build_now(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        // preconditions:
        //   - palette is empty
        //   - the brick types are already loaded in the brick data store
        let path = path.as_ref();
        log::info!("loading ttb {}", path.display());
        let f = std::fs::File::open(path)?;
        let mut reader = ttb::Reader::new(f);
        let mut palette = self.world.fetch_mut::<BrickPalette>();
        let bds = self.world.fetch_mut::<BrickDataStore>();
        let mut color_ids = [None; 256];
        let mut bricks = Vec::new();
        while let Some(evt) = reader.next_event()? {
            match evt {
                ttb::ReaderEvent::Meta {
                    build_name,
                    build_description,
                } => {
                    log::info!("build name: {}", build_name);
                    log::info!("build description: {}", build_description);
                }
                ttb::ReaderEvent::Color { id, color } => {
                    let color_id = palette.add_color(color);
                    color_ids[id as usize] = Some(color_id);
                    log::trace!("color id {} mapped to {:?}", id, color_id);
                }
                ttb::ReaderEvent::Brick {
                    brick_type,
                    x,
                    y,
                    z,
                    orientation,
                    color_id,
                } => {
                    log::trace!(
                        "loading brick {:?} at {} {} {}, orientation {}, color_id {}",
                        brick_type,
                        x,
                        y,
                        z,
                        orientation,
                        color_id
                    );
                    if let Some(brick_data_id) = bds.find_by_name(brick_type) {
                        if let Some(brick_color_id) = color_ids[color_id as usize] {
                            bricks.push(Brick::new(
                                brick_color_id,
                                brick_data_id,
                                BrickOrientation(orientation),
                                Vox::new(x, y, z),
                            ));
                        } else {
                            unreachable!()
                        }
                    } else {
                        log::error!(
                            "can't find brick type {:?} in the brick data store",
                            brick_type
                        );
                    }
                }
            }
        }
        drop(palette);
        drop(bds);
        for brick in bricks {
            self.world.create_entity().with(brick).build();
        }
        self.world.fetch_mut::<BrickEventQueue>().dirty = true;
        Ok(())
    }

    pub fn init(&mut self) -> anyhow::Result<()> {
        self.init_components();
        self.init_resources();
        self.load_bricks("resources/bricks")?;
        self.load_palette("resources/palette.txt")?;
        Ok(())
    }

    pub fn start(&mut self) -> anyhow::Result<()> {
        let mut client_start_dispatcher = DispatcherBuilder::new()
            .with(ClientSyncStartSystem, "client_sync_start", &[])
            .build();
        let mut server_start_dispatcher = DispatcherBuilder::new()
            .with(ServerSyncStartSystem, "server_sync_start", &[])
            .build();
        let mut controls_dispatcher = DispatcherBuilder::new()
            .with(ControlsSystem, "controls", &[])
            .build();
        let mut sim_dispatcher = DispatcherBuilder::new()
            .with(MovementSystem, "movement", &[])
            .with(PhysicsSystem, "physics", &["movement"])
            .with(CollisionSystem::new(&self.world), "collision", &["physics"])
            .with(
                BrickCollisionResolver::new(&self.world),
                "brick_Collision_resolver",
                &["collision"],
            )
            .with(BuildingSystem, "building", &["collision"])
            .with(BrickSpawnerSystem, "brick_spawner", &[])
            .build();
        let mut camera_dispatcher = DispatcherBuilder::new()
            .with(CameraSystem, "camera", &[])
            .build();
        let mut ui_dispatcher = DispatcherBuilder::new().with(UiSystem, "ui", &[]).build();
        let mut client_end_dispatcher = DispatcherBuilder::new()
            .with(ClientSyncEndSystem, "client_sync_end", &[])
            .build();
        let mut server_end_dispatcher = DispatcherBuilder::new()
            .with(ServerSyncEndSystem, "server_sync_end", &[])
            .build();
        let mut net_id_maintainer_dispatcher = DispatcherBuilder::new()
            .with(
                NetIdMaintainerSystem::new(&self.world),
                "net_id_maintainer",
                &[],
            )
            .build();

        let tick_duration = Duration::from_nanos(TICK_NANOS);

        if !self.is_server {
            self.world.fetch_mut::<ControlsManager>().config = self.config.control_config.clone();
            // TODO: don't do this on the client
            self.world
                .create_entity()
                .with(Player::new(Vector3::new(1., 0., 0.), Controller::Local))
                .with(Physical::new(
                    Point3::new(0., 10., 0.),
                    Quaternion::one(),
                    Shape::Cuboid(Vector3::new(0.5, 1., 0.5)),
                ))
                .build();
        }

        for command in self.command_queue.clone().drain(..) {
            match command {
                Command::LoadBuild(path) => {
                    self.load_build_now(path)?;
                }
            }
        }

        'app: loop {
            let elapsed = self.last_tick.elapsed();
            if elapsed < tick_duration {
                thread::sleep(tick_duration - elapsed);
            }
            log::info!(
                "starting tick {}, elapsed time: {:?}",
                self.world.fetch::<Tick>().0,
                self.last_tick.elapsed()
            );
            self.last_tick += tick_duration;
            self.backend.handle_events(&self.world)?;
            if self.is_client {
                client_start_dispatcher.dispatch(&mut self.world);
            }
            if self.is_server {
                server_start_dispatcher.dispatch(&mut self.world);
            }
            if !self.is_server {
                controls_dispatcher.dispatch(&mut self.world);
            }
            if !self.world.fetch::<Sim>().paused {
                sim_dispatcher.dispatch(&mut self.world);
            }
            if !self.is_server {
                camera_dispatcher.dispatch(&mut self.world);
                ui_dispatcher.dispatch(&mut self.world);
            }
            if self.is_client {
                client_end_dispatcher.dispatch(&mut self.world);
            }
            if self.is_server {
                server_end_dispatcher.dispatch(&mut self.world);
            }
            if self.is_server || self.is_client {
                net_id_maintainer_dispatcher.dispatch(&mut self.world);
            }
            self.world.maintain();
            self.world
                .fetch_mut::<Ui>()
                .layout(self.world.fetch::<WindowSize>().size());
            let start_render = Instant::now();
            self.backend.render(&self.world)?;
            self.world
                .fetch_mut::<Stats>()
                .render_time
                .push(start_render.elapsed().subsec_nanos() / 1000_000);
            if !self.is_server {
                let controls_manager = self.world.fetch::<ControlsManager>();
                if controls_manager.state.activated(Control::Exit)
                    || controls_manager.exit_requested()
                {
                    break 'app;
                }
            }
            self.world.fetch::<Stats>().print_stats();
            self.world.fetch_mut::<Tick>().0 += 1;
        }

        Ok(())
    }
}
