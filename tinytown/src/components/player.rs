use {cgmath::Vector3, specs::prelude::*};

use crate::{
    brick::{BrickColorId, BrickDataId},
    controls::ControlState,
    net::Controller,
    ui::Tool,
};

/// Player data component
pub struct Player {
    /// The direction this player is facing
    pub facing: Vector3<f32>,
    /// Whether the player can jump
    pub can_jump: bool,
    /// The entity id of the player's ghost brick, if any
    pub ghost_brick: Option<Entity>,
    /// The player's selected color
    pub selected_color_id: BrickColorId,
    /// The player's selected brick type
    pub selected_data_id: BrickDataId,
    /// The controller of the player
    pub controller: Controller,
    /// The control state of the player
    pub controls: ControlState,
    /// The current tool that the player is using
    pub tool: Tool,
}

impl Player {
    /// Create a new `Player` component.
    pub fn new(facing: Vector3<f32>, controller: Controller) -> Player {
        Player {
            facing,
            can_jump: false,
            ghost_brick: None,
            selected_color_id: BrickColorId(0),
            selected_data_id: BrickDataId(0),
            controller,
            controls: ControlState::default(),
            tool: Tool::Brick,
        }
    }

    /// Get the offset from the center of the player to their eye.
    pub fn eye_offset(&self) -> Vector3<f32> {
        Vector3::new(0., 1., 0.)
    }
}

impl Component for Player {
    type Storage = HashMapStorage<Player>;
}
