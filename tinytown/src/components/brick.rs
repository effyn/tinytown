use {
    cgmath::{Point3, Quaternion},
    specs::prelude::*,
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation, Vox},
    util::Directional,
};

/// A brick
#[derive(Clone, Debug)]
pub struct Brick {
    /// The color of the brick
    pub color_id: BrickColorId,
    /// The type of brick
    pub data_id: BrickDataId,
    /// The brick's orientation
    pub orientation: BrickOrientation,
    /// The brick's position
    pub pos: Vox,
    /// Whether the brick is covered in a specific direction
    pub covered: Directional<bool>,
    #[doc(hidden)]
    pub dirty: bool, // TODO: i'm sure there is a better way to do it
                     // TODO: maybe specify what bit it occupies in the dirty component bitset
}

impl Component for Brick {
    type Storage = FlaggedStorage<Brick, VecStorage<Brick>>;
}

impl Brick {
    /// Create a new brick component.
    pub fn new(
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Vox,
    ) -> Brick {
        Brick {
            color_id,
            data_id,
            orientation,
            pos,
            covered: Directional::all(false),
            dirty: true,
        }
    }
}

/// A ghost brick
#[derive(Clone, Debug)]
pub struct GhostBrick {
    /// The color of the ghost brick
    pub color_id: BrickColorId,
    /// The type of ghost brick
    pub data_id: BrickDataId,
    /// The ghost brick's orientation
    pub orientation: BrickOrientation,
    /// The ghost brick's position
    pub pos: Vox,
    /// The location to draw the ghost brick at
    pub visual_pos: Point3<f32>,
    /// The rotation to draw the ghost brick with
    pub visual_rot: Quaternion<f32>,
}

impl Component for GhostBrick {
    type Storage = VecStorage<Self>;
}
