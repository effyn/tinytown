use {
    cgmath::{Point3, Quaternion, Vector3},
    specs::prelude::*,
    tinytown_collision::{Aabb, Shape, TransformedShape},
};

/// A physical entity
#[derive(Debug)]
pub struct Physical {
    /// The position of the entity
    pub pos: Point3<f32>,
    /// The rotation of the entity
    pub rot: Quaternion<f32>,
    /// The positional velocity of the entity
    pub vel: Vector3<f32>,
    /// The shape of the tntiy
    pub shape: Shape,
}

impl Physical {
    /// Create a new `Physical` component.
    pub fn new(pos: Point3<f32>, rot: impl Into<Quaternion<f32>>, shape: Shape) -> Physical {
        Physical {
            pos,
            rot: rot.into(),
            vel: Vector3::new(0., 0., 0.),
            shape,
        }
    }

    /// Get the shape of the component, transformed according to position and rotation.
    pub fn transformed_shape(&self) -> TransformedShape {
        self.shape.with_transform(self.pos, self.rot)
    }

    /// Get the minimal AABB that contains the entire entity.
    pub fn aabb(&self) -> Aabb {
        self.transformed_shape().aabb()
    }
}

impl Component for Physical {
    type Storage = FlaggedStorage<Self, VecStorage<Self>>;
}
