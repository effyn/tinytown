use {std::io, thiserror::Error};

use crate::loaders::brick_loader::BrickLoaderError;

#[derive(Debug, Error)]
pub enum GameError {
    #[error("IO error: {0:}")]
    IoError(#[from] io::Error),
    #[error("Brick loader error: {0:}")]
    BrickLoaderError(#[from] BrickLoaderError),
}

pub type GameResult<T> = Result<T, GameError>;
