//! Brick grid

use {
    cgmath::{prelude::*, Point3, Vector3},
    std::collections::HashMap,
    tinytown_collision::{Aabb, EuclidElementWise, Ray, RaycastHit},
};

use super::{Vox, VoxAabb, VOXEL_SIZE};

/// The size of a cell, in brick voxels
pub const CELL_BRICK_SIZE: Vector3<i32> = Vector3 {
    x: 16,
    y: 16,
    z: 16,
};

/// The size of a cell, in world units
pub const CELL_WORLD_SIZE: Vector3<f32> = Vector3 {
    x: CELL_BRICK_SIZE.x as f32 * VOXEL_SIZE.x,
    y: CELL_BRICK_SIZE.y as f32 * VOXEL_SIZE.y,
    z: CELL_BRICK_SIZE.z as f32 * VOXEL_SIZE.z,
};

fn world_to_cell(world_pos: Point3<f32>) -> Point3<i32> {
    Point3::new(
        world_pos.x.div_euclid(CELL_WORLD_SIZE.x) as i32,
        world_pos.y.div_euclid(CELL_WORLD_SIZE.y) as i32,
        world_pos.z.div_euclid(CELL_WORLD_SIZE.z) as i32,
    )
}

fn brick_to_cell(brick_pos: Vox) -> Point3<i32> {
    Point3::new(
        brick_pos.x.div_euclid(CELL_BRICK_SIZE.x),
        brick_pos.y.div_euclid(CELL_BRICK_SIZE.y),
        brick_pos.z.div_euclid(CELL_BRICK_SIZE.z),
    )
}

#[derive(Debug, Clone)]
struct GridCell<T> {
    contents: Vec<(VoxAabb, T)>,
}

/// A datastructure containing the brick grid and functions to modify and query it.
#[derive(Debug, Clone)]
pub struct BrickGrid<T> {
    cells: HashMap<Point3<i32>, GridCell<T>>,
}

impl<T> BrickGrid<T> {
    /// Create a new brick grid
    pub fn new() -> BrickGrid<T> {
        BrickGrid {
            cells: HashMap::new(),
        }
    }

    /// Check whether the grid is empty
    pub fn is_empty(&self) -> bool {
        self.cells.is_empty()
    }
}

impl<T: PartialEq + Clone + Copy> BrickGrid<T> {
    /// Add a voxel AABB to the brick grid.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         grid::BrickGrid,
    /// #         VoxAabb,
    /// #         Vox,
    /// #     },
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut grid = BrickGrid::new();
    ///
    /// grid.add(VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)), 0);
    /// ```
    pub fn add(&mut self, vox_aabb: VoxAabb, value: T) -> bool {
        let min_pos = brick_to_cell(vox_aabb.min);
        let max_pos = brick_to_cell(vox_aabb.max + Vector3::new(1, 1, 1));
        for iz in min_pos.z..=max_pos.z {
            for iy in min_pos.y..=max_pos.y {
                for ix in min_pos.x..=max_pos.x {
                    let point = Point3::new(ix, iy, iz);
                    self.cells
                        .entry(point)
                        .or_insert(GridCell {
                            contents: Vec::new(),
                        })
                        .contents
                        .push((vox_aabb, value));
                }
            }
        }
        true // TODO: make it return false if it collides with any existing brick
             //       or maybe there is another solution?
    }

    /// Remove a voxel AABB from the brick grid.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         grid::BrickGrid,
    /// #         VoxAabb,
    /// #         Vox,
    /// #     },
    /// # };
    /// let mut grid = BrickGrid::new();
    ///
    /// grid.add(VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)), 0);
    ///
    /// assert!(!grid.is_empty());
    ///
    /// grid.remove(VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)), 0);
    ///
    /// assert!(grid.is_empty());
    /// ```
    pub fn remove(&mut self, vox_aabb: VoxAabb, value: T) -> bool {
        let mut success = false;
        let min_pos = brick_to_cell(vox_aabb.min);
        let max_pos = brick_to_cell(vox_aabb.max + Vector3::new(1, 1, 1));
        for iz in min_pos.z..=max_pos.z {
            for iy in min_pos.y..=max_pos.y {
                for ix in min_pos.x..=max_pos.x {
                    let point = Point3::new(ix, iy, iz);
                    if let Some(cell) = self.cells.get_mut(&point) {
                        cell.contents.retain(|&(_cell_aabb, cell_value)| {
                            if value == cell_value {
                                success = true;
                                false
                            } else {
                                true
                            }
                        });
                        if cell.contents.is_empty() {
                            self.cells.remove(&point);
                        }
                    }
                }
            }
        }
        success
    }

    /// Cast a ray in the brick grid.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         grid::BrickGrid,
    /// #         VoxAabb,
    /// #         Vox,
    /// #     },
    /// #     tinytown_collision::{
    /// #         Ray,
    /// #     },
    /// #     approx::{
    /// #         assert_ulps_eq,
    /// #     },
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut grid = BrickGrid::new();
    ///
    /// let vox_aabbs = [
    ///     VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)),
    ///     VoxAabb::new(Vox::new(3, 3, 3), Vox::new(3, 3, 3)),
    ///     VoxAabb::new(Vox::new(0, 1, 0), Vox::new(4, 1, 4)),
    /// ];
    ///
    /// for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
    ///     grid.add(vox_aabb, i);
    /// }
    ///
    /// let ray = Ray::new(Point3::new(0.25, 5., 0.25), Vector3::new(0., -1., 0.));
    ///
    /// let hit = grid.cast_ray(ray, 100.).unwrap();
    ///
    /// assert_eq!(hit.value, 2);
    /// assert_ulps_eq!(hit.point, Point3::new(0.25, 0.4, 0.25));
    /// assert_ulps_eq!(hit.distance, 4.6);
    /// assert_ulps_eq!(hit.normal, Vector3::new(0., 1., 0.));
    /// ```
    pub fn cast_ray(&self, ray: Ray, limit: f32) -> Option<RaycastHit<T>>
    where
        T: std::fmt::Debug,
    {
        // TODO: narrow phase
        log::debug!(
            "casting ray from {:?} in dir {:?}, limit {}",
            ray.origin,
            ray.dir,
            limit
        );
        let step =
            ray.dir
                .map(|x| x.signum())
                .cast::<i32>()
                .unwrap()
                .map(|x| if x == 0 { 1 } else { x });
        let mut t_max = ray.origin.to_vec().rem_euclid_element_wise(CELL_WORLD_SIZE);
        if step.x == 1 {
            t_max.x = CELL_WORLD_SIZE.x - t_max.x;
        }
        if step.y == 1 {
            t_max.y = CELL_WORLD_SIZE.y - t_max.y;
        }
        if step.z == 1 {
            t_max.z = CELL_WORLD_SIZE.z - t_max.z;
        }
        let dir_abs = ray.dir.map(|x| x.abs());
        t_max = t_max.div_element_wise(dir_abs);
        let t_delta = CELL_WORLD_SIZE.div_element_wise(dir_abs);
        let mut cur = world_to_cell(ray.origin);
        log::trace!(
            "cur={:?}, t_max={:?}, step={:?}, dir_abs={:?}, t_delta={:?}",
            cur,
            t_max,
            step,
            dir_abs,
            t_delta
        );
        while ray.origin.distance(Point3::from_vec(
            cur.cast::<f32>()
                .unwrap()
                .to_vec()
                .mul_element_wise(CELL_WORLD_SIZE),
        )) < limit + CELL_WORLD_SIZE.sum()
        {
            // TODO: i'm sure this can be done better
            log::trace!("iteration start: cur={:?} t_max={:?}", cur, t_max);
            let cell_vox_aabb = VoxAabb::new(
                Point3::from_vec(cur.to_vec().mul_element_wise(CELL_BRICK_SIZE)).into(),
                Point3::from_vec(cur.to_vec().mul_element_wise(CELL_BRICK_SIZE) + CELL_BRICK_SIZE)
                    .into(),
            );
            log::trace!("cell brick aabb: {:?}", cell_vox_aabb);
            log::trace!("cell world aabb: {:?}", Aabb::from(cell_vox_aabb));
            if let Some(cell) = self.cells.get(&cur) {
                log::trace!("cell hit!");
                let mut best = None;
                let mut best_distance = limit;
                for &(vox_aabb, value) in &cell.contents {
                    let world_aabb: Aabb = vox_aabb.into();
                    log::trace!("checking cell entry with aabb {:?}", world_aabb);
                    if let Some(distance) = world_aabb.cast_ray(ray) {
                        log::trace!("hit! distance={} vox_aabb={:?}", distance, vox_aabb);
                        if distance < best_distance {
                            let point = ray.point_at(distance);
                            let normal = world_aabb.normal_at(point);
                            // TODO: narrow phase
                            best = Some(RaycastHit {
                                point,
                                normal,
                                distance,
                                value,
                            });
                            best_distance = distance;
                        }
                    }
                }
                if let Some(hit) = best {
                    if hit.distance >= limit {
                        log::trace!(
                            "discarding hit due to distance: {} >= {}",
                            hit.distance,
                            limit
                        );
                        return None;
                    } else {
                        log::trace!("accepting hit");
                        return Some(hit);
                    }
                }
            }
            if t_max.x < t_max.y {
                if t_max.x < t_max.z {
                    cur.x += step.x;
                    t_max.x += t_delta.x;
                } else {
                    cur.z += step.z;
                    t_max.z += t_delta.z;
                }
            } else if t_max.y < t_max.z {
                cur.y += step.y;
                t_max.y += t_delta.y;
            } else {
                cur.z += step.z;
                t_max.z += t_delta.z;
            }
        }
        None
    }

    /// Query all voxel AABBs which intersect a queried voxel AABB.
    ///
    /// This will probably yield duplicates.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         grid::BrickGrid,
    /// #         VoxAabb,
    /// #         Vox,
    /// #     },
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut grid = BrickGrid::new();
    ///
    /// let vox_aabbs = [
    ///     VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)),
    ///     VoxAabb::new(Vox::new(3, 3, 3), Vox::new(3, 3, 3)),
    ///     VoxAabb::new(Vox::new(0, 1, 0), Vox::new(4, 1, 4)),
    /// ];
    ///
    /// for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
    ///     grid.add(vox_aabb, i);
    /// }
    ///
    /// let intersects = [true, false, true];
    /// let mut results = [false; 3];
    ///
    /// let query_vox_aabb = VoxAabb::new(Vox::new(-1, 0, -1), Vox::new(1, 1, 1));
    ///
    /// for i in grid.query_vox_aabb(query_vox_aabb) {
    ///     results[i] = true;
    /// }
    ///
    /// assert_eq!(intersects, results);
    /// ```
    pub fn query_vox_aabb<'a>(&'a self, vox_aabb: VoxAabb) -> VoxAabbQueryIterator<'a, T> {
        let cell_min = brick_to_cell(vox_aabb.min);
        let cell_max = brick_to_cell(vox_aabb.max + Vector3::new(1, 1, 1));
        let cell_point = cell_min;
        VoxAabbQueryIterator {
            grid: self,
            vox_aabb,
            first: true,
            cell_min,
            cell_max,
            point: cell_point,
            inner: None,
        }
    }

    /// Query all voxel AABBs which intersect a queried world AABB.
    ///
    /// This will probably yield duplicates.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         grid::BrickGrid,
    /// #         VoxAabb,
    /// #         Vox,
    /// #     },
    /// #     tinytown_collision::Aabb,
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut grid = BrickGrid::new();
    ///
    /// let vox_aabbs = [
    ///     VoxAabb::new(Vox::new(1, 1, 1), Vox::new(2, 2, 2)),
    ///     VoxAabb::new(Vox::new(3, 3, 3), Vox::new(3, 3, 3)),
    ///     VoxAabb::new(Vox::new(0, 1, 0), Vox::new(4, 1, 4)),
    /// ];
    ///
    /// for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
    ///     grid.add(vox_aabb, i);
    /// }
    ///
    /// let intersects = [true, false, true];
    /// let mut results = [false; 3];
    ///
    /// let query_aabb = Aabb::new(Point3::new(-0.2, 0., -0.2), Point3::new(1., 1., 1.));
    ///
    /// for i in grid.query_world_aabb(query_aabb) {
    ///     results[i] = true;
    /// }
    ///
    /// assert_eq!(intersects, results);
    /// ```
    pub fn query_world_aabb<'a>(&'a self, aabb: Aabb) -> impl Iterator<Item = T> + 'a {
        let vox_aabb = VoxAabb::smallest_containing_world_aabb(aabb);
        let min_pos = brick_to_cell(vox_aabb.min);
        let max_pos = brick_to_cell(vox_aabb.max + Vector3::new(1, 1, 1));
        (min_pos.z..=max_pos.z)
            .flat_map(move |z| (min_pos.y..=max_pos.y).map(move |y| (z, y)))
            .flat_map(move |(z, y)| (min_pos.x..=max_pos.x).map(move |x| (x, y, z)))
            .filter_map(move |(x, y, z)| self.cells.get(&Point3::new(x, y, z)))
            .flat_map(|cell| cell.contents.iter())
            .filter(move |&&(entry_aabb, _)| aabb.intersects(entry_aabb.into()))
            .map(|(_, value)| *value)
    }
}

/// An iterator that yields the values of all voxel AABBs intersecting a given voxel AABB.
///
/// Constructed with `BrickGrid::query_vox_aabb`.
pub struct VoxAabbQueryIterator<'a, T: PartialEq + Clone + Copy> {
    grid: &'a BrickGrid<T>,
    vox_aabb: VoxAabb,
    first: bool,
    cell_min: Point3<i32>,
    cell_max: Point3<i32>,
    point: Point3<i32>,
    inner: Option<std::slice::Iter<'a, (VoxAabb, T)>>,
}

impl<'a, T: PartialEq + Clone + Copy> Iterator for VoxAabbQueryIterator<'a, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(ref mut inner) = self.inner {
                loop {
                    match inner.next() {
                        Some(&(vox_aabb, value)) => {
                            if self.vox_aabb.intersects(vox_aabb) {
                                return Some(value);
                            }
                        }
                        None => {
                            self.inner = None;
                            break;
                        }
                    }
                }
            }
            if self.first {
                self.first = false;
            } else {
                self.point.x += 1;
                if self.point.x > self.cell_max.x {
                    self.point.y += 1;
                    self.point.x = self.cell_min.x;
                    if self.point.y > self.cell_max.y {
                        self.point.z += 1;
                        self.point.y = self.cell_min.y;
                        if self.point.z > self.cell_max.z {
                            return None;
                        }
                    }
                }
            }
            if let Some(cell) = self.grid.cells.get(&self.point) {
                if !cell.contents.is_empty() {
                    self.inner = Some(cell.contents.iter());
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use {
        approx::assert_ulps_eq,
        core::ops::Range,
        proptest::{prop_compose, proptest, test_runner::Config},
        tinytown_testutil::{prop_point3_all, prop_vec3_all, prop_vec3_unit},
    };

    use super::*;

    #[test]
    fn brick_grid_raycast_works_a() {
        let ray = Ray::new(
            Point3::new(38.792145, 0.0, 54.014038),
            Vector3::new(-0.39647558, 0.58395565, -0.70838046),
        );

        let vox_aabb = VoxAabb::new(Vox::new(61, 5, 93), Vox::new(75, 6, 104));

        let mut grid = BrickGrid::new();
        grid.add(vox_aabb, 0);

        let hit = grid.cast_ray(ray, 100.).unwrap();

        assert_ulps_eq!(hit.normal, Vector3::new(0., 0., 1.));
        assert_ulps_eq!(hit.point, Point3::new(37.944748, 1.2481021, 52.5));
        assert_eq!(hit.value, 0);
    }

    #[test]
    fn brick_grid_raycast_works_b() {
        let ray = Ray::new(Point3::new(0., 2., 0.), Vector3::new(-1., 0., 0.));

        let vox_aabb = VoxAabb::new(Vox::new(-9, 0, -7), Vox::new(-8, 15, 5));

        let mut grid = BrickGrid::new();
        grid.add(vox_aabb, 0);

        let hit = grid.cast_ray(ray, 100.).unwrap();

        assert_ulps_eq!(hit.normal, Vector3::new(1., 0., 0.));
        assert_ulps_eq!(hit.distance, 3.5);
        assert_ulps_eq!(hit.point, Point3::new(-3.5, 2., 0.));
        assert_eq!(hit.value, 0);
    }

    prop_compose! { // TODO: don't copypaste this
        fn prop_aabb_all
            (center_range: Range<f32>, radius_range: Range<f32>)
            (center in prop_point3_all(center_range), radius in prop_vec3_all(radius_range))
            -> Aabb {
            Aabb::new_centered(center, radius)
        }
    }

    prop_compose! {
        pub fn prop_vox_aabb()(
            x in 0..128,
            y in 0..128,
            z in 0..128,
            w in 1..16,
            h in 1..16,
            d in 1..16,
        ) -> VoxAabb {
            VoxAabb::with_size(Vox::new(x, y, z), Vector3::new(w, h, d))
        }
    }

    proptest! {
        #[test]
        fn prop_brick_grid_query_world_aabb(
            vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..8),
            query_aabbs in proptest::collection::vec(prop_aabb_all(-128. .. 128., 0. .. 8.), 0..16),
        ) {
            let mut grid = BrickGrid::new();

            for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                grid.add(vox_aabb, i);
            }

            for &query_aabb in &query_aabbs {
                let mut hits = [false; 16];
                for value in grid.query_world_aabb(query_aabb) {
                    hits[value] = true;
                }
                for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                    let world_aabb = Aabb::from(vox_aabb);
                    assert_eq!(world_aabb.intersects(query_aabb), hits[i]);
                }
            }
        }
    }

    proptest! {
        #[test]
        fn prop_brick_grid_query_vox_aabb(
            vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..8),
            query_vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..16),
        ) {
            let mut grid = BrickGrid::new();

            for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                grid.add(vox_aabb, i);
            }

            let len = vox_aabbs.len();

            for &query_vox_aabb in &query_vox_aabbs {
                let mut real_hits = vec![false; len];
                let mut test_hits = vec![false; len];
                for value in grid.query_vox_aabb(query_vox_aabb) {
                    test_hits[value] = true;
                }
                for (value, &vox_aabb) in vox_aabbs.iter().enumerate() {
                    real_hits[value] = vox_aabb.intersects(query_vox_aabb);
                }
                assert_eq!(real_hits, test_hits);
            }
        }
    }

    const AXIS_ALIGNED_RAY_DIRS: [Vector3<f32>; 6] = [
        Vector3 {
            x: 1.,
            y: 0.,
            z: 0.,
        },
        Vector3 {
            x: 0.,
            y: 1.,
            z: 0.,
        },
        Vector3 {
            x: 0.,
            y: 0.,
            z: 1.,
        },
        Vector3 {
            x: -1.,
            y: 0.,
            z: 0.,
        },
        Vector3 {
            x: 0.,
            y: -1.,
            z: 0.,
        },
        Vector3 {
            x: 0.,
            y: 0.,
            z: -1.,
        },
    ];

    #[test]
    fn brick_grid_raycast_vox_aabb_axis_aligned() {
        let mut grid = BrickGrid::new();

        grid.add(VoxAabb::new(Vox::new(-1, -1, -1), Vox::new(1, 1, 1)), 0);

        for &ray_dir in &AXIS_ALIGNED_RAY_DIRS[..] {
            eprintln!("trying ray dir {:?}", ray_dir);
            let ray_origin = Point3::origin() - ray_dir * 2.;
            eprintln!("ray origin {:?}", ray_origin);
            let ray = Ray::new(ray_origin, ray_dir);
            let hit = grid.cast_ray(ray, 100.).unwrap();
            assert_ulps_eq!(hit.normal, -ray_dir);
            assert_eq!(hit.value, 0);
            // TODO: check hit point and hit distance
        }
    }

    proptest! {
        #![proptest_config(Config::with_cases(3000))]
        #[test]
        fn prop_brick_grid_raycast_compare_with_brute_force_simple(
            vox_aabb in prop_vox_aabb(),
            ray_origin in prop_point3_all(-128. .. 128.),
            ray_dir in prop_vec3_unit(),
            ray_limit in 1f32 .. 128.,
        ) {
            let mut grid = BrickGrid::new();

            grid.add(vox_aabb, 0);

            let ray = Ray::new(ray_origin, ray_dir);
            let grid_hit = grid.cast_ray(ray, ray_limit);
            let brute_hit = {
                let mut hit_dist = ray_limit;
                let mut hit = None;
                let aabb = Aabb::from(vox_aabb);
                if let Some(distance) = aabb.cast_ray(ray) {
                    if distance < hit_dist {
                        let point = ray.point_at(distance);
                        let normal = aabb.normal_at(point);
                        hit_dist = distance;
                        hit = Some(RaycastHit {
                            distance,
                            normal,
                            point,
                            value: 0,
                        });
                    }
                }
                hit
            };
            match (grid_hit, brute_hit) {
                (Some(grid_hit), Some(brute_hit)) => {
                    assert_ulps_eq!(grid_hit.point, brute_hit.point);
                    assert_ulps_eq!(grid_hit.distance, brute_hit.distance);
                    assert_eq!(grid_hit.value, brute_hit.value);
                },
                (None, Some(_)) | (Some(_), None) => {
                    panic!("hits don't match: {:?} vs {:?}", grid_hit, brute_hit);
                },
                (None, None) => (),
            }
        }
    }

    proptest! {
        #[test]
        fn prop_brick_grid_raycast_compare_with_brute_force_axis_aligned(
            vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..32),
            rays in proptest::collection::vec((
                prop_point3_all(-128. .. 128.),
                proptest::sample::select(&AXIS_ALIGNED_RAY_DIRS[..]),
                1f32 .. 128.), 1..32),
        ) {
            let mut grid = BrickGrid::new();

            for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                grid.add(vox_aabb, i);
            }

            for &(ray_origin, ray_dir, ray_limit) in &rays {
                let ray = Ray::new(ray_origin, ray_dir);
                let grid_hit = grid.cast_ray(ray, ray_limit);
                let brute_hit = {
                    let mut hit_dist = ray_limit;
                    let mut hit = None;
                    for (value, &vox_aabb) in vox_aabbs.iter().enumerate() {
                        let aabb = Aabb::from(vox_aabb);
                        if let Some(distance) = aabb.cast_ray(ray) {
                            if distance < hit_dist {
                                let point = ray.point_at(distance);
                                let normal = aabb.normal_at(point);
                                hit_dist = distance;
                                hit = Some(RaycastHit {
                                    distance,
                                    normal,
                                    point,
                                    value,
                                });
                            }
                        }
                    }
                    hit
                };
                match (grid_hit, brute_hit) {
                    (Some(grid_hit), Some(brute_hit)) => {
                        assert_ulps_eq!(grid_hit.point, brute_hit.point);
                        assert_ulps_eq!(grid_hit.distance, brute_hit.distance);
                    },
                    (None, Some(_)) | (Some(_), None) => {
                        panic!("hits don't match: {:?} vs {:?}", grid_hit, brute_hit);
                    },
                    (None, None) => (),
                }
            }
        }
    }

    proptest! {
        #[test]
        fn prop_brick_grid_raycast_compare_with_brute_force_long_range(
            vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..32),
            rays in proptest::collection::vec((prop_point3_all(-128. .. 128.), prop_vec3_unit()), 1..32),
        ) {
            let mut grid = BrickGrid::new();

            for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                grid.add(vox_aabb, i);
            }

            for &(ray_origin, ray_dir) in &rays {
                let ray = Ray::new(ray_origin, ray_dir);
                let grid_hit = grid.cast_ray(ray, 1000.);
                let brute_hit = {
                    let mut hit_dist = 1000.;
                    let mut hit = None;
                    for (value, &vox_aabb) in vox_aabbs.iter().enumerate() {
                        let aabb = Aabb::from(vox_aabb);
                        if let Some(distance) = aabb.cast_ray(ray) {
                            if distance < hit_dist {
                                let point = ray.point_at(distance);
                                let normal = aabb.normal_at(point);
                                hit_dist = distance;
                                hit = Some(RaycastHit {
                                    distance,
                                    normal,
                                    point,
                                    value,
                                });
                            }
                        }
                    }
                    hit
                };
                match (grid_hit, brute_hit) {
                    (Some(grid_hit), Some(brute_hit)) => {
                        assert_ulps_eq!(grid_hit.point, brute_hit.point);
                        assert_ulps_eq!(grid_hit.distance, brute_hit.distance);
                    },
                    (None, Some(_)) | (Some(_), None) => {
                        panic!("hits don't match: {:?} vs {:?}", grid_hit, brute_hit);
                    },
                    (None, None) => (),
                }
            }
        }
    }

    proptest! {
        #[test]
        fn prop_brick_grid_raycast_compare_with_brute_force(
            vox_aabbs in proptest::collection::vec(prop_vox_aabb(), 0..32),
            rays in proptest::collection::vec((prop_point3_all(-128. .. 128.), prop_vec3_unit(), 1f32 .. 128.), 1..32),
        ) {
            let mut grid = BrickGrid::new();

            for (i, &vox_aabb) in vox_aabbs.iter().enumerate() {
                grid.add(vox_aabb, i);
            }

            for &(ray_origin, ray_dir, ray_limit) in &rays {
                let ray = Ray::new(ray_origin, ray_dir);
                let grid_hit = grid.cast_ray(ray, ray_limit);
                let brute_hit = {
                    let mut hit_dist = ray_limit;
                    let mut hit = None;
                    for (value, &vox_aabb) in vox_aabbs.iter().enumerate() {
                        let aabb = Aabb::from(vox_aabb);
                        if let Some(distance) = aabb.cast_ray(ray) {
                            if distance < hit_dist {
                                let point = ray.point_at(distance);
                                let normal = aabb.normal_at(point);
                                hit_dist = distance;
                                hit = Some(RaycastHit {
                                    distance,
                                    normal,
                                    point,
                                    value,
                                });
                            }
                        }
                    }
                    hit
                };
                match (grid_hit, brute_hit) {
                    (Some(grid_hit), Some(brute_hit)) => {
                        assert_ulps_eq!(grid_hit.point, brute_hit.point);
                        assert_ulps_eq!(grid_hit.distance, brute_hit.distance);
                    },
                    (None, Some(_)) | (Some(_), None) => {
                        panic!("hits don't match: {:?} vs {:?}", grid_hit, brute_hit);
                    },
                    (None, None) => (),
                }
            }
        }
    }

    #[test]
    fn test_brick_grid_works() {
        let mut grid = BrickGrid::new();

        let vox_aabb =
            VoxAabb::with_size(Vox::new(0, 0, 0), CELL_BRICK_SIZE - Vector3::new(1, 1, 1));

        grid.add(vox_aabb, 0);

        assert!(grid.cells[&Point3::new(0, 0, 0)]
            .contents
            .iter()
            .find(|&&(_, value)| value == 0)
            .is_some());

        assert!(!grid.cells.contains_key(&Point3::new(1, 0, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(-1, 0, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 1, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, -1, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 0, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 0, -1)));

        assert!(!grid.cells.contains_key(&Point3::new(1, 1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(1, 1, -1)));
        assert!(!grid.cells.contains_key(&Point3::new(1, -1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(1, -1, -1)));
        assert!(!grid.cells.contains_key(&Point3::new(-1, 1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(-1, 1, -1)));
        assert!(!grid.cells.contains_key(&Point3::new(-1, -1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(-1, -1, -1)));

        grid.remove(vox_aabb, 0);

        assert!(!grid.cells.contains_key(&Point3::new(0, 0, 0)));

        let vox_aabb_b = VoxAabb::with_size(Vox::new(-1, -1, -1), Vector3::new(2, 2, 2));

        grid.add(vox_aabb_b, 1);

        assert!(!grid.cells.contains_key(&Point3::new(1, 0, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(-2, 0, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 1, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, -2, 0)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 0, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(0, 0, -2)));

        assert!(!grid.cells.contains_key(&Point3::new(1, 1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(1, 1, -2)));
        assert!(!grid.cells.contains_key(&Point3::new(1, -2, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(1, -2, -2)));
        assert!(!grid.cells.contains_key(&Point3::new(-2, 1, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(-2, 1, -2)));
        assert!(!grid.cells.contains_key(&Point3::new(-2, -2, 1)));
        assert!(!grid.cells.contains_key(&Point3::new(-2, -2, -1)));

        for ix in -1..=0 {
            for iy in -1..=0 {
                for iz in -1..=0 {
                    assert!(grid.cells.contains_key(&Point3::new(ix, iy, iz)));
                }
            }
        }
    }
}
