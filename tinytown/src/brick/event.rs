//! Brick Event Queue

use specs::Entity;

use super::{BrickColorId, BrickDataId, BrickOrientation, Vox};

/// An event for the brick event queue to process
#[derive(Clone, Debug)]
pub enum BrickEvent {
    /// Build a brick
    Build {
        /// The kind of brick
        data_id: BrickDataId,
        /// The color of the brick
        color_id: BrickColorId,
        /// The orientation of the brick
        orientation: BrickOrientation,
        /// The position of the brick
        pos: Vox,
    },
    /// Destroy a brick
    Destroy {
        /// The brick to destroy
        entity: Entity,
    },
}

/// A queue holding events to apply to bricks.
///
/// Also keeps track of whether the mesh needs to be rebuilt.
#[derive(Clone, Debug)]
pub struct BrickEventQueue {
    data: Vec<BrickEvent>,
    /// Does the mesh need to be rebuilt?
    pub dirty: bool,
}

impl BrickEventQueue {
    /// Create a new brick event queue.
    pub fn new() -> BrickEventQueue {
        BrickEventQueue {
            data: Vec::new(),
            dirty: true,
        }
    }

    /// Build a brick.
    pub fn build(
        &mut self,
        data_id: BrickDataId,
        color_id: BrickColorId,
        orientation: BrickOrientation,
        pos: Vox,
    ) {
        self.data.push(BrickEvent::Build {
            data_id,
            color_id,
            orientation,
            pos,
        });
    }

    /// Destroy a brick.
    pub fn destroy(&mut self, entity: Entity) {
        self.data.push(BrickEvent::Destroy { entity });
    }

    /// Flush the queue, to process the events.
    pub fn flush(&mut self, mut f: impl FnMut(BrickEvent)) {
        if !self.data.is_empty() {
            self.dirty = true;
        }
        for evt in self.data.drain(..) {
            f(evt);
        }
    }
}
