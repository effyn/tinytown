//! A module containing an axis-aligned bounding box using the brick grid.

use {
    cgmath::{Point3, Vector3},
    core::{
        cmp, mem,
        ops::{Add, AddAssign, Sub, SubAssign},
    },
    tinytown_collision::Aabb,
};

use super::{BrickOrientation, Vox};

/// An axis-aligned bounding box in the brick grid.
///
/// Both the minimum and maximum values are inclusive.
///
/// For example, the AABB from A to B contains A, B and all of the marked cells.
///
/// ```plain
/// +-+-+-+-+-+
/// | | | | | |
/// +-+-+-+-+-+
/// | |X|X|B| |
/// +-+-+-+-+-+
/// | |X|X|X| |
/// +-+-+-+-+-+
/// | |A|X|X| |
/// +-+-+-+-+-+
/// | | | | | |
/// +-+-+-+-+-+
/// ```
///
/// # Invariants
///
///  - `min.x <= max.x`
///  - `min.y <= max.y`
///  - `min.z <= max.z`
///
#[derive(Clone, Copy, Debug, PartialEq, Hash)]
pub struct VoxAabb {
    /// The minimum voxel
    pub min: Vox,
    /// The maximum voxel
    pub max: Vox,
}

impl VoxAabb {
    /// Create a new voxel AABB using these `min` and `max` voxels.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::{
    /// #         brick::{Vox, VoxAabb},
    /// #     },
    /// # };
    /// let aabb = VoxAabb::new(Vox::new(-1, 6, -1), Vox::new(1, 9, 1));
    ///
    /// assert_eq!(aabb.min, Vox::new(-1, 6, -1));
    /// assert_eq!(aabb.max, Vox::new(1, 9, 1));
    /// ```
    pub fn new(min: Vox, max: Vox) -> VoxAabb {
        VoxAabb { min, max }
    }

    /// Create a new voxel AABB using a minimum point and a size.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{VoxAabb, BrickOrientation, Vox},
    /// #     cgmath::Vector3,
    /// # };
    /// let aabb = VoxAabb::with_size(Vox::new(5, 6, 7), Vector3::new(2, 1, 3));
    ///
    /// assert_eq!(aabb.min, Vox::new(5, 6, 7));
    /// assert_eq!(aabb.max, Vox::new(6, 6, 9));
    /// ```
    pub fn with_size(min: Vox, size: Vector3<i32>) -> VoxAabb {
        let max = min + size - Vector3::new(1, 1, 1);
        VoxAabb { min, max }
    }

    /// Create a new voxel AABB using a minimum point, a size, and an orientation.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{VoxAabb, BrickOrientation, Vox},
    /// #     cgmath::Vector3,
    /// # };
    /// let mut orient = BrickOrientation::default();
    /// let aabb_a = VoxAabb::with_orientation(Vox::new(1, 0, 1), Vector3::new(5, 3, 1), orient);
    /// orient = orient.next();
    /// let aabb_b = VoxAabb::with_orientation(Vox::new(1, 0, 1), Vector3::new(5, 3, 1), orient);
    /// orient = orient.next();
    /// let aabb_c = VoxAabb::with_orientation(Vox::new(1, 0, 1), Vector3::new(5, 3, 1), orient);
    /// orient = orient.next();
    /// let aabb_d = VoxAabb::with_orientation(Vox::new(1, 0, 1), Vector3::new(5, 3, 1), orient);
    ///
    /// let aabb_ac = VoxAabb::new(Vox::new(1, 0, 1), Vox::new(5, 2, 1));
    /// let aabb_bd = VoxAabb::new(Vox::new(1, 0, 1), Vox::new(1, 2, 5));
    ///
    /// assert_eq!(aabb_a, aabb_ac);
    /// assert_eq!(aabb_c, aabb_ac);
    /// assert_eq!(aabb_b, aabb_bd);
    /// assert_eq!(aabb_d, aabb_bd);
    /// ```
    pub fn with_orientation(
        min: Vox,
        mut size: Vector3<u32>,
        BrickOrientation(orient): BrickOrientation,
    ) -> VoxAabb {
        if orient % 2 == 1 {
            mem::swap(&mut size.x, &mut size.z);
        }
        VoxAabb::with_size(min, size.cast::<i32>().unwrap())
    }

    /// Get the size of the voxel AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{VoxAabb, Vox},
    /// #     cgmath::Vector3,
    /// # };
    /// let aabb = VoxAabb::new(Vox::new(-1, 6, -1), Vox::new(1, 9, 1));
    ///
    /// assert_eq!(aabb.size(), Vector3::new(2, 3, 2));
    /// ```
    pub fn size(self) -> Vector3<i32> {
        self.max - self.min
    }

    /// Get whether two voxel AABBs have any voxel in common.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{VoxAabb, Vox},
    /// #     cgmath::Vector3,
    /// # };
    /// let aabb_a = VoxAabb::with_size(Vox::new(0, 1, 1), Vector3::new(3, 1, 1));
    /// let aabb_b = VoxAabb::with_size(Vox::new(1, 0, 1), Vector3::new(1, 3, 1));
    ///
    /// assert!(aabb_a.intersects(aabb_b));
    /// ```
    pub fn intersects(self, other: VoxAabb) -> bool {
        let max_min = Point3::new(
            cmp::max(self.min.x, other.min.x),
            cmp::max(self.min.y, other.min.y),
            cmp::max(self.min.z, other.min.z),
        );
        let min_max = Point3::new(
            cmp::min(self.max.x, other.max.x),
            cmp::min(self.max.y, other.max.y),
            cmp::min(self.max.z, other.max.z),
        );
        max_min.x <= min_max.x && max_min.y <= min_max.y && max_min.z <= min_max.z
    }

    /// Get the smallest voxel AABB that contains this world AABB completely.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{VoxAabb, BrickOrientation, Vox},
    /// #     tinytown_collision::Aabb,
    /// #     cgmath::Point3,
    /// # };
    /// let aabb = Aabb::new(Point3::new(0.3, 0.2, 0.6), Point3::new(1.2, 5.4, 9.3));
    ///
    /// let vox_aabb = VoxAabb::smallest_containing_world_aabb(aabb);
    ///
    /// assert!(Aabb::from(vox_aabb).contains_aabb(aabb));
    ///
    /// assert_eq!(vox_aabb, VoxAabb::new(Vox::new(0, 1, 1), Vox::new(2, 27, 18)));
    /// ```
    pub fn smallest_containing_world_aabb(aabb: Aabb) -> VoxAabb {
        let min = Vox::containing_world_pos(aabb.min);
        let max = Vox::containing_world_pos(aabb.max);
        VoxAabb::new(min, max)
    }
}

impl From<VoxAabb> for Aabb {
    fn from(vox_aabb: VoxAabb) -> Aabb {
        Aabb::new(vox_aabb.min.world_min(), vox_aabb.max.world_max())
    }
}

impl Add<Vector3<i32>> for VoxAabb {
    type Output = VoxAabb;

    fn add(self, offset: Vector3<i32>) -> VoxAabb {
        VoxAabb {
            min: self.min + offset,
            max: self.max + offset,
        }
    }
}

impl AddAssign<Vector3<i32>> for VoxAabb {
    fn add_assign(&mut self, offset: Vector3<i32>) {
        self.min += offset;
        self.max += offset;
    }
}

impl Sub<Vector3<i32>> for VoxAabb {
    type Output = VoxAabb;

    fn sub(self, offset: Vector3<i32>) -> VoxAabb {
        VoxAabb {
            min: self.min - offset,
            max: self.max - offset,
        }
    }
}

impl SubAssign<Vector3<i32>> for VoxAabb {
    fn sub_assign(&mut self, offset: Vector3<i32>) {
        self.min -= offset;
        self.max -= offset;
    }
}

#[cfg(test)]
mod tests {
    use {
        approx::assert_ulps_eq,
        proptest::{prop_compose, proptest},
        tinytown_collision::Aabb,
    };

    use super::*;

    prop_compose! {
        fn prop_vox_aabb()(
            x in 0..8,
            y in 0..8,
            z in 0..8,
            w in 0..8,
            h in 0..8,
            d in 0..8,
        ) -> VoxAabb {
            VoxAabb::with_size(Vox::new(x, y, z), Vector3::new(w, h, d))
        }
    }

    proptest! {
        #[test]
        fn prop_vox_aabb_intersect_works(
            aabb_a in prop_vox_aabb(),
            aabb_b in prop_vox_aabb(),
        ) {
            let mut field = [[[false; 16]; 16]; 16];

            for iz in aabb_a.min.z ..= aabb_a.max.z {
                for iy in aabb_a.min.y ..= aabb_a.max.y {
                    for ix in aabb_a.min.x ..= aabb_a.max.x {
                        field[iz as usize][iy as usize][ix as usize] = true;
                    }
                }
            }

            let mut intersects = false;


            for iz in aabb_b.min.z ..= aabb_b.max.z {
                for iy in aabb_b.min.y ..= aabb_b.max.y {
                    for ix in aabb_b.min.x ..= aabb_b.max.x {
                        if field[iz as usize][iy as usize][ix as usize] {
                            intersects = true;
                        }
                    }
                }
            }

            assert_eq!(aabb_a.intersects(aabb_b), intersects);
        }
    }

    #[test]
    fn test_vox_aabb_to_world_aabb_works() {
        let vox_aabb_a = VoxAabb::new(Vox::new(1, 2, 3), Vox::new(1, 4, 6));
        let vox_aabb_b = VoxAabb::new(Vox::new(-1, -2, -3), Vox::new(1, 4, 6));

        assert_ulps_eq!(
            Aabb::from(vox_aabb_a),
            Aabb::new(Point3::new(0.5, 0.4, 1.5), Point3::new(1., 1., 3.5))
        );
        assert_ulps_eq!(
            Aabb::from(vox_aabb_b),
            Aabb::new(Point3::new(-0.5, -0.4, -1.5), Point3::new(1., 1., 3.5))
        );
    }
}
