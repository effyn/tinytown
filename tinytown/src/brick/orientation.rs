use cgmath::{prelude::*, Quaternion, Rad};

/// The orientation of a brick, this can be one of four values:
///
/// ```plain
/// orientation = 0:
///
/// +-+-+-+
/// | | | |
/// +-+-+-+
///
/// -->
///
/// orientation = 1:
///
/// +-+
/// | |
/// +-+
/// | |
/// +-+ ^
/// | | |
/// +-+ |
///
/// orientation = 2:
///
///     <--
///
/// +-+-+-+
/// | | | |
/// +-+-+-+
///
/// orientation = 3:
///
/// | +-+
/// | | |
/// v +-+
///   | |
///   +-+
///   | |
///   +-+
/// ```
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickOrientation(pub u8); // TODO: make non-pub

impl Default for BrickOrientation {
    fn default() -> BrickOrientation {
        BrickOrientation(0)
    }
}

impl BrickOrientation {
    /// Rotate the brick by 90°
    pub fn next(self) -> BrickOrientation {
        BrickOrientation((self.0 + 1) % 4)
    }

    /// Rotate the brick by 90°
    pub fn prev(self) -> BrickOrientation {
        if self.0 == 0 {
            BrickOrientation(3)
        } else {
            BrickOrientation(self.0 - 1)
        }
    }

    /// Convert this orientation to a quaternion.
    pub fn to_quaternion(self) -> Quaternion<f32> {
        let angle = Rad::turn_div_4() * self.0 as f32;
        Quaternion::from_angle_y(angle)
    }
}
