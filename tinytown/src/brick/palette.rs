//! Brick color palette

use {
    core::ops::{Index, IndexMut},
    regenboog::RgbaU8,
    std::path::Path,
};

use crate::loaders::palette_loader;

/// An offset into the brick color palette
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickColorId(pub usize);

/// A palette for brick colors
#[derive(Clone, Debug, Default)]
pub struct BrickPalette {
    /// The colors in the palette
    pub colors: Vec<RgbaU8>,
}

impl BrickPalette {
    /// Create a new palette.
    pub fn new() -> BrickPalette {
        BrickPalette { colors: Vec::new() }
    }

    /// Get the amount of colors in the palette.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// assert_eq!(palette.len(), 0);
    ///
    /// palette.add_color(RgbaU8::RED);
    ///
    /// assert_eq!(palette.len(), 1);
    ///
    /// palette.add_color(RgbaU8::GREEN);
    /// palette.add_color(RgbaU8::BLUE);
    ///
    /// assert_eq!(palette.len(), 3);
    /// ```
    pub fn len(&self) -> usize {
        self.colors.len()
    }

    /// Get whether the palette is empty.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// assert!(palette.is_empty());
    ///
    /// palette.add_color(RgbaU8::RED);
    ///
    /// assert!(!palette.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.colors.is_empty()
    }

    /// Get the first color in the palette, if any.
    ///
    /// This returns a `Some` with the first color if the palette is not empty.
    ///
    /// If it is empty, this returns `None`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// assert_eq!(palette.first(), None);
    ///
    /// palette.add_color(RgbaU8::RED);
    /// palette.add_color(RgbaU8::GREEN);
    /// palette.add_color(RgbaU8::BLUE);
    ///
    /// let first = palette.first().unwrap();
    ///
    /// assert_eq!(palette[first], RgbaU8::RED);
    /// ```
    pub fn first(&self) -> Option<BrickColorId> {
        if self.colors.is_empty() {
            None
        } else {
            Some(BrickColorId(0))
        }
    }

    /// Get the last color in the palette, if any.
    ///
    /// This returns a `Some` with the last color if the palette is not empty.
    ///
    /// If it is empty, this returns `None`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// assert_eq!(palette.last(), None);
    ///
    /// palette.add_color(RgbaU8::RED);
    /// palette.add_color(RgbaU8::GREEN);
    /// palette.add_color(RgbaU8::BLUE);
    ///
    /// let last = palette.last().unwrap();
    ///
    /// assert_eq!(palette[last], RgbaU8::BLUE);
    /// ```
    pub fn last(&self) -> Option<BrickColorId> {
        if self.colors.is_empty() {
            None
        } else {
            Some(BrickColorId(self.colors.len() - 1))
        }
    }

    /// Add a color to the palette.
    ///
    /// If this color is already in the palette, returns that color.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// let color_id_a = palette.add_color(RgbaU8::RED);
    /// let color_id_b = palette.add_color(RgbaU8::GREEN);
    /// let color_id_c = palette.add_color(RgbaU8::RED);
    ///
    /// assert_eq!(color_id_a, color_id_c);
    /// assert_ne!(color_id_a, color_id_b);
    /// ```
    pub fn add_color(&mut self, color: RgbaU8) -> BrickColorId {
        if let Some(idx) = self.colors.iter().position(|&col| col == color) {
            BrickColorId(idx)
        } else {
            let idx = self.colors.len();
            self.colors.push(color);
            BrickColorId(idx)
        }
    }

    /// Load a palette from a file.
    pub fn load_palette(
        &mut self,
        path: impl AsRef<Path>,
    ) -> Result<(), palette_loader::PaletteLoadError> {
        let palette = palette_loader::load(path)?;
        for color in palette {
            self.colors.push(color);
        }
        Ok(())
    }

    /// Get the next color in the palette, wrapping around if it gets to the end.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// palette.add_color(RgbaU8::RED);
    /// palette.add_color(RgbaU8::GREEN);
    /// palette.add_color(RgbaU8::BLUE);
    ///
    /// let mut color_id = palette.first().unwrap();
    ///
    /// assert_eq!(palette[color_id], RgbaU8::RED);
    ///
    /// color_id = palette.next(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::GREEN);
    ///
    /// color_id = palette.next(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::BLUE);
    ///
    /// color_id = palette.next(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::RED);
    /// ```
    pub fn next(&self, BrickColorId(mut color_id): BrickColorId) -> BrickColorId {
        color_id += 1;
        if color_id >= self.colors.len() {
            color_id -= self.colors.len();
        }
        BrickColorId(color_id)
    }

    /// Get the previous color in the palette, wrapping around if it gets to the start.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::brick::{
    /// #         BrickPalette,
    /// #     },
    /// #     regenboog::{
    /// #         RgbaU8,
    /// #     },
    /// # };
    /// let mut palette = BrickPalette::new();
    ///
    /// palette.add_color(RgbaU8::RED);
    /// palette.add_color(RgbaU8::GREEN);
    /// palette.add_color(RgbaU8::BLUE);
    ///
    /// let mut color_id = palette.first().unwrap();
    ///
    /// assert_eq!(palette[color_id], RgbaU8::RED);
    ///
    /// color_id = palette.prev(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::BLUE);
    ///
    /// color_id = palette.prev(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::GREEN);
    ///
    /// color_id = palette.prev(color_id);
    ///
    /// assert_eq!(palette[color_id], RgbaU8::RED);
    /// ```
    pub fn prev(&self, BrickColorId(color_id): BrickColorId) -> BrickColorId {
        if color_id == 0 {
            BrickColorId(self.colors.len() - 1)
        } else {
            BrickColorId(color_id - 1)
        }
    }
}

impl Index<BrickColorId> for BrickPalette {
    type Output = RgbaU8;

    fn index(&self, BrickColorId(idx): BrickColorId) -> &Self::Output {
        &self.colors[idx]
    }
}

impl IndexMut<BrickColorId> for BrickPalette {
    fn index_mut(&mut self, BrickColorId(idx): BrickColorId) -> &mut Self::Output {
        &mut self.colors[idx]
    }
}
