use {
    core::ops::{Index, IndexMut},
    std::{collections::HashMap, path::Path},
};

use super::{BrickData, BrickDataId};

use crate::loaders::brick_loader;

/// Stores brick data
#[derive(Clone, Debug, Default)]
pub struct BrickDataStore {
    brick_datas: Vec<BrickData>,
    brick_data_id: HashMap<String, BrickDataId>,
}

impl BrickDataStore {
    /// Create a new brick data store.
    pub fn new() -> BrickDataStore {
        BrickDataStore {
            brick_datas: Vec::new(),
            brick_data_id: HashMap::new(),
        }
    }

    /// Add a brick type to the store if it doesn't exist yet.
    ///
    /// If it does exist, this will return the id of the existing brick type.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Vector3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::{BrickDataStore, BrickData},
    /// #     },
    /// # };
    /// let mut bds = BrickDataStore::new();
    ///
    /// let data_id_a = bds.add(BrickData {
    ///     name: "1x1 Plate".to_owned(),
    ///     size: Vector3::new(1, 1, 1),
    /// });
    ///
    /// let data_id_b = bds.add(BrickData {
    ///     name: "8x8 Plate".to_owned(),
    ///     size: Vector3::new(8, 1, 8),
    /// });
    ///
    /// let data_id_c = bds.add(BrickData {
    ///     name: "1x1 Plate".to_owned(),
    ///     size: Vector3::new(1, 1, 1),
    /// });
    ///
    /// assert_eq!(data_id_a, data_id_c);
    /// assert_ne!(data_id_a, data_id_b);
    /// ```
    pub fn add(&mut self, brick_data: BrickData) -> BrickDataId {
        if let Some(&id) = self.brick_data_id.get(&brick_data.name) {
            // TODO: maybe check equality?
            id
        } else {
            let id = BrickDataId(self.brick_datas.len());
            self.brick_data_id.insert(brick_data.name.clone(), id);
            self.brick_datas.push(brick_data);
            id
        }
    }

    /// Load a brick type into the store from a brick definition.
    pub fn load_brick<P: AsRef<Path>>(
        &mut self,
        path: P,
    ) -> Result<BrickDataId, brick_loader::BrickLoaderError> {
        let brick_data = brick_loader::load(path)?;
        let idx = self.add(brick_data);
        Ok(idx)
    }

    /// Find a brick type in the store by name.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Vector3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::{BrickDataStore, BrickData},
    /// #     },
    /// # };
    /// let mut bds = BrickDataStore::new();
    ///
    /// let brick_data_a = BrickData {
    ///     name: "1x1 Plate".to_owned(),
    ///     size: Vector3::new(1, 1, 1),
    /// };
    ///
    /// let brick_data_b = BrickData {
    ///     name: "8x8 Plate".to_owned(),
    ///     size: Vector3::new(8, 1, 8),
    /// };
    ///
    /// let brick_data_id_a = bds.add(brick_data_a);
    /// let brick_data_id_b = bds.add(brick_data_b);
    ///
    /// assert_eq!(bds.find_by_name("1x1 Plate").unwrap(), brick_data_id_a);
    /// assert!(bds.find_by_name("4x4 Plate").is_none());
    /// assert_eq!(bds.find_by_name("8x8 Plate").unwrap(), brick_data_id_b);
    /// ```
    pub fn find_by_name(&self, name: &str) -> Option<BrickDataId> {
        self.brick_data_id.get(name).map(|x| *x)
    }

    /// Iterate over all brick types in the store.
    pub fn iter(&self) -> impl Iterator<Item = (BrickDataId, &BrickData)> {
        self.brick_datas
            .iter()
            .enumerate()
            .map(|(i, bd)| (BrickDataId(i), bd))
    }

    /// Get the next brick type, wrapping around if it reaches the end.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Vector3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::{BrickDataStore, BrickData},
    /// #     },
    /// # };
    /// let mut bds = BrickDataStore::new();
    ///
    /// let brick_data_a = BrickData {
    ///     name: "1x1 Plate".to_owned(),
    ///     size: Vector3::new(1, 1, 1),
    /// };
    ///
    /// let brick_data_b = BrickData {
    ///     name: "4x4 Plate".to_owned(),
    ///     size: Vector3::new(4, 1, 4),
    /// };
    ///
    /// let brick_data_c = BrickData {
    ///     name: "8x8 Plate".to_owned(),
    ///     size: Vector3::new(8, 1, 8),
    /// };
    ///
    /// let brick_data_id_a = bds.add(brick_data_a);
    /// let brick_data_id_b = bds.add(brick_data_b);
    /// let brick_data_id_c = bds.add(brick_data_c);
    ///
    /// let mut current = bds.find_by_name("1x1 Plate").unwrap();
    ///
    /// assert_eq!(current, brick_data_id_a);
    ///
    /// current = bds.next(current);
    ///
    /// assert_eq!(current, brick_data_id_b);
    ///
    /// current = bds.next(current);
    ///
    /// assert_eq!(current, brick_data_id_c);
    ///
    /// current = bds.next(current);
    ///
    /// assert_eq!(current, brick_data_id_a);
    /// ```
    pub fn next(&self, BrickDataId(mut brick_data_id): BrickDataId) -> BrickDataId {
        brick_data_id += 1;
        if brick_data_id >= self.brick_datas.len() {
            brick_data_id -= self.brick_datas.len();
        }
        BrickDataId(brick_data_id)
    }

    /// Get the previous brick type, wrapping around if it reaches the start.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Vector3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::{BrickDataStore, BrickData},
    /// #     },
    /// # };
    /// let mut bds = BrickDataStore::new();
    ///
    /// let brick_data_a = BrickData {
    ///     name: "1x1 Plate".to_owned(),
    ///     size: Vector3::new(1, 1, 1),
    /// };
    ///
    /// let brick_data_b = BrickData {
    ///     name: "4x4 Plate".to_owned(),
    ///     size: Vector3::new(4, 1, 4),
    /// };
    ///
    /// let brick_data_c = BrickData {
    ///     name: "8x8 Plate".to_owned(),
    ///     size: Vector3::new(8, 1, 8),
    /// };
    ///
    /// let brick_data_id_a = bds.add(brick_data_a);
    /// let brick_data_id_b = bds.add(brick_data_b);
    /// let brick_data_id_c = bds.add(brick_data_c);
    ///
    /// let mut current = bds.find_by_name("1x1 Plate").unwrap();
    ///
    /// assert_eq!(current, brick_data_id_a);
    ///
    /// current = bds.prev(current);
    ///
    /// assert_eq!(current, brick_data_id_c);
    ///
    /// current = bds.prev(current);
    ///
    /// assert_eq!(current, brick_data_id_b);
    ///
    /// current = bds.prev(current);
    ///
    /// assert_eq!(current, brick_data_id_a);
    /// ```
    pub fn prev(&self, BrickDataId(brick_data_id): BrickDataId) -> BrickDataId {
        if brick_data_id == 0 {
            BrickDataId(self.brick_datas.len() - 1)
        } else {
            BrickDataId(brick_data_id - 1)
        }
    }
}

impl Index<BrickDataId> for BrickDataStore {
    type Output = BrickData;

    fn index(&self, BrickDataId(idx): BrickDataId) -> &Self::Output {
        &self.brick_datas[idx]
    }
}

impl IndexMut<BrickDataId> for BrickDataStore {
    fn index_mut(&mut self, BrickDataId(idx): BrickDataId) -> &mut Self::Output {
        &mut self.brick_datas[idx]
    }
}
