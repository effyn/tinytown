//! Voxel positions

use {
    cgmath::{Point3, Vector3},
    core::ops::{Add, AddAssign, Sub, SubAssign},
    tinytown_collision::Aabb,
};

use super::VoxAabb;

/// The size of one voxel in the brick grid
pub const VOXEL_SIZE: Vector3<f32> = Vector3 {
    x: 0.5,
    y: 0.2,
    z: 0.5,
};

/// The location of a voxel in the brick grid.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Vox {
    /// The x coordinate of the location
    pub x: i32,
    /// The y coordinate of the location
    pub y: i32,
    /// The z coordinate of the location
    pub z: i32,
}

impl Vox {
    /// Construct a voxel location from its x, y and z components.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use tinytown::brick::Vox;
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// assert_eq!(vox.x, 1);
    /// assert_eq!(vox.y, 2);
    /// assert_eq!(vox.z, 3);
    /// ```
    pub fn new(x: i32, y: i32, z: i32) -> Vox {
        Vox { x, y, z }
    }

    /// Get the world coordinate AABB of this voxel.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Point3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::Vox,
    /// #     },
    /// #     tinytown_collision::{
    /// #         Aabb,
    /// #     },
    /// #     approx::{
    /// #         assert_ulps_eq,
    /// #     },
    /// # };
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// let aabb = vox.world_aabb();
    ///
    /// assert_ulps_eq!(aabb.min, Point3::new(0.5, 0.4, 1.5));
    /// assert_ulps_eq!(aabb.max, Point3::new(1. , 0.6, 2. ));
    /// ```
    pub fn world_aabb(self) -> Aabb {
        Aabb::new(self.world_min(), self.world_max())
    }

    /// Get the minimum world position of this voxel.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Point3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::Vox,
    /// #     },
    /// #     approx::{
    /// #         assert_ulps_eq,
    /// #     },
    /// # };
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// assert_ulps_eq!(vox.world_min(), Point3::new(0.5, 0.4, 1.5));
    /// ```
    pub fn world_min(self) -> Point3<f32> {
        Point3::new(
            self.x as f32 * VOXEL_SIZE.x,
            self.y as f32 * VOXEL_SIZE.y,
            self.z as f32 * VOXEL_SIZE.z,
        )
    }

    /// Get the maximum world position of this voxel.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Point3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::Vox,
    /// #     },
    /// #     approx::{
    /// #         assert_ulps_eq,
    /// #     },
    /// # };
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// assert_ulps_eq!(vox.world_max(), Point3::new(1. , 0.6, 2. ));
    /// ```
    pub fn world_max(self) -> Point3<f32> {
        self.world_min() + VOXEL_SIZE
    }

    /// Get the center world position of this voxel.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{
    /// #         Point3,
    /// #     },
    /// #     tinytown::{
    /// #         brick::Vox,
    /// #     },
    /// #     approx::{
    /// #         assert_ulps_eq,
    /// #     },
    /// # };
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// assert_ulps_eq!(vox.world_center(), Point3::new(0.75 , 0.5, 1.75 ));
    /// ```
    pub fn world_center(self) -> Point3<f32> {
        self.world_min() + 0.5 * VOXEL_SIZE
    }

    /// Get the `Vox` which contains this world position.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::{
    /// #         brick::{
    /// #             Vox,
    /// #         },
    /// #     },
    /// #     cgmath::{
    /// #         Point3,
    /// #     },
    /// # };
    /// let vox = Vox::containing_world_pos(Point3::new(0.75, 0.5, 1.75));
    ///
    /// assert_eq!(vox, Vox::new(1, 2, 3));
    /// ```
    pub fn containing_world_pos(world_pos: Point3<f32>) -> Vox {
        Vox {
            x: (world_pos.x / VOXEL_SIZE.x).floor() as i32,
            y: (world_pos.y / VOXEL_SIZE.y).floor() as i32,
            z: (world_pos.z / VOXEL_SIZE.z).floor() as i32,
        }
    }

    /// Get the unit voxel AABB for this voxel.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::{
    /// #         brick::{
    /// #             Vox,
    /// #             VoxAabb,
    /// #         },
    /// #     },
    /// # };
    /// let vox = Vox::new(1, 2, 3);
    ///
    /// assert_eq!(vox.vox_aabb(), VoxAabb::new(Vox::new(1, 2, 3), Vox::new(1, 2, 3)));
    /// ```
    pub fn vox_aabb(self) -> VoxAabb {
        VoxAabb::new(self, self)
    }
}

impl From<Point3<i32>> for Vox {
    fn from(point: Point3<i32>) -> Vox {
        Vox {
            x: point.x,
            y: point.y,
            z: point.z,
        }
    }
}

impl From<Vox> for Point3<i32> {
    fn from(vox: Vox) -> Point3<i32> {
        Point3::new(vox.x, vox.y, vox.z)
    }
}

impl Add<Vector3<i32>> for Vox {
    type Output = Vox;

    fn add(self, offset: Vector3<i32>) -> Vox {
        Vox {
            x: self.x + offset.x,
            y: self.y + offset.y,
            z: self.z + offset.z,
        }
    }
}

impl AddAssign<Vector3<i32>> for Vox {
    fn add_assign(&mut self, offset: Vector3<i32>) {
        self.x += offset.x;
        self.y += offset.y;
        self.z += offset.z;
    }
}

impl Sub<Vector3<i32>> for Vox {
    type Output = Vox;

    fn sub(self, offset: Vector3<i32>) -> Vox {
        Vox {
            x: self.x - offset.x,
            y: self.y - offset.y,
            z: self.z - offset.z,
        }
    }
}

impl SubAssign<Vector3<i32>> for Vox {
    fn sub_assign(&mut self, offset: Vector3<i32>) {
        self.x -= offset.x;
        self.y -= offset.y;
        self.z -= offset.z;
    }
}

impl Sub<Vox> for Vox {
    type Output = Vector3<i32>;

    fn sub(self, other: Vox) -> Vector3<i32> {
        Vector3::new(self.x - other.x, self.y - other.y, self.z - other.z)
    }
}

#[cfg(test)]
mod tests {
    use {
        proptest::{prop_compose, proptest},
        tinytown_testutil::{prop_point3_all, prop_vec3},
    };

    use super::*;

    prop_compose! {
        fn prop_vox()(x in -128 .. 128, y in -128 .. 128, z in -128 .. 128) -> Vox {
            Vox::new(x, y, z)
        }
    }

    proptest! {
        #[test]
        fn prop_vox_aabb_contains_inner_points(
            vox in prop_vox(),
            offset in prop_vec3(0. .. VOXEL_SIZE.x, 0. .. VOXEL_SIZE.y, 0. .. VOXEL_SIZE.z),
        ) {
            let aabb = vox.world_aabb();
            assert!(aabb.contains_point(vox.world_min() + offset));
        }
    }

    proptest! {
        #[test]
        fn prop_vox_containing_world_pos_contains_that_pos(
            world_pos in prop_point3_all(-128. .. 128.),
        ) {
            let vox = Vox::containing_world_pos(world_pos);
            assert!(vox.world_aabb().contains_point(world_pos));
        }
    }
}
