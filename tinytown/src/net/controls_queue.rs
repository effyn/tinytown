use cgmath::Vector3;

use crate::controls::ControlState;

#[derive(Debug, Clone)]
pub struct ControlsQueueEntry {
    pub player_crc: u16,
    pub controls: ControlState,
    pub facing: Vector3<f32>,
}

#[derive(Debug, Clone)]
pub struct ControlsQueue {
    last_tick: u32,
    max_size: usize,
    queue: Vec<Option<ControlsQueueEntry>>,
}

impl ControlsQueue {
    pub fn new(max_size: usize) -> ControlsQueue {
        let queue = vec![None; max_size];
        ControlsQueue {
            last_tick: max_size as u32,
            max_size,
            queue,
        }
    }

    pub fn submit(
        &mut self,
        tick: u32,
        player_crc: u16,
        controls: ControlState,
        facing: Vector3<f32>,
    ) {
        if tick > self.last_tick {
            let diff = (tick - self.last_tick) as usize;
            self.last_tick = tick;
            if diff < self.max_size {
                for idx in (diff..self.max_size).rev() {
                    self.queue.swap(idx, idx - diff);
                }
                for idx in 0..diff {
                    self.queue[idx] = None;
                }
            } else {
                for idx in 0..self.max_size {
                    self.queue[idx] = None;
                }
            }
        }
        let entry = ControlsQueueEntry {
            player_crc,
            controls,
            facing,
        };
        let entry_idx = (self.last_tick - tick) as usize;
        self.queue[entry_idx] = Some(entry);
    }

    pub fn last_tick(&self) -> u32 {
        self.last_tick
    }

    pub fn iter(&self) -> impl Iterator<Item = (usize, &ControlsQueueEntry)> {
        self.queue
            .iter()
            .enumerate()
            .filter_map(|(i, e)| e.as_ref().map(|e| (i, e)))
    }

    pub fn at_tick(&self, tick: u32) -> Option<&ControlsQueueEntry> {
        if tick <= self.last_tick && self.last_tick - tick < self.max_size as u32 {
            let idx = (self.last_tick - tick) as usize;
            self.queue[idx].as_ref()
        } else {
            None
        }
    }

    pub fn size(&self) -> usize {
        let mut ctr = 0;
        for entry in &self.queue {
            if entry.is_some() {
                ctr += 1;
            }
        }
        ctr
    }
}
