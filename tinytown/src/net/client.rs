use {
    cgmath::{Point3, Vector3},
    crc::crc16,
    pigeon::{dump_to_buffer, Dump, FrameReader, FrameWriter, Grab},
    rand::Rng,
    std::{
        io::ErrorKind,
        net::{ToSocketAddrs, UdpSocket},
        time::{Duration, Instant},
    },
    tinytown_protocol::{
        ClientCookie, FrameHeader, JoinRejectReason, PushHeader, PushKind, ServerChallenge,
        UnitVector,
    },
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation},
    components::NetId,
    consts::CONTROLS_QUEUE_SIZE,
    controls::ControlState,
};

use super::{Connection, ControlsQueue, ControlsQueueEntry, PushInfo};

#[derive(Debug, Clone)]
pub enum ConnectionEvent {
    Joined {
        server_tick: u32,
    },
    JoinRejected {
        reason: JoinRejectReason,
    },
    UpdateBrick {
        net_id: NetId,
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Point3<i32>,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    SendingJoinRequest,
    SendingJoinResponse,
    Connected,
}

pub struct ClientConnection {
    sock: UdpSocket,
    name: String,
    state: State,
    last_send_time: Instant,
    challenge: ServerChallenge,
    client_cookie: ClientCookie,
    server_tick: u32,
    connection: Connection,
    controls: ControlsQueue,
    player_crc: u16,
}

impl ClientConnection {
    pub fn connect<A: ToSocketAddrs>(addr: A, name: &str) -> anyhow::Result<ClientConnection> {
        let name = name.to_owned();
        let sock = UdpSocket::bind("0.0.0.0:0")?;
        sock.set_nonblocking(true)?;
        sock.connect(addr)?;
        let mut client_cookie = [0; 8];
        let mut rng = rand::thread_rng();
        rng.fill(&mut client_cookie[..]);
        Ok(ClientConnection {
            sock,
            name,
            state: State::SendingJoinRequest,
            last_send_time: Instant::now() - Duration::from_secs(3600), // a bit of a hack...
            challenge: [0; 8],
            client_cookie,
            server_tick: 0, // TODO: keep up-to-date
            connection: Connection::initiating(),
            controls: ControlsQueue::new(CONTROLS_QUEUE_SIZE),
            player_crc: 0,
        })
    }

    pub fn server_tick(&self) -> u32 {
        self.server_tick
    }

    pub fn push_controls(&mut self, tick: u32, controls: ControlState, facing: Vector3<f32>) {
        self.controls
            .submit(tick, self.player_crc, controls, facing);
    }

    pub fn update_player_state(&mut self, pos: Point3<f32>, vel: Vector3<f32>) {
        // TODO: maybe add rotation?
        let mut buf = [0; 24];
        FrameWriter::with(&mut buf[..], |writer| {
            writer.write(pos.x);
            writer.write(pos.y);
            writer.write(pos.z);
            writer.write(vel.x);
            writer.write(vel.y);
            writer.write(vel.z);
        });
        self.player_crc = crc16::checksum_x25(&buf[..]);
    }

    pub fn process(&mut self, mut cb: impl FnMut(ConnectionEvent)) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        loop {
            let pkt = match self.sock.recv(&mut buf[..]) {
                Ok(size) => &buf[..size],
                Err(err) => {
                    if err.kind() == ErrorKind::WouldBlock {
                        break;
                    } else {
                        return Err(err.into());
                    }
                }
            };
            log::info!("packet received from host");
            log::trace!("packet data(len={}): {:?}", pkt.len(), pkt);
            let server_tick = &mut self.server_tick;
            let mut reader = FrameReader::new(pkt);
            if let Some(header) = reader.read::<FrameHeader>() {
                match header {
                    FrameHeader::JoinChallenge { challenge } => {
                        self.challenge.copy_from_slice(&challenge[..]);
                        self.state = State::SendingJoinResponse;
                    }
                    FrameHeader::JoinAccept { server_tick } => {
                        self.state = State::Connected;
                        self.server_tick = server_tick;
                        cb(ConnectionEvent::Joined { server_tick });
                    }
                    FrameHeader::JoinReject { reason } => {
                        cb(ConnectionEvent::JoinRejected { reason });
                    }
                    other => {
                        let push_status_cb = |push_event| {
                            log::info!("push status cb {:?}", push_event);
                        };
                        let push_recv_cb = |reader: &mut FrameReader, push_header: PushHeader| {
                            log::info!("push recv cb {:?}", push_header);
                            match push_header {
                                PushHeader::ServerTick { tick } => {
                                    *server_tick = tick;
                                }
                                PushHeader::Ghost { net_id, .. } => {
                                    if let Some(BrickAdapter(color_id, data_id, orientation, pos)) =
                                        reader.read()
                                    {
                                        cb(ConnectionEvent::UpdateBrick {
                                            net_id: net_id.into(),
                                            color_id,
                                            data_id,
                                            orientation,
                                            pos,
                                        });
                                    }
                                }
                                _ => (),
                            }
                            Some(())
                        };
                        let mut new_reader = FrameReader::new(pkt);
                        self.connection
                            .on_recv(&mut new_reader, push_status_cb, push_recv_cb);
                    }
                }
            } else {
                log::warn!("ignoring malformed packet from host");
            }
        }
        Ok(())
    }

    pub fn flush_frames(&mut self) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        match self.state {
            State::SendingJoinRequest => {
                if self.last_send_time.elapsed() > Duration::from_millis(250) {
                    let frame = FrameHeader::JoinRequest {
                        client_cookie: self.client_cookie,
                    };
                    let len = dump_to_buffer(&frame, &mut buf[..]);
                    self.sock.send(&buf[..len])?;
                    self.last_send_time = Instant::now();
                }
            }
            State::SendingJoinResponse => {
                if self.last_send_time.elapsed() > Duration::from_millis(250) {
                    let frame = FrameHeader::JoinResponse {
                        client_cookie: self.client_cookie,
                        challenge: self.challenge,
                        name: &self.name,
                    };
                    let len = dump_to_buffer(&frame, &mut buf[..]);
                    self.sock.send(&buf[..len])?;
                    self.last_send_time = Instant::now();
                }
            }
            State::Connected => {
                let last_tick = self.controls.last_tick();
                let controls_count = self.controls.size();
                let header = PushHeader::ClientTick {
                    tick: last_tick,
                    controls_count: controls_count as u8,
                };
                log::trace!(
                    "writing controls for frame {}, count {}",
                    last_tick,
                    controls_count
                );
                let my_controls = &self.controls;
                self.connection.add_push(&header, |f| {
                    for (offset, entry) in my_controls.iter() {
                        log::trace!(
                            "writing controls: offset={} controls={:?} facing={:?} player_crc{}",
                            offset,
                            entry.controls,
                            entry.facing,
                            entry.player_crc
                        );
                        f.write(offset as u8);
                        f.write(entry.player_crc);
                        f.write(&entry.controls);
                        f.write(UnitVector(entry.facing));
                    }
                });
                let mut writer = FrameWriter::new(&mut buf[..]);
                self.connection.write_frame(&mut writer);
                self.sock.send(writer.as_bytes())?;
            }
        }
        Ok(())
    }

    pub fn is_connected(&self) -> bool {
        if let State::Connected = self.state {
            true
        } else {
            false
        }
    }
}

struct BrickAdapter(
    pub BrickColorId,
    pub BrickDataId,
    pub BrickOrientation,
    pub Point3<i32>,
);

impl<'a> Grab<'a> for BrickAdapter {
    fn grab_from(reader: &mut FrameReader<'a>) -> Option<Self> {
        let color_id = BrickColorId(reader.read::<u8>()? as usize);
        let data_id = BrickDataId(reader.read::<u8>()? as usize);
        let orientation = BrickOrientation(reader.read()?);
        let x = reader.read()?;
        let y = reader.read()?;
        let z = reader.read()?;
        let pos = Point3::new(x, y, z);
        Some(BrickAdapter(color_id, data_id, orientation, pos))
    }
}
