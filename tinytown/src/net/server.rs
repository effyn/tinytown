use {
    cgmath::Vector3,
    crc::crc16,
    pigeon::{dump_to_buffer, grab_from_buffer, FrameReader, FrameWriter},
    specs::prelude::*,
    std::{
        collections::VecDeque,
        io::ErrorKind,
        net::{SocketAddr, ToSocketAddrs, UdpSocket},
        time::{Duration, Instant},
    },
    tinytown_protocol::{FrameHeader, JoinRejectReason, PushHeader, PushKind, UnitVector},
};

use crate::{
    components::{Brick, NetId},
    consts::SERVER_CONTROLS_QUEUE_SIZE,
    controls::ControlState,
};

use super::{Connection, ControlsQueue, ControlsQueueEntry, PushEvent, PushInfo, SlotId};

const CLIENT_TIMEOUT_MS: u64 = 1000;

#[derive(Debug)]
pub enum ConnectionEvent<'a> {
    PlayerJoined { slot: SlotId, name: &'a str },
    PlayerLeft { slot: SlotId },
    LostGhost { net_id: NetId, components: u8 },
}

pub struct ConnectionManager {
    pub sock: UdpSocket,
    pub slots: Vec<Option<ServerConnection>>,
}

impl ConnectionManager {
    pub fn bind<A: ToSocketAddrs>(addr: A, slots: usize) -> anyhow::Result<ConnectionManager> {
        let sock = UdpSocket::bind(addr)?;
        sock.set_nonblocking(true)?;
        Ok(ConnectionManager {
            sock,
            slots: (0..slots).map(|_| None).collect(),
        })
    }

    pub fn start_tick(&mut self, tick: u32) -> anyhow::Result<()> {
        for slot in &mut self.slots {
            let mut timed_out = false;
            if let Some(conn) = slot {
                if conn.last_activity.elapsed() > Duration::from_millis(CLIENT_TIMEOUT_MS) {
                    timed_out = true;
                }
            }
            if timed_out {
                // TODO: more handling
                // TODO: notify
                log::info!(
                    "client with slot {:?} timed out",
                    slot.as_ref().unwrap().slot
                );
                *slot = None;
            }
        }
        for slot in &mut self.slots {
            if let Some(conn) = slot {
                conn.on_tick(tick);
            }
        }
        Ok(())
    }

    pub fn handle_frames(
        &mut self,
        mut cb: impl for<'a> FnMut(&mut ServerConnection, ConnectionEvent<'a>),
    ) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        loop {
            let (addr, pkt) = match self.sock.recv_from(&mut buf[..]) {
                Ok((size, addr)) => (addr, &buf[..size]),
                Err(err) => {
                    if err.kind() == ErrorKind::WouldBlock {
                        break Ok(());
                    } else {
                        break Err(err.into());
                    }
                }
            };
            log::debug!("packet received from {}", addr);
            log::trace!("packet data(len={}): {:?}", pkt.len(), pkt);
            if let Some(frame) = grab_from_buffer(pkt) {
                match frame {
                    FrameHeader::JoinRequest { client_cookie } => {
                        // TODO: generate a challenge using the join cookie and the originating
                        // socketaddr
                        let frame = FrameHeader::JoinChallenge {
                            challenge: client_cookie,
                        };
                        let len = dump_to_buffer(&frame, &mut buf[..]);
                        self.sock.send_to(&buf[..len], addr)?;
                    }
                    FrameHeader::JoinResponse {
                        client_cookie,
                        challenge,
                        name,
                    } => {
                        // TODO: check challenge
                        let mut found_slot = false;
                        for (idx, slot) in self.slots.iter_mut().enumerate() {
                            if slot.is_none() {
                                let slot_id = SlotId(idx);
                                log::info!("new client on slot {:?}: name={:?}", slot_id, name);
                                *slot = Some(ServerConnection::new(slot_id, name.to_owned(), addr));
                                cb(
                                    slot.as_mut().unwrap(),
                                    ConnectionEvent::PlayerJoined {
                                        slot: slot_id,
                                        name,
                                    },
                                );
                                let frame = FrameHeader::JoinAccept { server_tick: 0 }; // TODO: put the real tick here
                                let len = dump_to_buffer(&frame, &mut buf[..]);
                                self.sock.send_to(&buf[..len], addr)?;
                                found_slot = true;
                                break;
                            }
                        }
                        if !found_slot {
                            let frame = FrameHeader::JoinReject {
                                reason: JoinRejectReason::Full,
                            };
                            let len = dump_to_buffer(&frame, &mut buf[..]);
                            self.sock.send_to(&buf[..len], addr)?;
                        }
                    }
                    _ => {
                        if let Some(mut connection) = self
                            .slots
                            .iter_mut()
                            .filter_map(|s| s.as_mut())
                            .find(|c| c.addr == addr)
                        {
                            let mut queue = VecDeque::new();
                            let mut reader = FrameReader::new(pkt);
                            connection.on_recv(&mut reader, &mut queue);
                            for evt in queue.drain(..) {
                                cb(&mut connection, evt);
                            }
                        }
                    }
                }
            } else {
                log::warn!("ignoring malformed packet from {}", addr);
            }
        }
    }

    pub fn flush_frames(&mut self) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        for slot in &mut self.slots {
            if let Some(mut connection) = slot.as_mut() {
                let mut writer = FrameWriter::new(&mut buf[..]);
                connection.connection.write_frame(&mut writer);
                self.sock.send_to(writer.as_bytes(), connection.addr)?;
            }
        }
        Ok(())
    }
}

pub struct ServerConnection {
    pub slot: SlotId,
    pub name: String,
    pub addr: SocketAddr,
    pub last_activity: Instant,
    pub connection: Connection,
    pub controls: ControlsQueue,
    pub player_entity: Option<Entity>,
}

impl ServerConnection {
    pub fn new(slot: SlotId, name: String, addr: SocketAddr) -> ServerConnection {
        ServerConnection {
            slot,
            name,
            addr,
            last_activity: Instant::now(),
            connection: Connection::accepting(),
            controls: ControlsQueue::new(SERVER_CONTROLS_QUEUE_SIZE),
            player_entity: None,
        }
    }

    pub fn on_recv<'a, 'b: 'a>(
        &mut self,
        reader: &'b mut FrameReader<'a>,
        queue: &mut VecDeque<ConnectionEvent<'a>>,
    ) {
        self.last_activity = Instant::now();
        let controls_queue = &mut self.controls;
        let push_status_cb = |push_event| {
            log::debug!("push status cb {:?}", push_event);
            match push_event {
                PushEvent::Lost(push_info) => match push_info {
                    PushInfo::Ghost { net_id, components } => {
                        queue.push_back(ConnectionEvent::LostGhost {
                            net_id: NetId(net_id),
                            components,
                        });
                    }
                    _ => (),
                },
                PushEvent::Delivered(_push_info) => {}
            }
        };
        let push_recv_cb = |reader: &mut FrameReader, push_header: PushHeader| {
            log::debug!("push recv cb {:?}", push_header);
            match push_header {
                PushHeader::ClientTick {
                    tick,
                    controls_count,
                } => {
                    for i in 0..controls_count {
                        let offset_opt = reader.read::<u8>();
                        let player_crc_opt = reader.read::<u16>();
                        let controls_opt = reader.read::<ControlState>();
                        let facing_opt = reader.read::<UnitVector>();
                        if let (
                            Some(offset),
                            Some(controls),
                            Some(UnitVector(facing)),
                            Some(player_crc),
                        ) = (offset_opt, controls_opt, facing_opt, player_crc_opt)
                        {
                            log::debug!(
                                "received controls frame: {:?} {:?} {:?} {:?}",
                                tick + offset as u32,
                                facing,
                                controls,
                                player_crc
                            );
                            controls_queue.submit(
                                tick - offset as u32,
                                player_crc,
                                controls,
                                facing,
                            );
                        }
                    }
                }
                _ => (),
            }
            Some(())
        };
        self.connection
            .on_recv(reader, push_status_cb, push_recv_cb);
    }

    pub fn on_tick(&mut self, tick: u32) {
        self.connection
            .add_push(&PushHeader::ServerTick { tick }, |_| ());
    }

    pub fn controls_for_tick(&self, tick: u32) -> Option<(ControlState, Vector3<f32>, u16)> {
        self.controls
            .at_tick(tick)
            .map(|entry| (entry.controls.clone(), entry.facing, entry.player_crc))
    }

    pub fn add_brick_ghost(&mut self, net_id: NetId, brick: &Brick) {
        self.connection.add_push(
            &PushHeader::Ghost {
                net_id: net_id.0,
                components: 0,
            },
            |f| {
                f.write(brick.color_id.0 as u8);
                f.write(brick.data_id.0 as u8);
                f.write(brick.orientation.0);
                f.write(brick.pos.x);
                f.write(brick.pos.y);
                f.write(brick.pos.z);
            },
        );
    }
}
