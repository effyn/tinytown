#![deny(missing_docs)]

//! Brick-related datastructures and functions

use {
    cgmath::{Point2, Point3, Vector2, Vector3},
    core::mem,
    regenboog::RgbaU8,
};

mod aabb;
mod brick_data_store;
mod event;
mod orientation;
mod palette;
mod vox;

pub mod grid;

pub use self::{
    aabb::VoxAabb,
    brick_data_store::BrickDataStore,
    event::{BrickEvent, BrickEventQueue},
    orientation::BrickOrientation,
    palette::{BrickColorId, BrickPalette},
    vox::{Vox, VOXEL_SIZE},
};

use crate::{mesh::Mesh, util::Directional};

/// An id into the brick data store, which stores information about bricks, such as their size,
/// name and special properties.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickDataId(pub usize);

/// The information on a brick type.
#[derive(Clone, Debug)]
pub struct BrickData {
    /// The name of the brick
    pub name: String,
    /// The size of the brick, in voxels
    pub size: Vector3<u32>,
}

impl BrickData {
    /// Get the radius of a brick
    pub fn radius(&self) -> Vector3<f32> {
        Vector3::new(
            self.size.x as f32 * VOXEL_SIZE.x * 0.5,
            self.size.y as f32 * VOXEL_SIZE.y * 0.5,
            self.size.z as f32 * VOXEL_SIZE.z * 0.5,
        )
    }

    /// Get the size of the brick, when oriented in a specific orientation.
    pub fn size_oriented(&self, BrickOrientation(ori): BrickOrientation) -> Vector3<u32> {
        let mut size = self.size;
        if ori % 2 == 1 {
            mem::swap(&mut size.x, &mut size.z);
        }
        size
    }

    /// Create the mesh of a brick
    pub fn create_mesh<V: Clone>(
        &self,
        covered: Directional<bool>,
        make_vertex: impl Fn(Point3<f32>, RgbaU8, Vector3<f32>, Vector2<f32>, Point2<f32>) -> V,
    ) -> Mesh<V> {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let (bw, bh, bd) = (self.size.x, self.size.y, self.size.z);
        let (w, h, d) = (
            bw as f32 * VOXEL_SIZE.x,
            bh as f32 * VOXEL_SIZE.y,
            bd as f32 * VOXEL_SIZE.z,
        );
        let (hw, hh, hd) = (w / 2., h / 2., d / 2.);
        let (bwf, bhf, bdf) = (bw as f32, bh as f32, bd as f32);
        let (x, y, z) = (0., 0., 0.);
        let color = RgbaU8::rgba(255, 255, 255, 200);
        if !covered.up() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 1., 0.);
            let tex_size = Vector2::new(bwf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, bdf),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bdf),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.down() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., -1., 0.);
            let tex_size = Vector2::new(bwf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, bdf),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bdf),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.north() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., -1.);
            let tex_size = Vector2::new(bwf, bhf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, bhf),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bhf),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.south() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., 1.);
            let tex_size = Vector2::new(bwf, bhf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bhf),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, bhf),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bwf, 0.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.west() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(-1., 0., 0.);
            let tex_size = Vector2::new(bhf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bhf, 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bhf, bdf),
                ),
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bdf),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.east() {
            let si = vertices.len() as u32;
            let normal = Vector3::new(1., 0., 0.);
            let tex_size = Vector2::new(bhf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(0., bdf),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bhf, bdf),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    color,
                    normal,
                    tex_size,
                    Point2::new(bhf, 0.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        Mesh::new(vertices, indices)
    }
}
