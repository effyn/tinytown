use {
    cgmath::{prelude::*, Quaternion},
    specs::{prelude::*, world::Index},
    std::{
        cmp::{Eq, PartialEq},
        collections::HashSet,
        hash::{Hash, Hasher},
        time::Instant,
    },
    tinytown_collision::{
        narrow::gjk::gjk_collide, Aabb, CollisionMask, Contact, ContactPair, Shape,
    },
};

use crate::{
    brick::BrickDataStore,
    brick::VoxAabb,
    collision_manager::CollisionManager,
    components::{Brick, Physical},
    resources::Stats,
};

pub struct CollisionSystem {
    bricks_reader_id: ReaderId<ComponentEvent>,
    physicals_reader_id: ReaderId<ComponentEvent>,
    bricks_added: BitSet,
    bricks_modified: BitSet,
    bricks_removed: BitSet,
    physicals_added: BitSet,
    physicals_modified: BitSet,
    physicals_removed: BitSet,
}

impl CollisionSystem {
    pub fn new(world: &World) -> CollisionSystem {
        let mut bricks = world.write_storage::<Brick>();
        let mut physicals = world.write_storage::<Physical>();
        let bricks_reader_id = bricks.channel_mut().register_reader();
        let physicals_reader_id = physicals.channel_mut().register_reader();
        CollisionSystem {
            bricks_reader_id,
            physicals_reader_id,
            bricks_added: BitSet::new(),
            bricks_modified: BitSet::new(),
            bricks_removed: BitSet::new(),
            physicals_added: BitSet::new(),
            physicals_modified: BitSet::new(),
            physicals_removed: BitSet::new(),
        }
    }
}

impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        Write<'a, CollisionManager>,
        ReadExpect<'a, BrickDataStore>,
        WriteExpect<'a, Stats>,
        Entities<'a>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Brick>,
    );

    fn run(
        &mut self,
        (mut collision_manager, bds, mut stats, entities, physicals, bricks): Self::SystemData,
    ) {
        let start_broad = Instant::now();
        self.bricks_added.clear();
        self.bricks_modified.clear();
        self.bricks_removed.clear();
        self.physicals_added.clear();
        self.physicals_modified.clear();
        self.physicals_removed.clear();
        for &event in physicals.channel().read(&mut self.physicals_reader_id) {
            match event {
                ComponentEvent::Inserted(idx) => {
                    self.physicals_added.add(idx);
                }
                ComponentEvent::Modified(idx) => {
                    self.physicals_modified.add(idx);
                }
                ComponentEvent::Removed(idx) => {
                    self.physicals_removed.add(idx);
                }
            }
        }
        for &event in bricks.channel().read(&mut self.bricks_reader_id) {
            match event {
                ComponentEvent::Inserted(idx) => {
                    self.bricks_added.add(idx);
                }
                ComponentEvent::Modified(_idx) => {
                    // TODO: check brick modifications
                }
                ComponentEvent::Removed(idx) => {
                    self.bricks_removed.add(idx);
                }
            }
        }
        for (entity, physical, _) in (&*entities, &physicals, &self.physicals_added).join() {
            log::info!("adding entity {:?} to bvh as physical", entity);
            let shape = physical.shape.with_transform(physical.pos, physical.rot);
            // TODO: expand shape AABB out to account for velocity
            collision_manager.add(
                entity.id(),
                CollisionMask::PHYSICAL,
                CollisionMask::BRICK.union(CollisionMask::PHYSICAL),
                shape,
            );
        }
        for (entity, brick, _) in (&*entities, &bricks, &self.bricks_added).join() {
            log::info!("adding entity {:?} to the brick grid", entity);
            let brick_data = &bds[brick.data_id];
            let vox_aabb = VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
            log::trace!("vox_aabb={:?}", vox_aabb);
            collision_manager.add_brick(entity.id(), vox_aabb);
        }
        for (entity, physical, _) in (&*entities, &physicals, &self.physicals_modified).join() {
            log::info!("modified entity {:?} from bvh", entity);
            let shape = physical.shape.with_transform(physical.pos, physical.rot);
            // TODO: expand shape AABB out to account for velocity
            collision_manager.update(
                entity.id(),
                CollisionMask::PHYSICAL,
                CollisionMask::BRICK.union(CollisionMask::PHYSICAL),
                shape,
            );
        }
        // TODO: check brick modifications
        for (entity,) in (&self.physicals_removed,).join() {
            log::info!("removing entity {:?} from bvh", entity);
            collision_manager.remove(entity);
        }
        for (entity,) in (&self.bricks_removed,).join() {
            log::info!("removing entity {:?} from the brick grid", entity);
            collision_manager.remove_brick(entity);
        }
        struct C(Index, Index);
        impl Eq for C {}
        impl PartialEq<C> for C {
            fn eq(&self, other: &C) -> bool {
                let mut s0 = self.0;
                let mut s1 = self.1;
                let mut o0 = other.0;
                let mut o1 = other.1;
                if s0 > s1 {
                    let t = s0;
                    s0 = s1;
                    s1 = t;
                }
                if o0 > o1 {
                    let t = o0;
                    o0 = o1;
                    o1 = t;
                }
                (s0, s1) == (o0, o1)
            }
        }
        impl Hash for C {
            fn hash<H: Hasher>(&self, state: &mut H) {
                if self.0 > self.1 {
                    self.0.hash(state);
                    self.1.hash(state);
                } else {
                    self.1.hash(state);
                    self.0.hash(state);
                }
            }
        }
        let mut candidates: HashSet<C> = HashSet::new();
        for (entity1, entity2) in collision_manager.colliding_pairs() {
            log::trace!(
                "potential collision between {:?} and {:?}",
                entity1,
                entity2
            );
            candidates.insert(C(entity1, entity2));
        }
        collision_manager.colliding_with_bricks(|e1, e2| {
            candidates.insert(C(e1, e2));
        });
        stats
            .broad_phase_collision_time
            .push(start_broad.elapsed().subsec_nanos() / 1000_000);
        stats.narrow_phase_collisions.push(candidates.len() as u32);
        let start_narrow = Instant::now();
        let mut collisions = Vec::with_capacity(candidates.len());
        for C(ca, cb) in candidates {
            let a = entities.entity(ca);
            let b = entities.entity(cb);
            if bricks.contains(a) && bricks.contains(b) {
                log::warn!("bricks should not be able to collide, something went really wrong");
                continue;
            }
            let shape_a = if let Some(brick) = bricks.get(a) {
                let brick_data = &bds[brick.data_id];
                let vox_aabb =
                    VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = Aabb::from(vox_aabb);
                Shape::Cuboid(world_aabb.radius())
                    .with_transform(world_aabb.center(), Quaternion::one())
            } else if let Some(physical) = physicals.get(a) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            let shape_b = if let Some(brick) = bricks.get(b) {
                let brick_data = &bds[brick.data_id];
                let vox_aabb =
                    VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = Aabb::from(vox_aabb);
                Shape::Cuboid(world_aabb.radius())
                    .with_transform(world_aabb.center(), Quaternion::one())
            } else if let Some(physical) = physicals.get(b) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            if let Some(face) = gjk_collide(&shape_a, &shape_b) {
                collisions.push(ContactPair {
                    value_a: a,
                    value_b: b,
                    contact: Contact {
                        normal: face.normal,
                        depth: face.depth,
                    },
                });
            }
        }
        stats
            .narrow_phase_collision_time
            .push(start_narrow.elapsed().subsec_nanos() / 1000_000);
        collision_manager.events.drain_vec_write(&mut collisions);
    }
}
