use specs::prelude::*;

use crate::{
    components::Player,
    net::Controller,
    ui::{GameUi, Ui},
};

pub struct UiSystem;

impl<'a> System<'a> for UiSystem {
    type SystemData = (
        WriteExpect<'a, Ui>,
        WriteExpect<'a, GameUi>,
        ReadStorage<'a, Player>,
    );

    fn run(&mut self, (mut ui, mut game_ui, players): Self::SystemData) {
        for (player,) in (&players,).join() {
            if player.controller.is_local() {
                game_ui.set_current_tool(&mut ui, player.tool);
            }
        }
    }
}
