use {
    cgmath::{prelude::*, Point3, Vector3},
    specs::prelude::*,
    std::mem,
    tinytown_collision::{CollisionMask, Ray},
};

use crate::{
    brick::{
        BrickDataId, BrickDataStore, BrickEventQueue, BrickOrientation, BrickPalette, Vox, VoxAabb,
        VOXEL_SIZE,
    },
    collision_manager::CollisionManager,
    components::{Brick, GhostBrick, Physical, Player},
    controls::{Action, Control},
    ui::Tool,
};

pub struct BuildingSystem;

impl<'a> System<'a> for BuildingSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, CollisionManager>,
        ReadExpect<'a, BrickPalette>,
        ReadExpect<'a, BrickDataStore>,
        WriteExpect<'a, BrickEventQueue>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, GhostBrick>,
        ReadStorage<'a, Physical>,
        WriteStorage<'a, Player>,
        ReadExpect<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (
            entities,
            cm,
            bp,
            bds,
            mut beq,
            mut bricks,
            mut ghost_bricks,
            physicals,
            mut players,
            lazy,
        ): Self::SystemData,
    ) {
        // TODO: check that it's the local player
        for (physical, player) in (&physicals, &mut players).join() {
            let player_eye = physical.pos + player.eye_offset();
            let controls = &player.controls;
            let mut update_ghost = false;
            for &(action, control) in &controls.events {
                if action != Action::Press {
                    continue;
                }
                match control {
                    Control::Interact => match player.tool {
                        Tool::None => {}
                        Tool::Brick => {
                            if let Some(hit) = cm.cast_ray(
                                CollisionMask::BRICK,
                                Ray::new(player_eye, player.facing),
                                100.,
                            ) {
                                let pos_noncentered = Vox::containing_world_pos(
                                    hit.point + hit.normal.mul_element_wise(VOXEL_SIZE * 0.25),
                                );
                                if let Some(gb) = player
                                    .ghost_brick
                                    .and_then(|gb_entity| ghost_bricks.get_mut(gb_entity))
                                {
                                    let brick = &bds[gb.data_id];
                                    let vox_aabb = VoxAabb::with_orientation(
                                        pos_noncentered,
                                        brick.size,
                                        gb.orientation,
                                    );
                                    let ghost_hsize_xz = Vector3::new(
                                        vox_aabb.size().x / 2,
                                        0,
                                        vox_aabb.size().y as i32 / 2,
                                    );
                                    let pos = pos_noncentered - ghost_hsize_xz;
                                    gb.pos = pos;
                                } else {
                                    let brick = &bds[player.selected_data_id];
                                    let orientation = BrickOrientation::default();
                                    let vox_aabb = VoxAabb::with_orientation(
                                        pos_noncentered,
                                        brick.size,
                                        orientation,
                                    );
                                    let ghost_hsize_xz = Vector3::new(
                                        vox_aabb.size().x / 2,
                                        0,
                                        vox_aabb.size().y as i32 / 2,
                                    );
                                    let pos = pos_noncentered - ghost_hsize_xz;
                                    let gb_entity = lazy
                                        .create_entity(&entities)
                                        .with(GhostBrick {
                                            data_id: player.selected_data_id,
                                            color_id: player.selected_color_id,
                                            orientation,
                                            pos,
                                            visual_pos: pos.world_min(),
                                            visual_rot: orientation.to_quaternion(),
                                        })
                                        .build();
                                    player.ghost_brick = Some(gb_entity);
                                }
                            }
                        }
                        Tool::Hammer => {
                            if let Some(hit) = cm.cast_ray(
                                CollisionMask::BRICK,
                                Ray::new(player_eye, player.facing),
                                100.,
                            ) {
                                if let Some(entity_index) = hit.value {
                                    let entity = entities.entity(entity_index);
                                    beq.destroy(entity);
                                }
                            }
                        }
                        _ => (),
                    },
                    Control::PalettePrev => {
                        player.selected_color_id = bp.prev(player.selected_color_id);
                        update_ghost = true;
                    }
                    Control::PaletteNext => {
                        player.selected_color_id = bp.next(player.selected_color_id);
                        update_ghost = true;
                    }
                    Control::CycleGhostPrev => {
                        // TODO: move ghost brick so it stays centered?
                        player.selected_data_id = bds.prev(player.selected_data_id);
                        update_ghost = true;
                    }
                    Control::CycleGhostNext => {
                        // TODO: move ghost brick so it stays centered?
                        player.selected_data_id = bds.next(player.selected_data_id);
                        update_ghost = true;
                    }
                    // TODO: may need to be in its own system
                    Control::PrevTool => {
                        player.tool = player.tool.prev();
                    }
                    Control::NextTool => {
                        player.tool = player.tool.next();
                    }
                    _ => (),
                }
            }
            if controls.activated(Control::Interact) {
                match player.tool {
                    Tool::PaintCan => {
                        if let Some(hit) = cm.cast_ray(
                            CollisionMask::BRICK,
                            Ray::new(player_eye, player.facing),
                            100.,
                        ) {
                            if let Some(index) = hit.value {
                                let entity = entities.entity(index);
                                if let Some(brick) = bricks.get_mut(entity) {
                                    if brick.color_id != player.selected_color_id {
                                        brick.color_id = player.selected_color_id;
                                        beq.dirty = true;
                                    }
                                }
                            }
                        }
                    }
                    _ => (),
                }
            }
            if let Some(gb) = player
                .ghost_brick
                .and_then(|gb_entity| ghost_bricks.get_mut(gb_entity))
            {
                if update_ghost {
                    gb.color_id = player.selected_color_id;
                    gb.data_id = player.selected_data_id;
                }
                let mut ghost_size = bds[gb.data_id].size;
                if gb.orientation.0 % 2 == 1 {
                    mem::swap(&mut ghost_size.x, &mut ghost_size.z);
                }
                let target_pos = gb.pos.world_min().to_vec()
                    + VOXEL_SIZE.mul_element_wise(ghost_size.cast::<f32>().unwrap()) * 0.5; // TODO: have a look at later
                let target_rot = gb.orientation.to_quaternion();
                gb.visual_pos = Point3::from_vec(gb.visual_pos.to_vec() * 0.2 + target_pos * 0.8);
                gb.visual_rot = gb.visual_rot.nlerp(target_rot, 0.8);
                let brick_fwd = if player.facing.x.abs() > player.facing.z.abs() {
                    Vector3::new(player.facing.x.signum() as i32, 0, 0)
                } else {
                    Vector3::new(0, 0, player.facing.z.signum() as i32)
                };
                let brick_ltl = Vector3::new(-brick_fwd.z, 0, brick_fwd.x);
                let ghost_speed = if controls.activated(Control::MoveGhostProportional) {
                    bds[gb.data_id]
                        .size_oriented(gb.orientation)
                        .cast::<i32>()
                        .unwrap()
                } else {
                    Vector3::new(1, 1, 1)
                };
                for &(action, control) in &controls.events {
                    if action != Action::Press {
                        continue;
                    }
                    match control {
                        Control::Build => {
                            let brick_data = &bds[gb.data_id];
                            let vox_aabb =
                                VoxAabb::with_orientation(gb.pos, brick_data.size, gb.orientation);
                            if cm.brick_grid.query_vox_aabb(vox_aabb).next().is_none() {
                                beq.build(gb.data_id, gb.color_id, gb.orientation, gb.pos);
                            }
                        }
                        Control::MoveGhostForward => {
                            gb.pos += brick_fwd.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostBackward => {
                            gb.pos -= brick_fwd.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostRight => {
                            gb.pos += brick_ltl.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostLeft => {
                            gb.pos -= brick_ltl.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostUp => {
                            gb.pos += Vector3::new(0, 1, 0).mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostDown => {
                            gb.pos -= Vector3::new(0, 1, 0).mul_element_wise(ghost_speed);
                        }
                        Control::RotateGhostCcw => {
                            gb.orientation = gb.orientation.prev();
                        }
                        Control::RotateGhostCw => {
                            gb.orientation = gb.orientation.next();
                        }
                        Control::DispelGhost => {
                            // TODO: look at how to better handle this error
                            entities
                                .delete(player.ghost_brick.unwrap())
                                .expect("ghost brick failed to delete");
                            player.ghost_brick = None;
                            break;
                        }
                        _ => (),
                    }
                }
            }
        }
    }
}
