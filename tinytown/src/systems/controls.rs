use {
    cgmath::{prelude::*, Matrix4, Vector2, Vector3},
    specs::prelude::*,
};

use crate::{camera::Camera, components::Player, controls::ControlsManager, resources::WindowSize};

pub struct ControlsSystem;

impl<'a> System<'a> for ControlsSystem {
    type SystemData = (
        ReadExpect<'a, WindowSize>,
        ReadExpect<'a, Camera>,
        ReadExpect<'a, ControlsManager>,
        WriteStorage<'a, Player>,
    );

    fn run(&mut self, (window_size, camera, controls_manager, mut players): Self::SystemData) {
        let up = Vector3::new(0., 1., 0.);
        let view_speed = 25.;
        for player in (&mut players).join() {
            if player.controller.is_local() {
                player.controls = controls_manager.state.clone();
                if let (Some(last_cursor_pos), Some(cursor_pos)) = (
                    controls_manager.last_cursor_pos(),
                    controls_manager.cursor_pos(),
                ) {
                    let width = window_size.width as f32;
                    let height = window_size.height as f32;
                    let dpos = cursor_pos - last_cursor_pos;
                    let rdpos = Vector2::new(dpos.x / width, dpos.y / height);
                    let fovy = camera.perspective_fov.fovy;
                    let fovx = camera.perspective_fov.fovy * camera.perspective_fov.aspect;
                    let angley = -fovy / 2. * rdpos.y;
                    let anglex = -fovx / 2. * rdpos.x;
                    let camera_lateral = player.facing.cross(up).normalize();
                    let rot = Matrix4::from_axis_angle(up, anglex * view_speed)
                        * Matrix4::from_axis_angle(camera_lateral, angley * view_speed);
                    player.facing = (rot * player.facing.extend(0.)).truncate();
                }
            }
        }
    }
}
