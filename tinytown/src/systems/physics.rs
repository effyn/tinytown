use {cgmath::Vector3, specs::prelude::*};

use crate::components::{Physical, Player};

pub struct PhysicsSystem;

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Player>,
    );

    fn run(&mut self, (entities, mut physicals, mut players): Self::SystemData) {
        let gravity = Vector3::new(0., -0.06, 0.);
        for (ent, physical) in (&*entities, &mut physicals).join() {
            physical.vel *= 0.88;
            physical.vel += gravity;
            physical.pos += physical.vel;
            // TODO: this really should be done with a plane collider
            let player_center_height = 1.;
            if physical.pos.y < player_center_height {
                physical.pos.y = player_center_height;
                if let Some(player) = players.get_mut(ent) {
                    player.can_jump = true;
                }
            }
        }
    }
}
