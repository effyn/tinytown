use specs::prelude::*;

use crate::{
    brick::{BrickEvent, BrickEventQueue},
    components::{Brick, NetId},
    game::GameMode,
    resources::NetIdAllocator,
};

pub struct BrickSpawnerSystem;

impl<'a> System<'a> for BrickSpawnerSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, GameMode>,
        ReadExpect<'a, NetIdAllocator>,
        WriteExpect<'a, BrickEventQueue>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, NetId>,
    );

    fn run(
        &mut self,
        (entities, game_mode, nia, mut beq, mut bricks, mut net_ids): Self::SystemData,
    ) {
        if !game_mode.is_client() {
            beq.flush(|evt| match evt {
                BrickEvent::Build {
                    data_id,
                    color_id,
                    orientation,
                    pos,
                } => {
                    // TODO: maybe disallow bricks to intersect?
                    let net_id = nia.next_id();
                    let entity = entities
                        .build_entity()
                        .with(net_id, &mut net_ids)
                        .with(Brick::new(color_id, data_id, orientation, pos), &mut bricks)
                        .build();
                    nia.register_id(net_id, entity);
                    log::debug!("creating brick {:?} at {:?} with orientation={:?} color_id={:?} data_id={:?}", entity, pos, orientation, color_id, data_id);
                }
                BrickEvent::Destroy { entity } => {
                    log::debug!("removing brick {:?}", entity);
                    entities.delete(entity).expect("could not delete entity??");
                }
            });
        }
    }
}
