use {
    approx::ulps_eq,
    cgmath::{prelude::*, Vector3},
    specs::prelude::*,
};

use crate::{
    components::{Physical, Player},
    controls::Control,
};

const MOVE_SPEED_FORWARD: f32 = 0.03;
const MOVE_SPEED_BACKWARD: f32 = 0.02;
const MOVE_SPEED_SIDEWAYS: f32 = 0.01;

pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
    type SystemData = (WriteStorage<'a, Player>, WriteStorage<'a, Physical>);

    fn run(&mut self, (mut players, mut physicals): Self::SystemData) {
        let up = Vector3::new(0., 1., 0.);
        for (player, physical) in (&mut players, &mut physicals).join() {
            let ctl = &player.controls;
            let mut vel = Vector3::new(0., 0., 0.);
            let (forward, lateral) = if !ulps_eq!(player.facing.y, 0.) {
                (
                    Vector3::new(player.facing.x, 0., player.facing.z).normalize(),
                    Vector3::new(-player.facing.z, 0., player.facing.x).normalize(),
                )
            } else {
                (Vector3::new(0., 0., 0.), Vector3::new(0., 0., 0.))
            };
            if ctl.activated(Control::Jump) && player.can_jump {
                vel += up * 2.;
            }
            if ctl.activated(Control::MoveForward) {
                vel += forward * MOVE_SPEED_FORWARD;
            }
            if ctl.activated(Control::MoveBackward) {
                vel -= forward * MOVE_SPEED_BACKWARD;
            }
            if ctl.activated(Control::MoveRight) {
                vel += lateral * MOVE_SPEED_SIDEWAYS;
            }
            if ctl.activated(Control::MoveLeft) {
                vel -= lateral * MOVE_SPEED_SIDEWAYS;
            }
            if ctl.activated(Control::Jetpack) {
                vel += up * 0.3;
            }
            physical.vel += vel;
            player.can_jump = false;
        }
    }
}
