use specs::prelude::*;

use crate::{components::NetId, resources::NetIdAllocator};

pub struct NetIdMaintainerSystem {
    reader_id: ReaderId<ComponentEvent>,
    removed: BitSet,
}

impl NetIdMaintainerSystem {
    pub fn new(world: &World) -> NetIdMaintainerSystem {
        let reader_id = world.write_storage::<NetId>().register_reader();
        NetIdMaintainerSystem {
            reader_id,
            removed: BitSet::new(),
        }
    }
}

impl<'a> System<'a> for NetIdMaintainerSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, NetIdAllocator>,
        ReadStorage<'a, NetId>,
    );

    fn run(&mut self, (entities, nia, net_ids): Self::SystemData) {
        self.removed.clear();
        for event in net_ids.channel().read(&mut self.reader_id) {
            match event {
                ComponentEvent::Removed(idx) => {
                    self.removed.add(*idx);
                }
                _ => (),
            }
        }
        for (entity, net_id, _) in (&entities, &net_ids, &self.removed).join() {
            nia.unregister_id(*net_id);
        }
    }
}
