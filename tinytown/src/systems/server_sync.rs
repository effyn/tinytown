use {
    cgmath::{prelude::*, Point3, Quaternion, Vector3},
    crc::crc16,
    pigeon::FrameWriter,
    specs::prelude::*,
    tinytown_collision::Shape,
};

use crate::{
    components::{Brick, GhostBrick, NetId, Physical, Player},
    net::{server::ConnectionEvent, ConnectionManager, Controller},
    resources::NetIdAllocator,
    tick::Tick,
};

pub struct ServerSyncStartSystem;

impl<'a> System<'a> for ServerSyncStartSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Tick>,
        ReadExpect<'a, NetIdAllocator>,
        WriteExpect<'a, ConnectionManager>,
        WriteStorage<'a, NetId>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, Physical>,
        ReadStorage<'a, Brick>,
        ReadStorage<'a, GhostBrick>,
    );

    fn run(
        &mut self,
        (
            entities,
            tick,
            nia,
            mut connection_manager,
            mut net_ids,
            mut players,
            mut physicals,
            bricks,
            ghost_bricks,
        ): Self::SystemData,
    ) {
        connection_manager.start_tick(tick.0).unwrap();
        connection_manager
            .handle_frames(|conn, evt| match evt {
                ConnectionEvent::PlayerJoined { slot, name } => {
                    let net_id = nia.next_id();
                    let entity = entities
                        .build_entity()
                        .with(net_id, &mut net_ids)
                        .with(
                            Player::new(Vector3::new(1., 0., 0.), Controller::Remote(slot)),
                            &mut players,
                        )
                        .with(
                            Physical::new(
                                Point3::new(0., 10., 0.),
                                Quaternion::one(),
                                Shape::Cuboid(Vector3::new(0.5, 1., 0.5)),
                            ),
                            &mut physicals,
                        )
                        .build();
                    nia.register_id(net_id, entity);
                    conn.player_entity = Some(entity);
                }
                ConnectionEvent::PlayerLeft { slot } => {
                    if let Some(entity) = conn.player_entity {
                        entities.delete(entity);
                    }
                }
                ConnectionEvent::LostGhost { net_id, .. } => {
                    // TODO: can be done _way_ more efficiently
                    if let Some(entity) = nia.get(net_id) {
                        if let Some(brick) = bricks.get(entity) {
                            conn.add_brick_ghost(net_id, &brick);
                        }
                    } else {
                        // TODO: send that it's been destroyed
                    }
                }
                _ => (),
            })
            .unwrap();
        for (player,) in (&mut players,).join() {
            if let Controller::Remote(slot_id) = player.controller {
                if let Some(conn) = &mut connection_manager.slots[slot_id.0] {
                    if let Some((controls, facing, player_crc)) = conn.controls_for_tick(tick.0) {
                        log::trace!(
                            "{:?} {:?} {:?} {:?}",
                            player.controller,
                            facing,
                            controls,
                            player_crc
                        );
                        player.controls = controls;
                        player.facing = facing;
                    } else {
                        log::warn!("not enough controls for player {:?}", slot_id);
                    }
                }
            }
        }
    }
}

pub struct ServerSyncEndSystem;

impl<'a> System<'a> for ServerSyncEndSystem {
    type SystemData = (
        ReadExpect<'a, Tick>,
        WriteExpect<'a, ConnectionManager>,
        ReadStorage<'a, NetId>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
        WriteStorage<'a, Brick>,
    );

    fn run(
        &mut self,
        (tick, mut connection_manager, net_ids, players, physicals, mut bricks): Self::SystemData,
    ) {
        for (player, physical) in (&players, &physicals).join() {
            log::trace!(
                "{:?} {:?} {:?}",
                player.controller,
                physical.pos,
                physical.vel
            );
            if let Controller::Remote(slot_id) = player.controller {
                if let Some(conn) = &mut connection_manager.slots[slot_id.0] {
                    if let Some((_, _, client_player_crc)) = conn.controls_for_tick(tick.0) {
                        let mut buf = [0; 24];
                        let pos = physical.pos;
                        let vel = physical.vel;
                        FrameWriter::with(&mut buf[..], |f| {
                            f.write(pos.x);
                            f.write(pos.y);
                            f.write(pos.z);
                            f.write(vel.x);
                            f.write(vel.y);
                            f.write(vel.z);
                        });
                        let server_player_crc = crc16::checksum_x25(&buf[..]);
                        if server_player_crc != client_player_crc {
                            log::debug!(
                                "client and server crc don't match: {} != {}",
                                server_player_crc,
                                client_player_crc
                            );
                        }
                    }
                    for (net_id, brick) in (&net_ids, &mut bricks).join() {
                        if brick.dirty {
                            conn.add_brick_ghost(*net_id, &*brick);
                            brick.dirty = false;
                        }
                    }
                }
            }
        }
        connection_manager.flush_frames().unwrap();
    }
}
