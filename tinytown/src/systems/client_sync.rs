use {specs::prelude::*, tinytown_protocol::JoinRejectReason};

use crate::{
    brick::{BrickEventQueue, Vox},
    components::{Brick, GhostBrick, NetId, Physical, Player},
    net::{client::ConnectionEvent, ClientConnection, Controller},
    resources::{NetIdAllocator, Sim},
    tick::Tick,
    util::Directional,
};

pub struct ClientSyncStartSystem;

impl<'a> System<'a> for ClientSyncStartSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, Tick>,
        WriteExpect<'a, Sim>,
        WriteExpect<'a, ClientConnection>,
        WriteExpect<'a, BrickEventQueue>,
        ReadExpect<'a, NetIdAllocator>,
        WriteStorage<'a, NetId>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, GhostBrick>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut tick,
            mut sim,
            mut connection,
            mut beq,
            nia,
            mut net_ids,
            mut players,
            mut bricks,
            physical,
            ghost_bricks,
        ): Self::SystemData,
    ) {
        connection
            .process(|event| {
                log::info!("connection event: {:?}", event);
                match event {
                    ConnectionEvent::Joined { server_tick } => {
                        tick.0 = server_tick + 5; // TODO: make this not magical
                        sim.paused = false;
                    }
                    ConnectionEvent::JoinRejected { reason } => match reason {
                        JoinRejectReason::Full => {
                            panic!("server full");
                        }
                    },
                    ConnectionEvent::UpdateBrick {
                        net_id,
                        color_id,
                        data_id,
                        orientation,
                        pos,
                    } => {
                        // TODO: the BEQ should be handling this...
                        if let Some(entity) = nia.get(net_id) {
                            if let Some(brick) = bricks.get_mut(entity) {
                                brick.color_id = color_id;
                                brick.data_id = data_id;
                                brick.orientation = orientation;
                                brick.pos = Vox::from(pos);
                            } else {
                                panic!("net id exists, associated component does not");
                            }
                        } else {
                            let brick = Brick::new(color_id, data_id, orientation, Vox::from(pos));
                            let entity = entities
                                .build_entity()
                                .with(net_id, &mut net_ids)
                                .with(brick, &mut bricks)
                                .build();
                            nia.register_id(net_id, entity);
                            beq.dirty = true;
                        }
                    }
                }
            })
            .expect("connection processing failure");
        if connection.is_connected() {
            let target_tick = connection.server_tick() + 5;
            if tick.0 != target_tick {
                log::warn!("ticking out of sync");
                tick.0 = target_tick;
            }
        }
    }
}

pub struct ClientSyncEndSystem;

impl<'a> System<'a> for ClientSyncEndSystem {
    type SystemData = (
        WriteExpect<'a, ClientConnection>,
        ReadExpect<'a, Tick>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
    );

    fn run(&mut self, (mut connection, tick, players, physicals): Self::SystemData) {
        if connection.is_connected() {
            for (player, physical) in (&players, &physicals).join() {
                if player.controller.is_local() {
                    connection.update_player_state(physical.pos, physical.vel);
                    connection.push_controls(tick.0, player.controls.clone(), player.facing);
                }
            }
        }
        connection.flush_frames().unwrap();
    }
}
