use {
    cgmath::{prelude::*, Vector3},
    shrev::ReaderId,
    specs::prelude::*,
    tinytown_collision::{broad::bvh::AabbQueryCache, CollisionMask, ContactPair},
};

use crate::{
    brick::{BrickDataStore, VOXEL_SIZE},
    collision_manager::CollisionManager,
    components::{Brick, Physical, Player},
};

pub struct BrickCollisionResolver {
    reader_id: ReaderId<ContactPair<Entity>>,
    aabb_query_cache: AabbQueryCache,
}

impl BrickCollisionResolver {
    pub fn new(world: &World) -> BrickCollisionResolver {
        let reader_id = world
            .fetch_mut::<CollisionManager>()
            .events
            .register_reader();
        BrickCollisionResolver {
            reader_id,
            aabb_query_cache: AabbQueryCache::new(),
        }
    }
}

impl<'a> System<'a> for BrickCollisionResolver {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, CollisionManager>,
        ReadExpect<'a, BrickDataStore>,
        ReadStorage<'a, Brick>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Player>,
    );

    fn run(
        &mut self,
        (entities, collision_manager, bds, bricks, mut physicals, mut players): Self::SystemData,
    ) {
        for contact in collision_manager.events.read(&mut self.reader_id).cloned() {
            let mut entity_a = contact.value_a;
            let mut entity_b = contact.value_b;
            let mut normal = contact.contact.normal;
            let depth = contact.contact.depth;
            if bricks.contains(entity_a) {
                let temp = entity_a;
                entity_a = entity_b;
                entity_b = temp;
                normal = -normal;
            }
            if !bricks.contains(entity_b) {
                // not a brick collision
                continue;
            }
            let brick = bricks.get(entity_b).unwrap();
            let physical = physicals.get_mut(entity_a).unwrap();
            if normal.y == 0. {
                let brick_data = &bds[brick.data_id];
                let brick_top_y = brick.pos.world_min().y + VOXEL_SIZE.y * brick_data.size.y as f32; // TODO: clean up
                let physical_aabb = physical.aabb();
                let new_aabb = physical_aabb + Vector3::new(0., brick_top_y + 0.01, 0.);
                let hit = collision_manager
                    .query_aabb(&mut self.aabb_query_cache, CollisionMask::BRICK, new_aabb)
                    .next()
                    .is_some();
                if new_aabb.min.y >= physical_aabb.min.y
                    && !hit
                    && (new_aabb.min.y - physical_aabb.min.y).abs() <= 1.3
                {
                    physical.vel.y = 0.;
                    physical.pos = new_aabb.center();
                } else {
                    physical.vel -= physical.vel.project_on(normal);
                    physical.pos -= normal * depth;
                }
            } else {
                physical.vel -= physical.vel.project_on(normal);
                physical.pos -= normal * depth;
            }
            if let Some(p) = players.get_mut(entity_a) {
                if normal.dot(Vector3::new(0., -1., 0.)) > 0.5 {
                    p.can_jump = true;
                }
            }
        }
    }
}
